
DROP TABLE IF EXISTS alert_action;
CREATE TABLE alert_action (
	action_id character varying(50) NOT NULL,
    alert_id character varying(50) DEFAULT NULL::character varying,
    action_type text,
    action_name text,
    date_created date,
    date_modified date,
    workflow_id character varying(50) DEFAULT NULL::character varying,
    action_order integer,
    action_param text,
    tenant_id character varying(50)  NOT NULL,
PRIMARY KEY (ACTION_ID)
);

DROP TABLE IF EXISTS alert_data;
CREATE TABLE alert_data (
    alert_id character varying(50) NOT NULL,
    alert_type character varying(45) DEFAULT NULL::character varying,
    message text,
    description text,
    date_created timestamp without time zone,
    status character varying(20) DEFAULT NULL::character varying,
    alert_definition_id character varying(50) DEFAULT NULL::character varying,
    field1 character varying(40) DEFAULT NULL::character varying,
    field2 character varying(40) DEFAULT NULL::character varying,
    field3 character varying(40) DEFAULT NULL::character varying,
    alert_query text,
    message_name character varying(60) DEFAULT NULL::character varying,
    alert_name character varying(60) DEFAULT NULL::character varying,
    alert_index_id text,
    alert_status character varying(40) DEFAULT NULL::character varying,
    status_modified timestamp without time zone,
    tenant_id character varying(50) NOT NULL,
	alert_def_type character varying(50),
	alert_search_tags character varying(100),
PRIMARY KEY (ALERT_ID)
);

DROP TABLE IF EXISTS alert_status;
CREATE TABLE alert_status (
    status_id character varying(50) NOT NULL,
    alert_config_key character varying(100) DEFAULT NULL::character varying,
    field1 character varying(40) DEFAULT NULL::character varying,
    field2 character varying(40) DEFAULT NULL::character varying,
    field3 character varying(40) DEFAULT NULL::character varying,
    status character varying(20) DEFAULT NULL::character varying,
    tenant_id character varying(50)  NOT NULL,
PRIMARY KEY (STATUS_ID)
);


DROP TABLE IF EXISTS authorities;
CREATE TABLE authorities (
    authority character varying(50) NOT NULL
);

DROP TABLE IF EXISTS channel;
CREATE TABLE channel (
    channel_id character varying(50) NOT NULL,
    subsystem_id character varying(50) DEFAULT NULL::character varying,
    channel_json text NOT NULL,
    channel_name character varying(60) DEFAULT NULL::character varying,
    class_name character varying(255) DEFAULT NULL::character varying,
    parallelism integer,
    max_spout_pending integer,
    scheme character varying(255) DEFAULT NULL::character varying,
    output_fields text,
    message_type_id character varying(50) DEFAULT NULL::character varying,
    config text,
PRIMARY KEY (channel_id)
);

DROP TABLE IF EXISTS common_message_group_field;
CREATE TABLE common_message_group_field (  
    field_alias character varying(60) NOT NULL,
    index_analyzer character varying(50),
    search_analyzer character varying(50),
    index_tf character varying(20),
    store character varying(20),
    encrypt character varying(20),
    column_family character varying(60),
    store_in_indexer character varying(3),
    persistence_configured character varying(6),
    tenant_id character varying(50) NOT NULL,
    data_type character varying(50),
    field_name character varying(60) DEFAULT NULL::character varying,
    field_label character varying(60) DEFAULT NULL::character varying,
    regex text,
    validation_yn character varying(1) DEFAULT NULL::character varying,
    validation_id character varying(50) DEFAULT NULL::character varying,
    alertable_yn character varying(1) DEFAULT 'Y'::character varying,
    is_input_field character varying(1) DEFAULT 'Y'::character varying,
    del_prefix character varying(255) DEFAULT NULL::character varying,
    del_postfix character varying(255) DEFAULT NULL::character varying,
PRIMARY KEY (field_alias)
);


DROP TABLE IF EXISTS component;
CREATE TABLE component (
    component_id character varying(50) NOT NULL,
    subsystem_id character varying(50) DEFAULT NULL::character varying,
    component_json text NOT NULL,
    component_name character varying(60) DEFAULT NULL::character varying,
    class_name character varying(255) DEFAULT NULL::character varying,
    parallelism integer,
    task_count integer,
    output_fields text,
    message_type_id character varying(50) DEFAULT NULL::character varying,
    config text,
    type character varying(50) DEFAULT NULL::character varying,
    grouping text,
 PRIMARY KEY (component_id)
);


DROP TABLE IF EXISTS dynamic_cep_config_action;
CREATE TABLE dynamic_cep_config_action (
  	dynamic_cep_config_id character varying(100) NOT NULL,
    cep_query_id character varying(100) NOT NULL,
    subsystem_name character varying(60) DEFAULT NULL::character varying,
    cep_action text,
    creation_date date NOT NULL
);

DROP TABLE IF EXISTS dynamic_cep_config;
CREATE TABLE dynamic_cep_config (
	cep_query_id character varying(100) NOT NULL,
    component_id character varying(100) NOT NULL,
    cep_query text NOT NULL,
    creation_date date NOT NULL,
    tenant_id character varying(50) NOT NULL
);


DROP TABLE IF EXISTS grok_masterdata;
DROP SEQUENCE IF EXISTS grok_masterdata_seq;
CREATE SEQUENCE grok_masterdata_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE grok_masterdata (
	 pattern_id integer DEFAULT nextval('grok_masterdata_seq'::regclass) NOT NULL,
    pattern_name character varying(255),
    pattern_value text,
    date_created date,
    date_modified date,
    pattern_code character varying(255)
);


DROP TABLE IF EXISTS log_config;
CREATE TABLE log_config (
   log_config_id character varying(50) NOT NULL,
    path text DEFAULT NULL::character varying,
    message_id character varying(60) DEFAULT NULL::character varying,
    creation_date date,
    tenant_id character varying(50)  NOT NULL,
    is_filter_required character varying(1) DEFAULT 'N'::character varying,
    alert_json text,
    encrypt_at_source character varying(1) DEFAULT 'N'::character varying,
    emitter_info character varying(255),
    read_file_from character varying(10),
    location_enable character varying(1) DEFAULT 'Y'::character varying,
PRIMARY KEY (LOG_CONFIG_ID)
);

DROP TABLE IF EXISTS log_pattern;
CREATE TABLE log_pattern (
	pattern_id character varying(50) NOT NULL,
    message_id character varying(200) DEFAULT NULL::character varying,
    field_id character varying(50) DEFAULT NULL::character varying,
    pattern_name character varying(255),
    pattern text,
    tenant_id character varying(50)  NOT NULL,
    field_delimitter character varying(500) DEFAULT NULL::character varying,
    del_prefix character varying(255),
    del_postfix character varying(255),
PRIMARY KEY (pattern_id)
);

DROP TABLE IF EXISTS log_saved_search;
CREATE TABLE log_saved_search (
    log_search_id character varying(45) NOT NULL,
    name character varying(200) NOT NULL,
    message_id character varying(100) DEFAULT NULL::character varying,
    criteria text,
    select_fields character varying(300) DEFAULT NULL::character varying,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    tenant_id character varying(50)  NOT NULL,
PRIMARY KEY (log_search_id)
);

DROP TABLE IF EXISTS logpath_info;
CREATE TABLE logpath_info (
   log_id character varying(50) NOT NULL,
    system_id character varying(90) DEFAULT ''::character varying NOT NULL,
    logpath character varying(200) DEFAULT ''::character varying NOT NULL,
    log_type character varying(100) DEFAULT ''::character varying NOT NULL,
    tenant_id character varying(50)  NOT NULL,
 PRIMARY KEY (log_id)
);

DROP TABLE IF EXISTS message;
CREATE TABLE message (
    message_id character varying(50) NOT NULL,
    type character varying(45) DEFAULT NULL::character varying,
    delimiter character varying(10) DEFAULT NULL::character varying,
    message_name character varying(60) NOT NULL,
    createddt date,
    updateddt date,
    message_type character varying(45) DEFAULT NULL::character varying,
    is_multiline_required character varying(1) DEFAULT NULL::character varying,
    is_multiline_negate character varying(1) DEFAULT ''::character varying,
    multiline_regex character varying(255) DEFAULT NULL::character varying,
    timestamp_format character varying(255) DEFAULT 'STANDARD'::character varying,
    index_row_key character varying(255) DEFAULT 'com.streamanalytix.core.persistence.keygen.UUIDGenerator'::character varying,
    persistence_row_key character varying(255) DEFAULT 'com.streamanalytix.core.persistence.keygen.UUIDGenerator'::character varying,
    tenant_id character varying(50)  NOT NULL,
    timestamp_column character varying(60) NOT NULL,
    group_id character varying(50),
    strict_validation character varying(5) DEFAULT 'false'::character varying,
    custom_parser_class character varying(255),
    log_message text,
PRIMARY KEY (message_id),
CONSTRAINT message_per_tenant UNIQUE (message_name,TENANT_ID)
);


DROP TABLE IF EXISTS message_alert;
CREATE TABLE message_alert (
    message_alert_id character varying(50) NOT NULL,
    alert_name character varying(60) DEFAULT ''::character varying NOT NULL,
    alert_json text,
    alert_query text,
    creation_date timestamp without time zone,
    modification_date timestamp without time zone,
    message_name character varying(60) DEFAULT NULL::character varying,
    alert_query_fields character varying(100) DEFAULT NULL::character varying,
    alert_description text,
    alert_description_fields character varying(100) DEFAULT NULL::character varying,
    enable_yn character varying(1) DEFAULT 'Y'::character varying,
    persist_yn character varying(1) DEFAULT 'Y'::character varying,
    aggregation_yn character varying(5) DEFAULT 'true'::character varying,
    window_time integer,
    frequency integer,
    alert_display_query text,
    alert_action_type character varying(100) DEFAULT NULL::character varying,
    alert_severity character varying(20) DEFAULT NULL::character varying,
    tenant_id character varying(50)  NOT NULL,
    alert_grouping_fields character varying(200) DEFAULT NULL::character varying,
    grouped_yn character varying(1) DEFAULT 'N'::character varying,
	alert_def_type character varying(50),
	alert_search_tags character varying(100),
PRIMARY KEY (message_alert_id),
CONSTRAINT alert_per_tenant UNIQUE (ALERT_NAME,TENANT_ID)
);

DROP TABLE IF EXISTS message_field;
CREATE TABLE message_field (
    message_field_id character varying(50) NOT NULL,
    field_name character varying(60) DEFAULT NULL::character varying,
    field_label character varying(60) DEFAULT NULL::character varying,
    regex text,
    field_order integer,
    validation_yn character varying(1) DEFAULT NULL::character varying,
    validation_id character varying(50) DEFAULT NULL::character varying,
    alertable_yn character varying(1) DEFAULT 'Y'::character varying,
    is_input_field character varying(1) DEFAULT 'Y'::character varying,
    del_prefix character varying(255) DEFAULT NULL::character varying,
    del_postfix character varying(255) DEFAULT NULL::character varying,
    message_id character varying(50) DEFAULT NULL::character varying,
    group_field_id character varying(255) DEFAULT NULL::character varying,
    field_delimiter character varying(255) DEFAULT NULL::character varying,
PRIMARY KEY (message_field_id) 
);
  

  
DROP TABLE IF EXISTS message_group;
CREATE TABLE message_group (
    group_id character varying(50) NOT NULL,
    group_name character varying(60),
    index_numberof_shards integer DEFAULT 2,
    index_replication_factor integer,
    across_field_search_enabled character varying(5) DEFAULT 'true',
    tenant_id character varying(50),
    timestamp_alias character varying(60),
    routing_required character varying(5) DEFAULT 'false',
    routing_policy character varying(8192),
    index_source character varying(5) DEFAULT 'false',
    index_expression character varying(255),
    persistence_expression character varying(255),
    indexing_enabled character varying(10),
    persistence_enabled character varying(10),
    hbase_region_boundaries character varying(255),
    regionsplit character varying(20),
    compression character varying(5) DEFAULT 'false'::character varying,
PRIMARY KEY (group_id),
CONSTRAINT message_group_per_tenant UNIQUE (group_name,TENANT_ID)
);


DROP TABLE IF EXISTS message_group_field;
CREATE TABLE message_group_field (
    group_field_id character varying(50) NOT NULL,
    field_alias character varying(60),
    index_analyzer character varying(50),
    search_analyzer character varying(50),
    index_tf character varying(5),
    store character varying(5),
    encrypt character varying(5),
    column_family character varying(60),
    store_in_indexer character varying(5),
    persistence_configured character varying(6),
    group_id character varying(50),
    tenant_id character varying(50) NOT NULL,
    data_type character varying(50),
    is_common character varying(1) DEFAULT 'N'::character varying,
PRIMARY KEY (group_field_id)
);

DROP TABLE IF EXISTS message_template;
CREATE TABLE message_template (
    template_id character varying(50) NOT NULL,
    template_name character varying(100) NOT NULL,
    pattern_name character varying(255) DEFAULT NULL::character varying,
    parser_type character varying(100) DEFAULT NULL::character varying,
    is_multiline_required character varying(1) DEFAULT NULL::character varying,
    is_multiline_negate character varying(1) DEFAULT ''::character varying,
    multiline_regex character varying(255) DEFAULT NULL::character varying,
    timestamp_format character varying(255) DEFAULT NULL::character varying,
    parsing_at_source character varying(1) DEFAULT 'N'::character varying,
PRIMARY KEY (template_id)
);

DROP TABLE IF EXISTS message_template_field;
DROP SEQUENCE IF EXISTS message_template_field_seq;
CREATE SEQUENCE message_template_field_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE message_template_field (
  template_field_id integer DEFAULT nextval('message_template_field_seq'::regclass) NOT NULL,
  field_name character varying(60) DEFAULT NULL::character varying,
  field_label character varying(60) DEFAULT NULL::character varying,
  field_alias character varying(60) NOT NULL,
  data_type character varying(50) DEFAULT NULL::character varying,
  regex text,
  validation_yn character varying(1) DEFAULT NULL::character varying,
  validation_id character varying(50) DEFAULT NULL::character varying,
  alertable_yn character varying(1) DEFAULT 'Y'::character varying,
  is_input_field character varying(1) DEFAULT 'Y'::character varying,
  del_prefix character varying(255) DEFAULT NULL::character varying,
  del_postfix character varying(255) DEFAULT NULL::character varying,
  is_template_timefield character varying(1) DEFAULT NULL::character varying,
  index_analyzer character varying(50) DEFAULT NULL::character varying,
  search_analyzer character varying(50) DEFAULT NULL::character varying,
  index_tf character varying(20) DEFAULT NULL::character varying,
  store character varying(20) DEFAULT NULL::character varying,
  encrypt character varying(20) DEFAULT NULL::character varying,
  column_family character varying(60) DEFAULT NULL::character varying,
  store_in_indexer character varying(3) DEFAULT NULL::character varying,
  persistence_configured character varying(6) DEFAULT NULL::character varying, 
  template_id character varying(50) NOT NULL,
  tenant_id character varying(50) NOT NULL,
PRIMARY KEY (template_field_id)
);

DROP TABLE IF EXISTS sax_lic_violation;
DROP SEQUENCE IF EXISTS sax_lic_violation_seq;
CREATE SEQUENCE sax_lic_violation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE sax_lic_violation (
    violation_id integer DEFAULT nextval('sax_lic_violation_seq'::regclass) NOT NULL,
    license_id integer NOT NULL,
    violate_index character varying(50) DEFAULT NULL::character varying,
    violate_date character varying(50) DEFAULT NULL::character varying,
    notification_sent character varying(50) DEFAULT NULL::character varying,
    isrectified character varying(50) DEFAULT NULL::character varying,
PRIMARY KEY (violation_id)
);

DROP TABLE IF EXISTS sax_license;
DROP SEQUENCE IF EXISTS sax_license_seq;
CREATE SEQUENCE sax_license_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE sax_license (
    license_id integer DEFAULT nextval('sax_license_seq'::regclass) NOT NULL,
    license_content bytea,
    date_created character varying(50) DEFAULT NULL::character varying,
    active character varying(50) DEFAULT NULL::character varying,
    master_mac_address character varying(50) DEFAULT NULL::character varying,
    expiry_notify character varying(50) DEFAULT NULL::character varying,
PRIMARY KEY (license_id)
);

DROP TABLE IF EXISTS self_test_record;
CREATE TABLE self_test_record (
    test_record_id character varying(50) NOT NULL,
    record text,
    channel_source character varying(10) DEFAULT NULL::character varying,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
PRIMARY KEY (test_record_id)
);

DROP TABLE IF EXISTS self_test_result;
CREATE TABLE self_test_result (
    test_result_id integer,
    test_status character varying(20) DEFAULT NULL::character varying,
    component_name character varying(60) DEFAULT ''::character varying NOT NULL,
    test_record_id character varying(50) DEFAULT ''::character varying NOT NULL,
    test_timestamp timestamp without time zone DEFAULT now() NOT NULL,
PRIMARY KEY (component_name,test_record_id)
);

DROP TABLE IF EXISTS terms;
CREATE TABLE terms (
  agreement_accepted character varying(5) NOT NULL,
  PRIMARY KEY (agreement_accepted)
);
DROP TABLE IF EXISTS subsystem_info;
CREATE TABLE subsystem_info (
    subsystem_id character varying(50) NOT NULL,
    subsystem_json text NOT NULL,
    subsystem_name character varying(60) NOT NULL,
    config character varying(100) DEFAULT NULL::character varying,
    worker_count integer,
    ackers_count integer,
    defaultfallback character varying(50) DEFAULT NULL::character varying,
    scheduling text,
    status character varying(20) NOT NULL,
    customjarstatus character varying(20) DEFAULT NULL::character varying,
    failedreason text,
    tenant_id character varying(50) ,
PRIMARY KEY (subsystem_name)
);

DROP TABLE IF EXISTS subsystem_integration;
CREATE TABLE subsystem_integration (
    id character varying(50) NOT NULL,
    json text,
    tenant_id character varying(50) DEFAULT NULL::character varying,
PRIMARY KEY (id)
);


DROP TABLE IF EXISTS system_alert_data;
CREATE TABLE system_alert_data (
  ALERT_ID character varying(50) NOT NULL,
  ALERT_TYPE character varying(45) DEFAULT NULL::character varying,
  MESSAGE text,
  DESCRIPTION text,
  DATE_CREATED timestamp DEFAULT NULL,
  STATUS character varying(20) DEFAULT NULL::character varying,
  ALERT_DEFINITION_ID character varying(50) DEFAULT NULL::character varying,
  FIELD1 character varying(60) DEFAULT NULL::character varying,
  FIELD2 character varying(60) DEFAULT NULL::character varying,
  FIELD3 character varying(60) DEFAULT NULL::character varying,
  ALERT_QUERY text,
  MESSAGE_NAME character varying(60) DEFAULT NULL::character varying,
  ALERT_NAME character varying(60) DEFAULT NULL::character varying,
  ALERT_INDEX_ID text,
  ALERT_STATUS character varying(40) DEFAULT NULL::character varying,
  STATUS_MODIFIED timestamp DEFAULT NULL,
  TENANT_ID character varying(50) NOT NULL,
  ALERT_DEF_TYPE character varying(50),
  ALERT_SEARCH_TAGS character varying(100),
  PRIMARY KEY (ALERT_ID)
);

DROP TABLE IF EXISTS system_group;
CREATE TABLE system_group (
    group_id character varying(50) NOT NULL,
    group_name character varying(60) DEFAULT NULL::character varying,
    tenant_id character varying(50)  NOT NULL,
PRIMARY KEY (group_id)
);
CREATE UNIQUE INDEX group_name_tenant ON system_group
(group_id, UPPER(group_name), tenant_id);

DROP TABLE IF EXISTS system_group_jointable;
CREATE TABLE system_group_jointable (
    group_id character varying(50) DEFAULT NULL::character varying,
    system_id character varying(50) DEFAULT NULL::character varying,
    tenant_id character varying(50)  NOT NULL,
CONSTRAINT group_system_tenant UNIQUE (group_id,system_id,TENANT_ID)
);

DROP TABLE IF EXISTS system_info;
CREATE TABLE system_info (
    system_id character varying(50) NOT NULL,
    architecture character varying(10) DEFAULT NULL::character varying,
    data_model character varying(50) DEFAULT NULL::character varying,
    os_name character varying(200) DEFAULT NULL::character varying,
    cpu_cores integer,
    cpu_frequency integer,
    cpu_model character varying(70) DEFAULT NULL::character varying,
    cpu_vendor character varying(20) DEFAULT NULL::character varying,
    disk_info text,
    host_ip character varying(100) DEFAULT NULL::character varying,
    host_name character varying(200) DEFAULT NULL::character varying,
    memory integer,
    ping timestamp without time zone,
    host_availability character varying(5) DEFAULT NULL::character varying,
    last_available_time timestamp without time zone,
    disable_flag character varying(5) DEFAULT NULL::character varying,
    tenant_id character varying(50)  NOT NULL,
PRIMARY KEY (system_id)
);

DROP TABLE IF EXISTS tenant;
CREATE TABLE tenant (
    tenant_id character varying(50) NOT NULL,
    parent_tenant_id character varying(50) NOT NULL,
    tenant_name character varying(60) DEFAULT NULL::character varying,
    delete_yn character varying(1) DEFAULT NULL::character varying,
    tenant_supervisors text,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
PRIMARY KEY (tenant_id),
CONSTRAINT tenant_name UNIQUE (tenant_name,delete_yn)
);

DROP TABLE IF EXISTS user_tenant_association;
DROP SEQUENCE IF EXISTS user_tenant_association_seq;
CREATE SEQUENCE user_tenant_association_seq
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE user_tenant_association (
    association_id integer DEFAULT nextval('user_tenant_association_seq'::regclass) NOT NULL,
    user_id character varying(50) NOT NULL,
    tenant_id character varying(50) NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
PRIMARY KEY (association_id)
);

DROP TABLE IF EXISTS user_token;
CREATE TABLE user_token (
    user_token_id character varying(50) NOT NULL,
    token character varying(200) NOT NULL,
    username character varying(60) DEFAULT NULL::character varying,
    tenant_id character varying(50) DEFAULT NULL::character varying,
    time_to_live character varying(100) DEFAULT NULL::character varying,
    createdt timestamp without time zone DEFAULT now() NOT NULL,
PRIMARY KEY (user_token_id)
);

DROP TABLE IF EXISTS users;
CREATE TABLE users (
    username character varying(60) NOT NULL,
    password character varying(60) NOT NULL,
    email_id character varying(254) NOT NULL,
    enabled character varying(1) NOT NULL,
    tenant_id character varying(50) NOT NULL,
    delete_yn character varying(1) NOT NULL,
PRIMARY KEY (username)
);

DROP TABLE IF EXISTS users_authorities;
CREATE TABLE users_authorities (
    username character varying(60) NOT NULL,
    authority character varying(30) NOT NULL
);

DROP TABLE IF EXISTS validation;
CREATE TABLE validation (
    validation_id character varying(50) NOT NULL,
    validation_name character varying(60) DEFAULT NULL::character varying,
    validation_data_type character varying(50) DEFAULT NULL::character varying,
    validation_type character varying(50) DEFAULT NULL::character varying,
    validation_expression character varying(50) DEFAULT NULL::character varying,
    tenant_id character varying(50)  NOT NULL,
PRIMARY KEY (VALIDATION_ID)
);

DROP TABLE IF EXISTS workflow;
CREATE TABLE workflow (
    workflow_id character varying(50) NOT NULL,
    workflow_name character varying(60) NOT NULL,
    workflow_type character varying(50) DEFAULT NULL::character varying,
    workflow_param text,
    date_created date,
    date_modified date,
    tenant_id character varying(50)  NOT NULL,
PRIMARY KEY (WORKFLOW_ID)
);

DROP TABLE IF EXISTS rtstream_zk_info;
CREATE TABLE rtstream_zk_info (
  rtstream_zk_info_id varchar(50) NOT NULL,
  stream_id varchar(100) DEFAULT NULL,
  query_id varchar(100) DEFAULT NULL,
  zk_path varchar(100) DEFAULT NULL,
  PRIMARY KEY (rtstream_zk_info_id)
);

/* GA-1.2 release scripts */

DROP TABLE IF EXISTS validation;
ALTER TABLE message_field DROP COLUMN validation_yn, DROP COLUMN validation_id;
ALTER TABLE message_template_field DROP COLUMN validation_yn, DROP COLUMN validation_id;
ALTER TABLE common_message_group_field DROP COLUMN validation_yn, DROP COLUMN validation_id;

ALTER TABLE log_config ALTER COLUMN emitter_info TYPE text;

ALTER TABLE message DROP custom_parser_class;
ALTER TABLE message ADD COLUMN is_registered_parser varchar(1) DEFAULT 'N';
ALTER TABLE message ADD COLUMN custom_parser_id varchar(60) DEFAULT NULL;
ALTER TABLE message ADD COLUMN createdby varchar(60) DEFAULT NULL;

ALTER TABLE message_group ADD COLUMN date_created timestamp without time zone DEFAULT now() NOT NULL;
ALTER TABLE message_group_field ADD COLUMN date_format character varying(60) DEFAULT NULL::character varying;

ALTER TABLE sax_license ADD COLUMN violation text NOT NULL;
ALTER TABLE sax_license ADD COLUMN sax_lic_id text NOT NULL;

ALTER TABLE subsystem_info ADD COLUMN username character varying(60);

ALTER TABLE users ADD COLUMN locale character varying(10) NOT NULL DEFAULT 'en_US';

DROP TABLE IF EXISTS variable;
DROP SEQUENCE IF EXISTS variable_id_field_seq;
CREATE SEQUENCE variable_id_field_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE variable (
  variable_id integer DEFAULT nextval('variable_id_field_seq'::regclass) NOT NULL,
  variable_name character varying(50) NOT NULL,
  variable_value character varying(512) NOT NULL,
  variable_datatype character varying(50) NOT NULL,
  variable_scope  character varying(50) NOT NULL,
  tenant_id character varying(50) DEFAULT NULL,
  topology_name character varying(50) DEFAULT NULL,
  PRIMARY KEY (variable_id)
);

DROP TABLE IF EXISTS custom_component;
CREATE TABLE custom_component (
  custom_component_id varchar(50) NOT NULL,
  custom_component_name varchar(50) NOT NULL,
  jar_name varchar(255) NOT NULL,
  jar_version decimal NOT NULL,
  custom_component_json text NOT NULL,
  scope varchar(10) NOT NULL,
  custom_component_search_tag text NOT NULL,
  owner varchar(50) NOT NULL,
  tenant_id varchar(50) NOT NULL,
  PRIMARY KEY (custom_component_id)
);

DROP TABLE IF EXISTS supported_languages;
CREATE TABLE supported_languages (
  code character varying(10) PRIMARY KEY NOT NULL, 
  name character varying(40) NOT NULL);

  /*Table structure for table `subsystem_version` */
DROP TABLE IF EXISTS subsystem_version;
DROP SEQUENCE IF EXISTS version_id_field_seq;
CREATE SEQUENCE version_id_field_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;    

CREATE TABLE subsystem_version (
  version_id integer DEFAULT nextval('version_id_field_seq'::regclass) NOT NULL,
  tenant_id character varying(50) NOT NULL,
  subsystem_name character varying(60) NOT NULL,
  date_created timestamp DEFAULT now() NOT NULL,
  version_number integer NOT NULL,
  date_modified timestamp NOT NULL,
  description character varying(255) NOT NULL ,
  createdby character varying(60) DEFAULT NULL,
  modifiedby character varying(60) DEFAULT NULL,
  subsystem_json text NOT NULL,
  PRIMARY KEY (version_id)
);

/*Table structure for table `analytics_detail` */

DROP TABLE IF EXISTS analytics_detail;
CREATE TABLE analytics_detail (
  component_id varchar(50) not null,
  workspace_id varchar(20),
  subsystem_id varchar(50),
  tenant_id varchar(20),
  user_id varchar(50),
  date_created timestamp,
  pmml_file text,
  is_validated varchar(10)
);

/*Table structure for table `transformation_detail` */

DROP TABLE IF EXISTS transformation_detail;
CREATE TABLE transformation_detail (
  message_group_id varchar(50),
  message_id varchar(50),
  tenant_id varchar(50),
  transformed_var_id varchar(20),
  dependent_var_name varchar(20),
  date_created timestamp,
  transformed_xml text,
  transformation_type character varying(50) DEFAULT NULL,
  transformation_json text DEFAULT NULL,
  PRIMARY KEY (message_id,tenant_id,transformed_var_id)
);

DROP TABLE IF EXISTS custom_parser;
CREATE TABLE custom_parser (
   custom_parser_id varchar(60) NOT NULL PRIMARY KEY,
   custom_parser_class VARCHAR(100) NOT NULL,
   config text DEFAULT NULL,
   version decimal NOT NULL DEFAULT 0
  );
  
DROP TABLE IF EXISTS udf_info;
CREATE TABLE udf_info (
   udf_id character varying(50) NOT NULL,
   udf_name character varying(50) NOT NULL,
   udf_class text NOT NULL,
   udf_args text,
   udf_param text,
   date_created date,
   date_modified date,
   isudf varchar(5),
   jar_name text,
   udf_type varchar(50),
   udf_return_type varchar(100),
   udf_desc text,
   cache_enable varchar(5),
   PRIMARY KEY (udf_id)
);

DROP TABLE IF EXISTS subsystem_connection_info;
DROP TABLE IF EXISTS component_connection;

/*Table structure for table `component_connection` */
CREATE TABLE component_connection (
  connection_id varchar(50) NOT NULL,
  connection_json text NOT NULL,
  connection_name varchar(60) NOT NULL,
  tenant_id varchar(20) NOT NULL,
  component_type varchar(60) NOT NULL,
  PRIMARY KEY (connection_id)
);

/*Table structure for table `subsystem_connection_info` */
create table subsystem_connection_info(
     connection_id varchar(50) not null,
     subsystem_name varchar(50) not null,
     id varchar(50) not null primary key,
     foreign key (connection_id)
     references component_connection(connection_id)
     ON UPDATE RESTRICT 
     ON DELETE RESTRICT
);

DROP TABLE IF EXISTS fileblob;
CREATE TABLE fileblob (
   id  SERIAL PRIMARY KEY,
   filename  text NOT NULL,
   foldername text NOT NULL,
   filestream bytea NOT NULL,
   filesize INT NOT NULL
);

DROP TABLE IF EXISTS field_data_type;
CREATE TABLE field_data_type (
    data_type_id varchar(50) NOT NULL,
    data_type_name varchar(50) NOT NULL,
    data_type_value varchar(100) NOT NULL,
    PRIMARY KEY (data_type_id)
); 

DROP TABLE IF EXISTS subsystem_history;
CREATE TABLE subsystem_history (
    subsystem_id character varying(50) NOT NULL,
    subsystem_name character varying(60) NOT NULL,
    start_time timestamp without time zone,
    up_time integer DEFAULT NULL,
    status character varying(20) NOT NULL,
    PRIMARY KEY (subsystem_id)
);

DROP TABLE IF EXISTS subsystem_history_details;
CREATE TABLE subsystem_history_details (
    subsystem_id character varying(50) NOT NULL,
    subsystem_detail_json text NOT NULL,
    PRIMARY KEY (subsystem_id)
);

ALTER TABLE subsystem_version ALTER COLUMN date_created TYPE text;
ALTER TABLE subsystem_version ALTER COLUMN date_modified TYPE text;
