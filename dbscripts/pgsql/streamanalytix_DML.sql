SET standard_conforming_strings = off;

INSERT INTO authorities (authority) VALUES ('ROLE_SUPER_USER'),('ROLE_ADVANCED_USER'),('ROLE_NORMAL_USER');

INSERT INTO channel (channel_id, subsystem_id, channel_json, channel_name, class_name, parallelism, max_spout_pending, scheme, output_fields, message_type_id, config) VALUES ('KafkaChannel',NULL,'{
    "id": "kafka-channel", 
    "name": "KafkaChannel", 
    "label": "Kafka", 
    "className": "com.streamanalytix.storm.core.spout.VajraKafkaSpout", 
    "config": [
        "topicName:sampleTopic", 
        "zkRootPath:datanode", 
        "zkID:sampleTopic", 
        "replicationFactor:1",
        "partitions:1",
        "forceFromStart:false", 
        "fetch.size:1024", 
        "buffer.size:1024"
    ], 
    "messageTypeId": "tracemessage", 
    "parallelism": 1, 
    "maxSpoutPending": 10, 
    "scheme": "com.streamanalytix.storm.core.scheme.MultiTraceMessageMultiScheme", 
    "emitStreamIds": [],
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('RabbitMQChannel',NULL,'{
    "id": "rabbitmq-channel", 
    "name": "RabbitMQChannel", 
    "label": "RabbitMQ", 
    "className": "com.streamanalytix.storm.core.spout.VajraAMQPSpout", 
    "config": [
        "queueConfig:sampleQueue", 
        "exchangeName:sampleExchange"
    ], 
    "messageTypeId": "tracemessage", 
    "parallelism": 1, 
    "maxSpoutPending": 10, 
    "scheme": "com.streamanalytix.storm.core.scheme.MultiTraceMessageScheme", 
    "emitStreamIds": [],
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('CustomChannel',NULL,'{
    "id": "custom-channel", 
    "name": "CustomChannel", 
    "label": "Custom", 
    "className": "com.streamanalytix.storm.core.spout.CustomChannel", 
    "config": [
        "channelPlugin:"
    ], 
    "messageTypeId": "tracemessage", 
    "parallelism": 1, 
    "maxSpoutPending": 10, 
    "scheme": "com.streamanalytix.storm.core.scheme.MultiTraceMessageScheme", 
    "emitStreamIds": [],
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

INSERT INTO common_message_group_field ( field_alias, index_analyzer, search_analyzer, index_tf, store , encrypt, column_family, store_in_indexer , persistence_configured, tenant_id, data_type,field_name,field_label,regex,alertable_yn ,is_input_field ,del_prefix,del_postfix) VALUES
('indexId','keyword','keyword','3','yes','false','cf1',NULL,NULL,'0','java.lang.String','indexId','indexId',NULL,'Y','N','',''),
('persistId','','','2','yes','false','cf1', NULL,NULL,'0','java.lang.String','persistId','persistId',NULL,'Y','N','',''),
('type','standard','standard','3','yes','false','cf1', NULL,NULL,'0','java.lang.String','type','type',NULL,'Y','N','','');

INSERT INTO component (component_id, subsystem_id, component_json, component_name, class_name, parallelism, task_count, output_fields, message_type_id, config, type, grouping) VALUES ('CustomProcessor',NULL,'{
    "id": "custom-processor", 
    "name": "CustomProcessor", 
    "label": "Custom", 
    "className": "com.streamanalytix.storm.core.bolt.CustomBolt", 
    "config": [
        "executorPlugin:"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL),('IndexerProcessor',NULL,'{
    "id": "index-processor", 
    "name": "IndexerProcessor", 
    "label": "Indexer", 
    "className": "com.streamanalytix.storm.core.bolt.DataIndexBolt", 
    "config": [
        "isBatchEnable: false", 
        "batchSize:1",
		"timeLimitInSeconds: 1"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL),('KafkaEmitter',NULL,'{
    "id": "kafka-emitter", 
    "name": "KafkaEmitter", 
    "label": "KafkaProducer", 
    "className": "com.streamanalytix.storm.core.bolt.KafkaProducerBolt", 
    "config": [
        "topicName:sampleTopic",
        "replicationFactor:1",
        "partitions:1",
        "producerType:sync",
        "outputFormat:json",
        "delimiter:,",
        "outputFields:tracemessage"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "emitter",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'emitter',NULL),('PersistenceProcessor',NULL,'{
    "id": "persistence-processor", 
    "name": "PersistenceProcessor", 
    "label": "Persister", 
    "className": "com.streamanalytix.storm.core.bolt.TraceMessagePersistenceBolt", 
    "config": [
        "isBatchEnable: false", 
        "batchSize:1",
		"timeLimitInSeconds: 1"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL),('PMMLProcessor',NULL,' {
    "id": "pmml-processor", 
    "name": "PMMLProcessor", 
    "label": "PMML", 
    "className": "com.streamanalytix.storm.core.bolt.PMMLProcessorBolt", 
    "config": [
        "sax.pmml.file.path:/usr/local/pmml/sample.pmml", 
        "pmml.messageName:tracemessage"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL),('RouterEmitter',NULL,'{
    "id": "router-emitter", 
    "name": "RouterEmitter", 
    "label": "Router", 
    "className": "com.streamanalytix.storm.core.bolt.RouterBolt", 
    "messageTypeId": "tracemessage", 
    "config": [], 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "emitter",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'emitter',NULL),('StreamEmitter',NULL,'{
    "id": "stream-emitter", 
    "name": "StreamEmitter", 
    "label": "Streaming", 
    "className": "com.streamanalytix.storm.core.bolt.stream.RTStreamBolt", 
    "config": [
        "rt_stream_criteria:type", 
        "zk.parent.node:/vajraRT", 
        "zk.realtime.child.node:rtStream", 
        "StreamId:rtStreamExchange", 
        "routingKey:rt"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "emitter",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'emitter',NULL),('ValidatorProcessor',NULL,'{
    "id": "validator-processor", 
    "name": "ValidatorProcessor", 
    "label": "Alert", 
    "className": "com.streamanalytix.storm.core.bolt.ValidatorBolt", 
    "config": [
        "exchangeName:alertsExchange", 
        "routingKey:alertsQueue"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL),('TimerProcessor',NULL,'{
    "id": "timer-processor", 
    "name": "TimerProcessor", 
    "label": "Timer", 
    "className": "com.streamanalytix.storm.core.bolt.TimerBolt", 
    "config": [
        "timerPlugin:", 
        "tickFrequencyInSeconds:30", 
        "tupleQueueSize:100"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL),('RabbitMQEmitter',NULL,'{
    "id": "rabbitmq-emitter",
    "name": "RabbitMQEmitter",
    "label": "RabbitMQ",
    "className": "com.streamanalytix.storm.core.bolt.RabbitMQProducerBolt",
    "config": [
        "exchangeName:sampleExchange",
        "queueConfig:sampleQueue",
        "outputFormat:json",
        "delimiter:,",
        "outputFields:tracemessage"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1,
    "taskCount": 1,
    "type": "emitter",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'emitter',NULL),('FilterProcessor',NULL,'{
    "id": "filter-processor",
    "name": "FilterProcessor",
    "label": "Filter",
    "className": "com.streamanalytix.storm.core.bolt.FilterBolt",
    "config": [
        "filterQuery:{}"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1,
    "taskCount": 1,
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL),('CustomCEPProcessor',NULL,'{
    "id": "custom-cep-processor",
    "name": "CustomCEPProcessor",
    "label": "CustomCEP",
    "className": "com.streamanalytix.storm.core.bolt.EsperBolt",
    "config": [
        "cep.query:select * from GenericEvent.win:time(2 sec) where a > 50.0",
        "cep.query.messageName:tracemessage",
        "cep.delegate:"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1,
    "taskCount": 1,
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL),('StatisticalCEPProcessor',NULL,'{
    "id": "statistical-cep-processor",
    "name": "StatisticalCEPProcessor",
    "label": "StatisticalCEP",
    "className": "com.streamanalytix.storm.core.bolt.EsperBolt",
    "config": [
        "cep.query:select * from GenericEvent.win:time(2 sec)",
        "cep.query.messageName:tracemessage",
        "cep.alias:minA,maxB,sumA,avgB",
        "cep.delegate:com.streamanalytix.storm.core.bolt.helper.TestListener"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1,
    "taskCount": 1,
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL),('DynamicCEPProcessor',NULL,'{
    "id": "dynamic-cep-processor",
    "name": "DynamicCEPProcessor",
    "label": "DynamicCEP",
    "className": "com.streamanalytix.storm.core.bolt.DynamicEsperBolt",
    "config": [
        "messageNames: tracemessage"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1,
    "taskCount": 1,
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL),('HDFSProcessor', NULL, '{
    "id": "hdfs-processor",
    "name": "HDFSProcessor",
    "label": "HDFS",
    "className": "com.streamanalytix.storm.core.bolt.HDFSBolt",
    "config": [
        "isBatchEnable:false",
        "batchSize:1",
        "hdfsPaths:{}",
        "hdfsFilePrefix:",
        "syncSize:500",
        "blockSize:67108864",
        "maxFileSize:536870912",
        "replication:1",
        "outputType:Delimited",
		"timeLimitInSeconds: 1",
        "delimiter:|^^^,^^^:^^^;^^^\\\\t",
        "compressionType:none^^^deflate^^^gzip^^^bzip2"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "processor",
    "coordinates": []
}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'processor', NULL),('DMXHProcessor',NULL,'{
    "id": "dmxh-processor", 
    "name": "DMXHProcessor", 
    "label": "DMX-h", 
    "className": "com.streamanalytix.storm.core.bolt.DMXHBolt", 
    "config": [
        "messageName:tracemessage",
        "TaskFile:"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "processor",
    "coordinates": []
}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'processor',NULL);

INSERT INTO grok_masterdata (pattern_name, pattern_value, date_created, date_modified, pattern_code) values ('User name','[a-zA-Z0-9._-]+',now(),now(),'USERNAME'),('User','%{USERNAME}',now(),now(),'USER'),('Integer Number','(?:[+-]?(?:[0-9]+))',now(),now(),'INT'),('Base 10 Number','(?<![0-9.+-])(?>[+-]?(?:(?:[0-9]+(?:\\.[0-9]+)?)|(?:\\.[0-9]+)))',now(),now(),'BASE10NUM'),('Number','(?:%{BASE10NUM})',now(),now(),'NUMBER'),('Hexa decimal number','(?<![0-9A-Fa-f])(?:[+-]?(?:0x)?(?:[0-9A-Fa-f]+))',now(),now(),'BASE16NUM'),('Float or decimal number','\\b(?<![0-9A-Fa-f.])(?:[+-]?(?:0x)?(?:(?:[0-9A-Fa-f]+(?:\\.[0-9A-Fa-f]*)?)|(?:\\.[0-9A-Fa-f]+)))\\b',now(),now(),'BASE16FLOAT'),('MULTIINT','\\b(?:[0-9][0-9]*)\\b',now(),now(),'MULTIINT'),('AWSMULTIINT','\\b(?:[0-9][0-9]*)',now(),now(),'AWSMULTIINT'),('Positive Integer','\\b(?:[1-9][0-9]*)\\b',now(),now(),'POSINT'),('Non negative Integer','\\b(?:[0-9]+)\\b',now(),now(),'NONNEGINT'),('Word or Verb','\\b\\w+\\b',now(),now(),'WORD'),('String without space','\\S+',now(),now(),'NOTSPACE'),('Space','\\s*',now(),now(),'SPACE'),('Data','.*?',now(),now(),'DATA'),('Comma','[,]',now(),now(),'COMMA'),('No Comma','.*?(?![,])+',now(),now(),'NOCOMMA'),('Greedy Data','.*',now(),now(),'GREEDYDATA'),('Quoted String','(?>(?<!\\\\)(?>\"(?>\\\\.|[^\\\\\"]+)+\"|\"\"|(?>\'(?>\\\\.|[^\\\\\']+)+\')|\'\'|(?>`(?>\\\\.|[^\\\\`]+)+`)|``))',now(),now(),'QUOTEDSTRING'),('UUID','[A-Fa-f0-9]{8}-(?:[A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}',now(),now(),'UUID'),('MAC','(?:%{CISCOMAC}|%{WINDOWSMAC}|%{COMMONMAC})',now(),now(),'MAC'),('CISCOMAC','(?:(?:[A-Fa-f0-9]{4}\\.){2}[A-Fa-f0-9]{4})',now(),now(),'CISCOMAC'),('WINDOWS MAC','(?:(?:[A-Fa-f0-9]{2}-){5}[A-Fa-f0-9]{2})',now(),now(),'WINDOWSMAC'),('COMMON MAC','(?:(?:[A-Fa-f0-9]{2}:){5}[A-Fa-f0-9]{2})',now(),now(),'COMMONMAC'),('IPV6','((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:)))(%.+)?',now(),now(),'IPV6'),('IPV4','(?<![0-9])(?:(?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[.](?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[.](?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2})[.](?:25[0-5]|2[0-4][0-9]|[0-1]?[0-9]{1,2}))(?![0-9])',now(),now(),'IPV4'),('IP','(?:%{IPV6}|%{IPV4})',now(),now(),'IP'),('HostName','\\b(?:[0-9A-Za-z][0-9A-Za-z-]{0,62})(?:\\.(?:[0-9A-Za-z][0-9A-Za-z-]{0,62}))*(\\.?|\\b)',now(),now(),'HOSTNAME'),('Host','%{HOSTNAME}',now(),now(),'HOST'),('IP Or Host','(?:%{HOSTNAME}|%{IP})',now(),now(),'IPORHOST'),('Host and port','(?:%{IPORHOST=~/\\./}:%{POSINT})',now(),now(),'HOSTPORT'),('AWSHOSTPORT','(?:%{IPORHOST}:%{POSINT})',now(),now(),'AWSHOSTPORT'),('PATH','(?:%{UNIXPATH}|%{WINPATH})',now(),now(),'PATH'),('UNIXPATH','(?>/(?>[\\w_%!$@:.,-]+|\\\\.)*)+',now(),now(),'UNIXPATH'),('TTY','(?:/dev/(pts|tty([pq])?)(\\w+)?/?(?:[0-9]+))',now(),now(),'TTY'),('Windows Path','(?>[A-Za-z]+:|\\\\)(?:\\\\[^\\\\?*]*)+',now(),now(),'WINPATH'),('URIPROTO','[A-Za-z]+(+[A-Za-z+]+)?',now(),now(),'URIPROTO'),('URIHOST','%{IPORHOST}(?::%{POSINT:port})?',now(),now(),'URIHOST'),('URIPATH','(?:/[A-Za-z0-9$.+!*\'(){},~:;=@#%_\\-]*)+',now(),now(),'URIPATH'),('URIPARAM','\\?(?:[A-Za-z0-9]+(?:=(?:[^&]*))?(?:&(?:[A-Za-z0-9]+(?:=(?:[^&]*))?)?)*)?',now(),now(),'URIPARAM'),('URIPATHPARAM','%{URIPATH}(?:%{URIPARAM})?',now(),now(),'URIPATHPARAM'),('URI','%{URIPROTO}://(?:%{USER}(?::[^@]*)?@)?(?:%{URIHOST})?(?:%{URIPATHPARAM})?',now(),now(),'URI'),('MONTH','\\b(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)\\b',now(),now(),'MONTH'),('Month in Number','(?:0?[1-9]|1[0-2])',now(),now(),'MONTHNUM'),('Month Days','(?:(?:0[1-9])|(?:[12][0-9])|(?:3[01])|[1-9])',now(),now(),'MONTHDAY'),('DAY','(?:Mon(?:day)?|Tue(?:sday)?|Wed(?:nesday)?|Thu(?:rsday)?|Fri(?:day)?|Sat(?:urday)?|Sun(?:day)?)',now(),now(),'DAY'),('YEAR','(?>\\d\\d){1,2}',now(),now(),'YEAR'),('HOUR','(?:2[0123]|[01]?[0-9])',now(),now(),'HOUR'),('MINUTE','(?:[0-5][0-9])',now(),now(),'MINUTE'),('SECOND','(?:(?:[0-5][0-9]|60)(?:[:.,][0-9]+)?)',now(),now(),'SECOND'),('TIME','(?!<[0-9])%{HOUR}:%{MINUTE}(?::%{SECOND})(?![0-9])',now(),now(),'TIME'),('DATE_US','%{MONTHNUM}[/-]%{MONTHDAY}[/-]%{YEAR}',now(),now(),'DATE_US'),('DATE_EU','%{MONTHDAY}[./-]%{MONTHNUM}[./-]%{YEAR}',now(),now(),'DATE_EU'),('ISO8601_TIMEZONE','(?:Z|[+-]%{HOUR}(?::?%{MINUTE}))',now(),now(),'ISO8601_TIMEZONE'),('ISO8601_SECOND','(?:%{SECOND}|60)',now(),now(),'ISO8601_SECOND'),('TIMESTAMP_ISO8601','%{YEAR}-%{MONTHNUM}-%{MONTHDAY}[T ]%{HOUR}:?%{MINUTE}(?::?%{SECOND})?%{ISO8601_TIMEZONE}?',now(),now(),'TIMESTAMP_ISO8601'),('GREENPLUM_TIMESTAMP','%{YEAR}-%{MONTHNUM}-%{MONTHDAY} %{HOUR}:?%{MINUTE}(?::?%{SECOND})[.]%{MULTIINT}%{SPACE}%{TZ}',now(),now(),'GREENPLUM_TIMESTAMP'),('HDFS_TIMESTAMP','%{YEAR}-%{MONTHNUM}-%{MONTHDAY} %{HOUR}:%{MINUTE}:%{SECOND},%{MULTIINT}',now(),now(),'HDFS_TIMESTAMP'),('AWS_TIMESTAMP','%{YEAR}-%{MONTHNUM}-%{MONTHDAY}[T]%{HOUR}:?%{MINUTE}(?::?%{SECOND})[.]%{AWSMULTIINT}[Z]',now(),now(),'AWS_TIMESTAMP'),('DATE','%{DATE_US}|%{DATE_EU}',now(),now(),'DATE'),('DATESTAMP','%{DATE}[- ]%{TIME}',now(),now(),'DATESTAMP'),('TZ','(?:[IPMCE][SD]T|UTC)',now(),now(),'TZ'),('DATESTAMP_RFC822','%{DAY} %{MONTH} %{MONTHDAY} %{YEAR} %{TIME} %{TZ}',now(),now(),'DATESTAMP_RFC822'),('DATESTAMP_OTHER','%{DAY} %{MONTH} %{MONTHDAY} %{TIME} %{TZ} %{YEAR}',now(),now(),'DATESTAMP_OTHER'),('SYSLOGTIMESTAMP','%{MONTH} +%{MONTHDAY} %{TIME}',now(),now(),'SYSLOGTIMESTAMP'),('PROG','(?:[\\w._/%-]+)',now(),now(),'PROG'),('SYSLOGPROG','%{PROG:program}(?:[%{POSINT:pid}])?',now(),now(),'SYSLOGPROG'),('SYSLOGHOST','%{IPORHOST}',now(),now(),'SYSLOGHOST'),('SYSLOGFACILITY','<%{NONNEGINT:facility}.%{NONNEGINT:priority}>',now(),now(),'SYSLOGFACILITY'),('HTTPDATE','%{MONTHDAY}/%{MONTH}/%{YEAR}:%{TIME} %{INT}',now(),now(),'HTTPDATE'),('QS','%{QUOTEDSTRING}',now(),now(),'QS'),('Log Level','([A-a]lert|ALERT|[T|t]race|TRACE|[D|d]ebug|DEBUG|[N|n]otice|NOTICE|[I|i]nfo|INFO|[W|w]arn?(?:ing)?|WARN?(?:ING)?|[E|e]rr?(?:or)?|ERR?(?:OR)?|[C|c]rit?(?:ical)?|CRIT?(?:ICAL)?|[F|f]atal|FATAL|[S|s]evere|SEVERE|EMERG(?:ENCY)?|[Ee]merg(?:ency)?)',now(),now(),'LOGLEVEL'),('Double Quot','[\"]',now(),now(),'QUOT');

INSERT INTO message (message_id, type, delimiter, message_name, createddt, updateddt, message_type, is_multiline_required, is_multiline_negate, multiline_regex, timestamp_format, index_row_key, persistence_row_key, TENANT_ID, timestamp_column, group_id, strict_validation, custom_parser_id, log_message) VALUES 
('4838b00d-097f-4d07-9c89-87f96e01baca','json','','selfTestability',now(),now(),'custom','N','N','','STANDARD','com.streamanalytix.core.persistence.keygen.UUIDGenerator','com.streamanalytix.core.persistence.keygen.UUIDGenerator','0','timestamp','0',NULL,NULL,NULL),
('4838b00d-097f-4d07-9c89-87f96e01ba78','json','','alertMessage',now(),now(),'custom','N','N','','STANDARD','com.streamanalytix.core.persistence.keygen.UUIDGenerator','com.streamanalytix.core.persistence.keygen.UUIDGenerator','0','timestamp','0',NULL,NULL,NULL),
('21c37a7c-d374-46f9-b55f-b95e48a2918b', 'custom', '|', 'saxDFMonMsg', now(), NULL, 'custom', 'N', 'N', '', 'STANDARD', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', '0', 'timestamp', '0', 'false', 'f31ce8cf-8580-4ba3-a28a-6b3ab9d23d57', NULL),
('8cedf074-d70d-47eb-9d47-94d6731fd495', 'custom', '|', 'software-jmx', now(), NULL, 'custom', 'N', 'N', '', 'STANDARD', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', '0', 'timestamp', '0', 'false', 'f31ce8cf-8580-4ba3-a28a-6b3ab9d23d57', NULL),
('3b81cb56-ef61-4114-8312-c1ad3d058e15', 'custom', '|', 'infrastructure', now(), NULL, 'custom', 'N', 'N', '', 'STANDARD', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', '0', 'timestamp', '0', 'false', 'f31ce8cf-8580-4ba3-a28a-6b3ab9d23d57', NULL),
('b181cb56-ef61-4114-8092-e0cde1058d09', 'custom', '|', 'infrastructure-disk', now(), NULL, 'custom', 'N', 'N', '', 'STANDARD', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', '0', 'timestamp', '0', 'false', 'f31ce8cf-8580-4ba3-a28a-6b3ab9d23d57', NULL),
('a981cb54-fe61-ea14-8312-ca3d3d087e54', 'custom', '|', 'infrastructure-network', now(), NULL, 'custom', 'N', 'N', '', 'STANDARD', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', '0', 'timestamp', '0', 'false', 'f31ce8cf-8580-4ba3-a28a-6b3ab9d23d57', NULL);

INSERT INTO message_field ( message_field_id, field_name, field_label, regex, field_order,alertable_yn, is_input_field, del_prefix, del_postfix, message_id, group_field_id ) VALUES 
('123eecf7-9b1f-4321-a5b3-93f5b2d4db12','indexId','Unique Id',NULL,2,'N','N',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01baca','589eecf7-9b1f-4321-a5b3-93f5b2d4db41'),
('234b5c1b-7075-4294-8698-63568a58c623','timestamp','timestamp',NULL,4,'Y','Y',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01baca','692b5c1b-7075-4294-8698-63568a58c698'),
('34504c95-b7ab-4371-8a92-c5c30e8eb734','persistId','persistId',NULL,3,'N','N',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01baca','70104c95-b7ab-4371-8a92-c5c30e8eb723'),
('45672b71-bfe6-491a-842f-070b0e8f8645','b','B',NULL,1,'Y','Y',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01baca','26572b71-bfe6-491a-842f-070b0e8f8662'),
('5670913d-95ae-4953-ac71-43578989b556','a','A',NULL,0,'Y','Y',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01baca','6060913d-95ae-4953-ac71-43578989b518'),
('6770913d-35be-3943-bc72-23478969b567','type','Type',NULL,5,'N','N',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01baca','8070913d-35be-3943-bc72-23478969b566'),
('dc4e64f8-bd47-4bc9-9c91-2dbce97b3fb1', 'indexId', 'indexId', NULL,0, 'Y', 'N', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '589eecf7-9b1f-4321-a5b3-93f5b2d4db41'),
('e3a583d3-865e-4dd7-ba51-00ba882ed933', 'persistId', 'persistId', NULL,1, 'Y', 'N', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '70104c95-b7ab-4371-8a92-c5c30e8eb723'),
('a08aa0d2-bd72-4b63-acec-dfbd1d85fefd', 'type', 'type', NULL,2, 'Y', 'N', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '8070913d-35be-3943-bc72-23478969b566'),
('f9f04cf2-4cd6-4ee9-9214-ab4d8f4805ba', 'subsystemName', 'subsystemName', NULL,3, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '0d3c021f-93b8-4c0d-b595-8c1eae041654'),
('a906b2db-f86e-437d-ba56-e18a05b524e8', 'componentType', 'componentType', NULL,4, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'e206c6d0-a484-4b6f-a18d-4b0698771504'),
('62e2cc94-671b-4c17-9b76-fc28ec518a97', 'componentId', 'componentId', NULL,5, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'd9ead6d4-2adf-499b-8919-d7234baaeaf4'),
('b3705b1f-a297-498b-abd1-096804bd2470', 'hostName', 'hostName', NULL,6, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'f3fddb35-5eb9-4f0f-ab68-79e0da81be70'),
('e3272b16-7068-4368-9206-5606a72cab41', 'processId', 'processId', NULL,7, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'd79ca6cf-2834-4966-a85d-01647532909c'),
('eebcbe2d-d821-492c-b5eb-cf2fb9dcc203', 'status', 'status', NULL,8, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '65ebbe73-c2b3-4b9c-aa70-61bd824ca04b'),
('06ba64cc-46b6-48ec-ab89-df860f96e58d', 'timestamp', 'timestamp', NULL,9, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '692b5c1b-7075-4294-8698-63568a58c698'),
('c5e89886-7270-415b-9f34-fd857e8f149b', 'timercount', 'timercount', NULL,10, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '62f4e84f-4b2a-4a2b-b338-fa28c3f1676d'),
('38faa10f-0134-4888-a9bf-bd01fd9c20cd', 'timerm15rate', 'timerm15rate', NULL,11, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'dc0a56a9-1d1e-4652-988d-0faa13492320'),
('700fb72d-032b-48e5-a0b4-4d6c3642359c', 'timerm1rate', 'timerm1rate', NULL,12, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '90187377-c336-4ff1-a3f1-8e6fd1746583'),
('14fe383d-9218-4169-997e-8ab73e5a9c32', 'timerm5rate', 'timerm5rate', NULL,13, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '3a09499c-41d3-49c2-b7aa-d53320e0c925'),
('4911a1bc-53c0-4105-812d-bdf06730bc7a', 'timermax', 'timermax', NULL,14, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '9e2ed4c4-6179-451e-8982-f0d7dcb8f498'),
('9d59e1d6-3d3b-42f1-a006-cde7b4a0f594', 'timermean', 'timermean', NULL,15, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '6736b287-1aec-4aa1-a3cc-34afc43f193b'),
('79d26f57-5b5c-4c91-9a16-eada98637039', 'timermeanrate', 'timermeanrate', NULL,16, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'c6e56355-1042-41f1-92d7-7abc569c1d38'),
('0925aeb8-0764-4848-aef3-641be250f493', 'timermin', 'timermin', NULL,17, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '00886dd8-2b51-4ffc-b9b6-50b7835357c6'),
('19412fce-6ec9-4887-b0f1-2567dc113572', 'timerp50', 'timerp50', NULL,18, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'b0191140-83be-47f3-8e4e-66201d979303'),
('74bd0bde-1d2e-4897-ad29-544097a3750b', 'timerp75', 'timerp75', NULL,19, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'c8226731-d52a-4e05-a7ae-781a8e143e80'),
('d2772dba-eb97-4c6a-a009-74f5a0241156', 'timerp95', 'timerp95', NULL,20, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '342c48e8-d0d1-40d6-add7-542fa24124f1'),
('06d5aa11-eaeb-47d7-babb-dcd92a805d58', 'timerp98', 'timerp98', NULL,21, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '257b8365-0806-48cb-8227-ab9afd06c84f'),
('216011e3-fb25-463e-8f0d-8ee20bce677e', 'timerp99', 'timerp99', NULL,22, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '27c4e6b2-f7ba-45b8-97e7-62a271e57716'),
('0c571e3e-6b74-4cae-a57c-418a8f38b346', 'timerp999', 'timerp999', NULL,23, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '961909d9-766b-45b6-963f-ab106d91e147'),
('8e910394-035e-4c07-930c-50974835d796', 'timerstddev', 'timerstddev', NULL,24, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '5d3bd234-0772-4c5a-97c7-ec62527f9131'),
('a21cca7a-abe2-4783-a707-482fff65a04a', 'envFailurecount', 'envFailurecount', NULL,25, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'ecfe0057-9677-4838-93a9-d3a4566899db'),
('7f952cc2-44d0-410f-8fe7-6f9bfe809d43', 'envFailurem1rate', 'envFailurem1rate', NULL,26, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'feddc96d-f3e6-4292-96e0-8225a8909af8'),
('3f490b95-2e38-4ff1-b73f-f5ef6b94f1d0', 'envFailurem5rate', 'envFailurem5rate', NULL,27, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'ab5548f4-d4da-421c-bb21-f8d6246b4693'),
('2c31af18-1ae4-47ba-acd9-28d708d8a183', 'envFailurem15rate', 'envFailurem15rate', NULL,28, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '62d3a564-5270-45fe-82ad-e9826ce00b45'),
('c53fe6c1-d069-454b-becc-21840a7bd52f', 'envFailuremeanrate', 'envFailuremeanrate', NULL,29, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '46a6dcb6-9b3c-456a-8975-fde9d4c891ee'),
('51ff3d2d-4de8-4e3a-b112-125da72f1d14', 'businessFailurecount', 'businessFailurecount', NULL,30, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '7e79aab9-2599-4022-98d9-417e46d994e2'),
('ec5b35d1-1be6-44b6-a190-987c03ae845d', 'businessFailurem1rate', 'businessFailurem1rate', NULL,31, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', 'b6af2f96-f46a-4a25-bfc6-dd2111e8c2e4'),
('43332a99-d781-4d4d-b78b-9b4d7113673f', 'businessFailurem5rate', 'businessFailurem5rate', NULL,32, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '6e555843-57f9-4187-b9d5-8d38312f0d82'),
('51dd70e8-156d-4b48-b9b7-fc484c07cde2', 'businessFailurem15rate', 'businessFailurem15rate', NULL,33, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '343d2a82-29ca-4357-8848-2e4c649573f1'),
('05a6d36f-fae1-4ed3-a53b-b86190a9c95c', 'businessFailuremeanrate', 'businessFailuremeanrate', NULL,34, 'Y', 'Y', NULL, NULL, '21c37a7c-d374-46f9-b55f-b95e48a2918b', '436c2f9b-140a-4d0c-b0b7-48e36cbaa6a0'),
('75e63a25-e975-4cfa-b47d-946d9174a539', 'indexId', 'indexId', NULL,0, 'Y', 'N', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '589eecf7-9b1f-4321-a5b3-93f5b2d4db41'),
('6db7b03e-c829-4e81-8fd1-d48ea57dbf75', 'persistId', 'persistId', NULL,1, 'Y', 'N', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '70104c95-b7ab-4371-8a92-c5c30e8eb723'),
('853f1efa-50c5-4e33-9a3f-220d06db9e4e', 'type', 'type', NULL,2, 'Y', 'N', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '8070913d-35be-3943-bc72-23478969b566'),
('a7d4422e-ba36-4adf-a492-aaf7b2bb87cf', 'timestamp', 'Timestamp', NULL,3, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '692b5c1b-7075-4294-8698-63568a58c698'),
('9b82c6ef-90bf-4560-96ec-ca421ebcffc1', 'indexId', 'indexId', NULL,0, 'Y', 'N', NULL, NULL, '3b81cb56-ef61-4114-8312-c1ad3d058e15', '589eecf7-9b1f-4321-a5b3-93f5b2d4db41'),
('81f177ca-1acd-4e1a-baf1-d91118f5af55', 'persistId', 'persistId', NULL,1, 'Y', 'N', NULL, NULL, '3b81cb56-ef61-4114-8312-c1ad3d058e15', '70104c95-b7ab-4371-8a92-c5c30e8eb723'),
('7eec19f3-7a4b-4ad2-8303-24738cf90d70', 'type', 'type', NULL,2, 'Y', 'N', NULL, NULL, '3b81cb56-ef61-4114-8312-c1ad3d058e15', '8070913d-35be-3943-bc72-23478969b566'),
('edfbea95-abc9-4fad-9c45-8e332b234d49', 'timestamp', 'Timestamp', NULL,3, 'Y', 'Y', NULL, NULL, '3b81cb56-ef61-4114-8312-c1ad3d058e15', '692b5c1b-7075-4294-8698-63568a58c698'),
('016f473d-000a-4473-bb48-a39ed63a4f84','swapused','used swap','',24,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','97e2e5da-3fb3-4ac3-ba2d-23852da54839'),
('03539e7b-63dd-4614-b60f-7fdf9cd4b5dd','memorytotal','total memory','',14,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','8d123f60-61bc-4702-b6cd-d8a36f2e4761'),
('0ce454f7-7f10-4577-8bf0-4524580de4da','cpuidle','idle CPU','',5,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','eb0b8e48-c434-4b25-9dd9-2fa0394701b7'),
('22c88d67-5223-4b0b-82cd-b418746b883e','cpunice','nice CPU','',8,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','ebcb8213-81f4-4cbc-a415-329b4cab0e68'),
('39772849-ef7f-454c-86be-bac08d2be47b','memoryfree','free memory','',16,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','2805fcc8-7d53-4249-9b69-549347569e02'),
('4e7d0ae0-07b2-407b-ab23-b2aaa1b746d6','swappageIn','swap page in','',22,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','4c73ad44-801b-49b9-8cc7-d9fea22a7446'),
('503fb2ed-6053-41fb-8bca-d257b62b323d','memoryused','used memory','',18,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','9714b6bc-4e16-45ed-abf6-c4e21edff090'),
('58942190-eae6-4419-a24c-0e14f131be0e','cpusys','system CPU','',6,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','70e94bb6-90b1-4b04-93b8-149303110697'),
('641ebfb3-ff22-4f57-8975-8178c965797f','swappageOut','swap page out','',23,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','03ee0b12-2c37-490a-a556-10fc739e7cad'),
('7606d48c-159a-490c-9084-c75fa5f38269','memoryfreePercentage','memory free percentage','',19,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','889a8a30-7815-4fa3-9c68-a46c3de4e134'),
('760994b7-ed70-451e-b4ae-715ba27925fa','memoryusedPercentage','memory used percentage','',17,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','59681d67-f983-4717-a618-45e448bd6d24'),
('80abab71-f804-4a72-a734-364d0ede76dc','cpuwait','wait CPU','',9,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','b590d28e-605a-4b2d-8147-859b5e5cf2de'),
('81d64bab-32dd-40ea-a8ff-71b787916c4c','host','hostname','',4,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','d173f9fd-1a06-4742-afcd-f6ce2e93c53b'),
('bf19bb16-9b74-4e96-8c65-a8f644e7ce68','memoryactualUsed','actual memory used','',13,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','33a814c3-f3e9-4dc7-bc98-a6f71818885e'),
('cbec1cdd-6992-4776-9420-ef02d884ba0d','cpustolen','stolen CPU','',11,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','34a6a468-6ef0-4288-8fec-6828bb68c0a5'),
('cfaadc1f-37d0-46d0-b288-0214d440dae8','cpuuser','user CPU','',10,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','9227eec2-c96d-432b-8732-eb58e20774d6'),
('d3430c6f-8ade-4180-bc30-d28f255ee8f2','swapfree','free swap','',21,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','230a64ee-2d0a-47da-8acb-d416eeda661a'),
('e5d4daf3-d8f7-4f7a-9fcc-c3c02ee3a049','memoryactualFree','memory actual free','',15,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','f4e14f55-3b95-4398-9806-3c74595c997a'),
('ebce580b-82bd-4454-9eaa-f45a51079349','swaptotal','total swap','',20,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','b94e76c4-b27f-45d7-9c6d-06ae2e389435'),
('ed4a7f2f-41a7-490c-adeb-c7659e0dfaaa','cpusoftIrq','soft request CPU','',7,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','2bcbb5db-692b-437b-ac23-852e929687e3'),
('f23428c6-1023-4f99-a0e9-80e24e708bcb','cpucombined','combined CPU','',12,'Y','Y','','','3b81cb56-ef61-4114-8312-c1ad3d058e15','9f9c4846-5fdb-41ad-b673-a9b26d3a70aa'),
('123eecf7-9b1f-4321-a5b3-93f5b2d4db78','indexId','Unique Id',NULL,2,'N','N',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01ba78','589eecf7-9b1f-4321-a5b3-93f5b2d4db41'),
('234b5c1b-7075-4294-8698-63568a58c678','timestamp','timestamp',NULL,4,'Y','Y',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01ba78','692b5c1b-7075-4294-8698-63568a58c698'),
('34504c95-b7ab-4371-8a92-c5c30e8eb778','persistId','persistId',NULL,3,'N','N',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01ba78','70104c95-b7ab-4371-8a92-c5c30e8eb723'),
('45672b71-bfe6-491a-842f-070b0e8f8678','alertConfigId','B',NULL,1,'Y','Y',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01ba78','26572b71-bfe6-491a-842f-070b0e8f8678'),
('5670913d-95ae-4953-ac71-43578989b578','data','A',NULL,0,'Y','Y',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01ba78','6060913d-95ae-4953-ac71-43578989b578'),
('6770913d-35be-3943-bc72-23478969b578','type','Type',NULL,5,'N','N',NULL,NULL,'4838b00d-097f-4d07-9c89-87f96e01ba78','8070913d-35be-3943-bc72-23478969b566'),
('9b82c6ef-90bf-4560-12ec-ca421ebcffc2', 'indexId', 'indexId', NULL,0, 'Y', 'N', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', '589eecf7-9b1f-4321-a5b3-93f5b2d4db41'),
('81f177ca-1acd-4e1a-13f1-d91118f5af54', 'persistId', 'persistId', NULL,1, 'Y', 'N', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', '70104c95-b7ab-4371-8a92-c5c30e8eb723'),
('7eec19f3-7a4b-4ad2-1403-24738cf90d78', 'type', 'type', NULL,2, 'Y', 'N', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', '8070913d-35be-3943-bc72-23478969b566'),
('edfbea95-abc9-4fad-1545-8e332b234d43', 'timestamp', 'Timestamp', NULL,3, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', '692b5c1b-7075-4294-8698-63568a58c698'),
('9b82c6ef-90bf-4560-16ec-ca421ebcffc2', 'indexId', 'indexId', NULL,0, 'Y', 'N', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', '589eecf7-9b1f-4321-a5b3-93f5b2d4db41'),
('81f177ca-1acd-4e1a-17f1-d91118f5af53', 'persistId', 'persistId', NULL,1, 'Y', 'N', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', '70104c95-b7ab-4371-8a92-c5c30e8eb723'),
('7eec19f3-7a4b-4ad2-1803-24738cf90d77', 'type', 'type', NULL,2, 'Y', 'N', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', '8070913d-35be-3943-bc72-23478969b566'),
('edfbea95-abc9-4fad-1945-8e332b234d44', 'timestamp', 'Timestamp', NULL,3, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', '692b5c1b-7075-4294-8698-63568a58c698');

INSERT INTO message_group (group_id, group_name, index_numberof_shards, index_replication_factor, across_field_search_enabled, tenant_id, timestamp_alias, routing_required, routing_policy, index_source, index_expression, persistence_expression, indexing_enabled, persistence_enabled, hbase_region_boundaries, regionsplit, compression) VALUES ('0', 'superuserGroup', '2', '1', 'true', '0', 'timestamp', 'false', null, 'false', '''superusergroup''', '''superusergroup''', null, null, null, null, 'false');

INSERT INTO message_group_field (group_field_id, field_alias, index_analyzer, search_analyzer, index_tf, store, encrypt, column_family, store_in_indexer, persistence_configured, group_id, tenant_id, data_type, is_common) VALUES
('589eecf7-9b1f-4321-a5b3-93f5b2d4db41','indexId','keyword','keyword','3','yes','false','cf1',NULL,NULL,'0','0','java.lang.String','Y'),
('692b5c1b-7075-4294-8698-63568a58c698','timestamp','standard','standard','3','yes','false','cf1',NULL,NULL,'0','0','java.lang.String','N'),
('70104c95-b7ab-4371-8a92-c5c30e8eb723','persistId','','','2','yes','false','cf1', NULL,NULL,'0','0','java.lang.String','Y'),
('26572b71-bfe6-491a-842f-070b0e8f8662','b','standard','standard','3','yes','false','cf1',NULL,NULL,'0','0','java.lang.Double','N'),
('6060913d-95ae-4953-ac71-43578989b518','a','standard','standard','3','yes','false','cf1',NULL,NULL,'0','0','java.lang.Double','N'),
('8070913d-35be-3943-bc72-23478969b566','type','standard','standard','3','yes','false','cf1', NULL,NULL,'0','0','java.lang.String','Y'),
('f9c19f9c-b25b-4c1c-bd85-d5d563aff80d', 'indexId', 'keyword', 'keyword', '3', 'yes', 'false', 'cf1', 'yes', NULL, '0', '0', 'java.lang.String', 'Y'),
('df47e1d0-1ed5-483a-a5d1-68ea0fda9bea', 'persistId', '', '', '2', 'yes', 'false', 'cf1', 'yes', NULL, '0', '0', 'java.lang.String', 'Y'),
('bab33fc0-87ca-4a5e-8a3c-a14c1c7bbfdb', 'type', 'standard', 'standard', '3', 'yes', 'false', 'cf1', 'yes', NULL, '0', '0', 'java.lang.String', 'Y'),
('0d3c021f-93b8-4c0d-b595-8c1eae041654', 'subsystemName', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('e206c6d0-a484-4b6f-a18d-4b0698771504', 'componentType', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('d9ead6d4-2adf-499b-8919-d7234baaeaf4', 'componentId', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('f3fddb35-5eb9-4f0f-ab68-79e0da81be70', 'hostName', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('d79ca6cf-2834-4966-a85d-01647532909c', 'processId', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('65ebbe73-c2b3-4b9c-aa70-61bd824ca04b', 'status', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('5bcebd00-2d33-431f-94fb-c9b7b3f58140', 'timestamp', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('62f4e84f-4b2a-4a2b-b338-fa28c3f1676d', 'timercount', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('dc0a56a9-1d1e-4652-988d-0faa13492320', 'timerm15rate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('90187377-c336-4ff1-a3f1-8e6fd1746583', 'timerm1rate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('3a09499c-41d3-49c2-b7aa-d53320e0c925', 'timerm5rate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('9e2ed4c4-6179-451e-8982-f0d7dcb8f498', 'timermax', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('6736b287-1aec-4aa1-a3cc-34afc43f193b', 'timermean', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('c6e56355-1042-41f1-92d7-7abc569c1d38', 'timermeanrate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('00886dd8-2b51-4ffc-b9b6-50b7835357c6', 'timermin', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('b0191140-83be-47f3-8e4e-66201d979303', 'timerp50', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('c8226731-d52a-4e05-a7ae-781a8e143e80', 'timerp75', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('342c48e8-d0d1-40d6-add7-542fa24124f1', 'timerp95', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('257b8365-0806-48cb-8227-ab9afd06c84f', 'timerp98', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('27c4e6b2-f7ba-45b8-97e7-62a271e57716', 'timerp99', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('961909d9-766b-45b6-963f-ab106d91e147', 'timerp999', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('5d3bd234-0772-4c5a-97c7-ec62527f9131', 'timerstddev', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('687ddd0f-0c15-4dd0-ab28-ea5094bd41a5', 'subsystemType', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('ecfe0057-9677-4838-93a9-d3a4566899db', 'envFailurecount', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('feddc96d-f3e6-4292-96e0-8225a8909af8', 'envFailurem1rate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('ab5548f4-d4da-421c-bb21-f8d6246b4693', 'envFailurem5rate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('62d3a564-5270-45fe-82ad-e9826ce00b45', 'envFailurem15rate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('46a6dcb6-9b3c-456a-8975-fde9d4c891ee', 'envFailuremeanrate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('7e79aab9-2599-4022-98d9-417e46d994e2', 'businessFailurecount', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('b6af2f96-f46a-4a25-bfc6-dd2111e8c2e4', 'businessFailurem1rate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('6e555843-57f9-4187-b9d5-8d38312f0d82', 'businessFailurem5rate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('343d2a82-29ca-4357-8848-2e4c649573f1', 'businessFailurem15rate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('436c2f9b-140a-4d0c-b0b7-48e36cbaa6a0', 'businessFailuremeanrate', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.String', 'N'),
('5fdb5fb6-099c-4444-a0ff-1b043b56625f', 'A', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.Double', 'N'),
('d1cb1f60-85db-4a19-b192-d94640f89057', 'B', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '0', '0', 'java.lang.Double', 'N'),
('03ee0b12-2c37-490a-a556-10fc739e7cad','swappageOut','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('230a64ee-2d0a-47da-8acb-d416eeda661a','swapfree','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('2805fcc8-7d53-4249-9b69-549347569e02','memoryfree','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('2bcbb5db-692b-437b-ac23-852e929687e3','cpusoftIrq','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('33a814c3-f3e9-4dc7-bc98-a6f71818885e','memoryactualUsed','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('34a6a468-6ef0-4288-8fec-6828bb68c0a5','cpustolen','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('4c73ad44-801b-49b9-8cc7-d9fea22a7446','swappageIn','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('59681d67-f983-4717-a618-45e448bd6d24','memoryusedPercentage','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('70e94bb6-90b1-4b04-93b8-149303110697','cpusys','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('889a8a30-7815-4fa3-9c68-a46c3de4e134','memoryfreePercentage','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('8d123f60-61bc-4702-b6cd-d8a36f2e4761','memorytotal','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('9227eec2-c96d-432b-8732-eb58e20774d6','cpuuser','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('9714b6bc-4e16-45ed-abf6-c4e21edff090','memoryused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('97e2e5da-3fb3-4ac3-ba2d-23852da54839','swapused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('9f9c4846-5fdb-41ad-b673-a9b26d3a70aa','cpucombined','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('b590d28e-605a-4b2d-8147-859b5e5cf2de','cpuwait','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('b94e76c4-b27f-45d7-9c6d-06ae2e389435','swaptotal','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d173f9fd-1a06-4742-afcd-f6ce2e93c53b','host','standard','standard','3','no','false','','yes','','0','0','java.lang.String','N'),
('eb0b8e48-c434-4b25-9dd9-2fa0394701b7','cpuidle','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('ebcb8213-81f4-4cbc-a415-329b4cab0e68','cpunice','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('f4e14f55-3b95-4398-9806-3c74595c997a','memoryactualFree','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('26572b71-bfe6-491a-842f-070b0e8f8678','alertConfigId','standard','standard','3','yes','false','cf1',NULL,NULL,'0','0','java.lang.String','N'),
('6060913d-95ae-4953-ac71-43578989b578','data','standard','standard','3','yes','false','cf1',NULL,NULL,'0','0','java.lang.String','N');

INSERT INTO message_template (template_id, template_name, pattern_name, parser_type, is_multiline_required, is_multiline_negate, multiline_regex, timestamp_format) VALUES ('custom','Custom','custom',NULL,NULL,NULL,NULL,'STANDARD');

INSERT INTO terms (agreement_accepted) VALUES ('false');

INSERT INTO subsystem_info (subsystem_id, subsystem_json, subsystem_name, config, worker_count, ackers_count, defaultfallback, scheduling, status, customJarStatus, failedReason, tenant_id) VALUES ('AlertSubsystem','{
    "id": "AlertSubsystem",
    "name": "AlertSubsystem",
    "status": "inactive",
    "config": null,
    "workerCount": "1",
    "ackersCount": "1",
    "scheduling": null,
    "spouts": [
        {
            "id": "rabbitmq",
            "name": "RabbitmqChannel_1",
            "label": "RabbitMQ",
            "className": "com.streamanalytix.storm.core.spout.VajraAMQPSpout",
            "config": [
                "queueConfig:alertsQueue",
                "exchangeName:alertsExchange"
            ],
            "messageTypeId": "tracemessage",
            "parallelism": "1",
            "maxSpoutPending": "10",
            "scheme": "com.streamanalytix.storm.core.scheme.AlertTraceMessageScheme",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "AlertConsumerProcessor_1"
                }
            ],
            "channelType": "single",
            "messageTypes": [
                "alertsTraceMessage"
            ],
            "coordinates": []
        }
    ],
    "bolts": [
        {
            "id": "alert-consumer-processor",
            "name": "AlertConsumerProcessor_1",
            "label": "AlertConsumer",
            "className": "com.streamanalytix.storm.core.bolt.AlertsConsumerBolt",
            "config": [],
            "messageTypeId": "tracemessage",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage",
                        "alertDfnId"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "AlertAggregatorProcessor_2"
                }
            ],
            "groupings": [
                {
                    "type": "shuffle",
                    "componentId": "RabbitmqChannel_1",
                    "groupFields": [],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": "1",
            "taskCount": "1",
            "type": "processor",
            "channelType": "single",
            "messageTypes": [
                "alertsTraceMessage"
            ],
            "coordinates": []
        },
        {
            "id": "alert-aggregator-processor",
            "name": "AlertAggregatorProcessor_2",
            "label": "AlertAggregator",
            "className": "com.streamanalytix.storm.core.bolt.AlertEsperBolt",
            "config": [],
            "messageTypeId": "tracemessage",
            "emitStreamIds": [],
            "groupings": [
                {
                    "type": "fields",
                    "componentId": "AlertConsumerProcessor_1",
                    "groupFields": [
                        "alertDfnId"
                    ],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": "1",
            "taskCount": "1",
            "type": "processor",
            "channelType": "single",
            "messageTypes": [
                "alertsTraceMessage"
            ],
            "coordinates": []
        }
    ]
}','AlertSubsystem',NULL,NULL,NULL,NULL,NULL,'inactive', 'NA', 'NA','0'),('GraphiteWriterSubsystem', '{
    "id": "GraphiteWriterSubsystem",
    "name": "GraphiteWriterSubsystem",
    "status": "inactive",
    "config": null,
    "workerCount": "1",
    "ackersCount": "1",
    "scheduling": null,
    "spouts": [
        {
            "id": "rabbitmq-channel",
            "name": "Rabbitmq_334142",
            "label": "RabbitMQ",
            "className": "com.streamanalytix.storm.core.spout.VajraAMQPSpout",
            "config": [
                "queueConfig:saxGraphiteQ",
                "exchangeName:saxGraphiteEx"
            ],
            "messageTypeId": "tracemessage",
            "parallelism": "1",
            "maxSpoutPending": "10",
            "scheme": "com.streamanalytix.model.parser.MonitoringDataParser",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "Graphite_121664"
                }
            ],
            "channelType": "custom",
            "messageTypes": [
                "saxDFMonMsg",
                "infrastructure",
                "infrastructure-disk",
                "infrastructure-network",
                "software-jmx"
            ]
        }
    ],
    "bolts": [
        {
            "id": "graphite-processor",
            "name": "Graphite_121664",
            "label": "Graphite",
            "className": "com.streamanalytix.storm.core.bolt.GraphiteBolt",
            "messageTypeId": "tracemessage",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "Alert_269424"
                }
            ],
            "groupings": [
                {
                    "type": "shuffle",
                    "componentId": "Rabbitmq_334142",
                    "groupFields": [],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": 1,
            "taskCount": 1,
            "type": "processor",
            "channelType": "custom",
            "messageTypes": [
                "saxDFMonMsg",
                "infrastructure",
                "infrastructure-disk",
                "infrastructure-network",
                "software-jmx"
            ]
        },
        {
            "id": "validator-processor",
            "name": "Alert_269424",
            "label": "Alert",
            "className": "com.streamanalytix.storm.core.bolt.ValidatorBolt",
            "config": [
                "exchangeName:alertsExchange",
                "routingKey:alertsQueue"
            ],
            "messageTypeId": "tracemessage",
            "emitStreamIds": [],
            "groupings": [
                {
                    "type": "shuffle",
                    "componentId": "Graphite_121664",
                    "groupFields": [],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": 1,
            "taskCount": 1,
            "type": "processor",
            "channelType": "custom",
            "messageTypes": [
                "saxDFMonMsg",
                "infrastructure",
                "infrastructure-disk",
                "infrastructure-network",
                "software-jmx"
            ]
        }
    ]
}', 'GraphiteWriterSubsystem', NULL, NULL, NULL, NULL, NULL, 'inactive', 'NA', 'NA', '0'),('MonitorMetricSubsystem', '{
    "id": "MonitorMetricSubsystem",
    "name": "MonitorMetricSubsystem",
    "status": "inactive",
    "config": null,
    "workerCount": "1",
    "ackersCount": "1",
    "scheduling": null,
    "spouts": [
        {
            "id": "rabbitmq-channel",
            "name": "Rabbitmq_867064",
            "label": "RabbitMQ",
            "className": "com.streamanalytix.storm.core.spout.VajraAMQPSpout",
            "config": [
                "queueConfig:saxMetricQ",
                "exchangeName:saxMetricEX"
            ],
            "messageTypeId": "tracemessage",
            "parallelism": "1",
            "maxSpoutPending": "10",
            "scheme": "com.streamanalytix.model.parser.MonitoringDataParser",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "Timer_529542"
                }
            ],
            "channelType": "custom",
            "messageTypes": [
                "saxDFMonMsg",
                "infrastructure",
                "software-jmx"
            ]
        }
    ],
    "bolts": [
        {
            "id": "timer-processor",
            "name": "Timer_529542",
            "label": "Timer",
            "className": "com.streamanalytix.storm.core.bolt.TimerBolt",
            "config": [
                "timerPlugin:com.streamanalytix.storm.core.bolt.GraphiteTimerImpl",
                "tickFrequencyInSeconds:60",
                "tupleQueueSize:100"
            ],
            "messageTypeId": "tracemessage",
            "emitStreamIds": [],
            "groupings": [
                {
                    "type": "shuffle",
                    "componentId": "Rabbitmq_867064",
                    "groupFields": [],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": "1",
            "taskCount": "1",
            "type": "processor",
            "channelType": "custom",
            "messageTypes": [
                "saxDFMonMsg",
                "infrastructure",
                "software-jmx"
            ]
        }
    ]
}', 'MonitorMetricSubsystem', NULL, NULL, NULL, NULL, NULL, 'inactive', 'NA', 'NA', '0');

INSERT INTO tenant (tenant_id, parent_tenant_id, tenant_name, delete_yn, tenant_supervisors,date_created) VALUES ('0','0','superuser','Y','',now()), ('1','0','admin','N','',now());

INSERT INTO user_tenant_association (association_id, user_id, tenant_id, date_created) VALUES (0, 'superuser', '0', now()), (1,'admin','1',now());

INSERT INTO users (username, password, email_id, enabled, tenant_id, delete_yn) VALUES ('superuser','Ru3zPH5pFOrDNbyLRhNYLg==','sax-support@impetus.com','Y','0','N'), ('admin','tbE/HsuPW+/DNbyLRhNYLg==','sax-support@impetus.com','Y','1','N');

INSERT INTO users_authorities (username, authority) VALUES ('superuser','ROLE_SUPER_USER'), ('admin','ROLE_ADVANCED_USER');

INSERT INTO user_token (user_token_id, token, username, tenant_id, time_to_live, createdt) VALUES ('532f2d6d-f414-4235-a6f4-11cdad4f1541', 'Bn9hCIWR+D2QeS2yVRqVXj1eH9bE+IXhYkJLmuLUt0uTqNsGBP3EKQ==', 'superuser', '0', '365', now()), ('82dce51d-178b-4b80-aeb2-9bdda4687c42', '8q2EuQSY1NDE5RRlS0r8RGoajvRnv19CB82IBfVZWH5IjTf4azkxLw==', 'admin', '1', '365', now());

INSERT INTO workflow (WORKFLOW_ID, WORKFLOW_NAME, WORKFLOW_TYPE, WORKFLOW_PARAM, DATE_CREATED, DATE_MODIFIED, TENANT_ID) VALUES ('AlertEmailFlow','AlertEmailFlow','WORKFLOW','to_list,cc_list,bcc_list,email_subject',now(),now(),'0');

UPDATE message_field SET field_name='saxMessageType' WHERE field_name='type';
UPDATE message_field SET field_label='SaxMessageType' WHERE field_label='type';
UPDATE message_field SET field_label='SaxMessageType' WHERE field_label='Type';

UPDATE message_group_field SET field_alias='saxMessageType' WHERE field_alias='type';

UPDATE common_message_group_field SET field_alias='saxMessageType' WHERE field_alias='type';
UPDATE common_message_group_field SET field_name='saxMessageType' WHERE field_name='type';
UPDATE common_message_group_field SET field_label='SaxMessageType' WHERE field_label='type';

/* GA-1.2 release scripts */

INSERT INTO custom_parser (custom_parser_id, custom_parser_class) VALUES ('f31ce8cf-8580-4ba3-a28a-6b3ab9d23d57', 'com.streamanalytix.model.parser.MonitoringDataParser');

UPDATE message SET createdby='superuser' WHERE tenant_id='0';

UPDATE subsystem_info SET subsystem_json = '{
    "id": "AlertPipeline",
    "name": "AlertPipeline",
    "status": "inactive",
    "config": null,
    "workerCount": "1",
    "ackersCount": "1",
    "scheduling": null,
    "spouts": [
        {
            "id": "rabbitmq-channel",
            "name": "RabbitmqChannel_1",
            "label": "RabbitMQ",
            "className": "com.streamanalytix.storm.core.spout.VajraAMQPSpout",
            "config": [
                "connectionType:default",
                "exchangeName:alertsExchange",
                "exchangeType:direct",
                "exchangeDurable:true",
                "exchangeAutoDelete:false",
                "routingKey:alertsQueue",
                "queueName:alertsQueue",
                "queueDurable:true",
                "queueExclusive:false",
                "queueAutoDelete:false",
                "queueConfig:"
            ],
            "messageTypeId": "tracemessage",
            "parallelism": "1",
            "maxSpoutPending": "10",
            "scheme": "com.streamanalytix.storm.core.scheme.AlertTraceMessageScheme",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage",
                        "streamIds"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "AlertConsumerProcessor_1"
                }
            ],
            "channelType": "single",
            "messageTypes": [
                "alertTraceMessage"
            ],
            "coordinates": [],
            "errorConfig": [
                "connectionType:default"
            ]
        }
    ],
    "bolts": [
        {
            "id": "alert-consumer-processor",
            "name": "AlertConsumerProcessor_1",
            "label": "AlertConsumer",
            "className": "com.streamanalytix.storm.core.bolt.AlertsConsumerBolt",
            "config": [],
            "messageTypeId": "tracemessage",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage",
                        "alertDfnId"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "AlertAggregatorProcessor_2"
                }
            ],
            "groupings": [
                {
                    "type": "shuffle",
                    "componentId": "RabbitmqChannel_1",
                    "groupFields": [],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": "1",
            "taskCount": "1",
            "type": "processor",
            "channelType": "single",
            "messageTypes": [
                "alertTraceMessage"
            ],
            "coordinates": [],
            "errorConfig": [
                "connectionType:default"
            ]
        },
        {
            "id": "alert-aggregator-processor",
            "name": "AlertAggregatorProcessor_2",
            "label": "AlertAggregator",
            "className": "com.streamanalytix.storm.core.bolt.AlertEsperBolt",
            "config": [],
            "messageTypeId": "tracemessage",
            "emitStreamIds": [],
            "groupings": [
                {
                    "type": "fields",
                    "componentId": "AlertConsumerProcessor_1",
                    "groupFields": [
                        "alertDfnId"
                    ],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": "1",
            "taskCount": "1",
            "type": "processor",
            "channelType": "single",
            "messageTypes": [
                "alertTraceMessage"
            ],
            "coordinates": [],
            "errorConfig": [
                "connectionType:default"
            ]
        }
    ],
    "esperHAEnabled": false
}', subsystem_id='AlertPipeline', subsystem_name='AlertPipeline' WHERE subsystem_id='AlertSubsystem';

UPDATE subsystem_info SET subsystem_json = '{
    "id": "GraphiteWriterPipeline",
    "name": "GraphiteWriterPipeline",
    "status": "inactive",
    "config": null,
    "workerCount": "1",
    "ackersCount": "1",
    "scheduling": null,
    "spouts": [
        {
            "id": "rabbitmq-channel",
            "name": "Rabbitmq_334142",
            "label": "RabbitMQ",
            "className": "com.streamanalytix.storm.core.spout.VajraAMQPSpout",
            "config": [
                "connectionType:default",
                "exchangeName:saxGraphiteEx",
                "exchangeType:direct",
                "exchangeDurable:true",
                "exchangeAutoDelete:false",
                "routingKey:saxGraphiteQ",
                "queueName:saxGraphiteQ",
                "queueDurable:true",
                "queueExclusive:false",
                "queueAutoDelete:false",
                "queueConfig:"
            ],
            "messageTypeId": "tracemessage",
            "parallelism": "1",
            "maxSpoutPending": "10",
            "scheme": "com.streamanalytix.storm.core.scheme.TraceMessageScheme",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage",
                        "streamIds"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "Graphite_121664"
                }
            ],
            "channelType": "single",
            "messageTypes": [
                "saxDFMonMsg",
                "infrastructure",
                "infrastructure-disk",
                "infrastructure-network",
                "software-jmx"
            ],
            "errorConfig": [
                "connectionType:default"
            ]
        }
    ],
    "bolts": [
        {
            "id": "graphite-processor",
            "name": "Graphite_121664",
            "label": "Graphite",
            "className": "com.streamanalytix.storm.core.bolt.GraphiteBolt",
            "messageTypeId": "tracemessage",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "Alert_269424"
                }
            ],
            "groupings": [
                {
                    "type": "shuffle",
                    "componentId": "Rabbitmq_334142",
                    "groupFields": [],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": 1,
            "taskCount": 1,
            "type": "processor",
            "channelType": "single",
            "messageTypes": [
                "saxDFMonMsg",
                "infrastructure",
                "infrastructure-disk",
                "infrastructure-network",
                "software-jmx"
            ],
            "errorConfig": [
                "connectionType:default"
            ]
        },
        {
            "id": "validator-processor",
            "name": "Alert_269424",
            "label": "Alert",
            "className": "com.streamanalytix.storm.core.bolt.ValidatorBolt",
            "config": [
                "exchangeName:alertsExchange",
                "routingKey:alertsQueue"
            ],
            "messageTypeId": "tracemessage",
            "emitStreamIds": [],
            "groupings": [
                {
                    "type": "shuffle",
                    "componentId": "Graphite_121664",
                    "groupFields": [],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": 1,
            "taskCount": 1,
            "type": "processor",
            "channelType": "single",
            "messageTypes": [
                "saxDFMonMsg",
                "infrastructure",
                "infrastructure-disk",
                "infrastructure-network",
                "software-jmx"
            ],
            "errorConfig": [
                "connectionType:default"
            ]
        }
    ],
    "esperHAEnabled": false
}', subsystem_id='GraphiteWriterPipeline', subsystem_name='GraphiteWriterPipeline' WHERE subsystem_id='GraphiteWriterSubsystem';

UPDATE subsystem_info SET subsystem_json = '{
    "id": "MonitorMetricPipeline",
    "name": "MonitorMetricPipeline",
    "status": "inactive",
    "config": null,
    "workerCount": "1",
    "ackersCount": "1",
    "scheduling": null,
    "spouts": [
        {
            "id": "rabbitmq-channel",
            "name": "Rabbitmq_867064",
            "label": "RabbitMQ",
            "className": "com.streamanalytix.storm.core.spout.VajraAMQPSpout",
            "config": [
                "connectionType:default",
                "exchangeName:saxMetricEX",
                "exchangeType:direct",
                "exchangeDurable:true",
                "exchangeAutoDelete:false",
                "routingKey:saxMetricQ",
                "queueName:saxMetricQ",
                "queueDurable:true",
                "queueExclusive:false",
                "queueAutoDelete:false",
                "queueConfig:"
            ],
            "messageTypeId": "tracemessage",
            "parallelism": "1",
            "maxSpoutPending": "10",
            "scheme": "com.streamanalytix.storm.core.scheme.TraceMessageScheme",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage",
                        "streamIds"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "Timer_529542"
                }
            ],
            "channelType": "single",
            "messageTypes": [
                "saxDFMonMsg",
                "infrastructure",
                "software-jmx"
            ],
            "errorConfig": [
                "connectionType:default"
            ]
        }
    ],
    "bolts": [
        {
            "id": "timer-processor",
            "name": "Timer_529542",
            "label": "Timer",
            "className": "com.streamanalytix.storm.core.bolt.TimerBolt",
            "config": [
                "timerPlugin:com.streamanalytix.storm.core.bolt.MonitorMetricTimerExecutor",
                "tickFrequencyInSeconds:60",
                "tupleQueueSize:100"
            ],
            "messageTypeId": "tracemessage",
            "emitStreamIds": [],
            "groupings": [
                {
                    "type": "shuffle",
                    "componentId": "Rabbitmq_867064",
                    "groupFields": [],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": "1",
            "taskCount": "1",
            "type": "processor",
            "channelType": "single",
            "messageTypes": [
                "saxDFMonMsg",
                "infrastructure",
                "software-jmx"
            ],
            "errorConfig": [
                "connectionType:default"
            ]
        }
    ],
    "esperHAEnabled": false
}', subsystem_id='MonitorMetricPipeline', subsystem_name='MonitorMetricPipeline' WHERE subsystem_id='MonitorMetricSubsystem';

UPDATE subsystem_info SET username = 'superuser' WHERE subsystem_id IN ('AlertPipeline','GraphiteWriterPipeline','MonitorMetricPipeline');

INSERT INTO message_group (group_id, group_name, index_numberof_shards, index_replication_factor, across_field_search_enabled, tenant_id, timestamp_alias, routing_required, routing_policy, index_source, index_expression, persistence_expression, indexing_enabled, persistence_enabled, hbase_region_boundaries, regionsplit, compression) VALUES
('871d9d1c-027c-4f25-8aa5-a7872e303f40', 'saxstormerrorsearchgroup', 2, 1, 'true', '0', 'timestamp', 'false', '', 'false', '''ns_0_''+''saxstormerrorsearchgroup_''+Math.round(timestamp/(3600*1000))', '''ns_0_''+''saxstormerrorsearchgroup''', NULL, NULL, '', NULL, 'false');

INSERT INTO message (message_id, type, delimiter, message_name, createddt, updateddt, message_type, is_multiline_required, is_multiline_negate, multiline_regex, timestamp_format, index_row_key, persistence_row_key, TENANT_ID, timestamp_column, group_id, strict_validation, custom_parser_id, log_message) VALUES
('e91dd50d-c590-4566-8213-d2e2fde8dc51', 'json', '|', 'saxStormErrorSearchMessage', now(), NULL, 'custom', 'N', 'N', '', 'STANDARD', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', 'com.streamanalytix.core.persistence.keygen.UUIDGenerator', '0', 'time', '871d9d1c-027c-4f25-8aa5-a7872e303f40', 'false', NULL, '');

INSERT INTO message_group_field (group_field_id, field_alias, index_analyzer, search_analyzer, index_tf, store, encrypt, column_family, store_in_indexer, persistence_configured, group_id, tenant_id, data_type, is_common) VALUES
('6ba42d7d-9bb1-4dd8-86a7-41a5c4ef988f', 'indexId', 'keyword', 'keyword', '3', 'yes', 'false', 'cf1', 'yes', NULL, '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'Y'),
('16a251e9-8b26-4f8f-817c-9c84de2a1e40', 'persistId', '', '', '2', 'yes', 'false', 'cf1', 'yes', NULL, '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'Y'),
('5a4cbd3d-6ee9-4346-a72e-3cfbae52e066', 'saxMessageType', 'standard', 'standard', '3', 'yes', 'false', 'cf1', 'yes', NULL, '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'Y'),
('b0346c78-fb75-4482-9eca-e1f298afab2d', 'timestamp', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'N'),
('94e64684-08d7-4b01-b13c-e4adc7a19453', 'tenant', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'N'),
('24815eb2-17e0-4247-8d38-0de69b99b359', 'topologyName', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'N'),
('9dd9787a-ba67-46fa-8de7-d0e13f962b84', 'componentName', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'N'),
('6004bba4-ffbf-41d1-8463-3f9234e9d94c', 'className', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'N'),
('5282473c-6e1a-4618-854b-91a7d5f577c3', 'errorMessage', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'N'),
('38b639e6-9832-497c-82f6-de8a60c007a3', 'message', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'N'),
('88c19c01-607f-4c22-8795-99110dfa66f0', 'data', 'standard', 'standard', '3', 'no', 'false', '', 'yes', '', '871d9d1c-027c-4f25-8aa5-a7872e303f40', '0', 'java.lang.String', 'N');

INSERT INTO message_field ( message_field_id, field_name, field_label, regex, field_order,alertable_yn, is_input_field, del_prefix, del_postfix, message_id, group_field_id ) VALUES
('10a1e25c-4942-4848-8061-ae1eae570dd6', 'indexId', 'indexId', '', 0, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', '6ba42d7d-9bb1-4dd8-86a7-41a5c4ef988f'),
('38e84434-b8b1-4cfc-a8a5-9e367ecbe86f', 'persistId', 'persistId', '', 1, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', '16a251e9-8b26-4f8f-817c-9c84de2a1e40'),
('e0dbb7f4-208f-468f-a87b-cfd3f59ec271', 'saxMessageType', 'SaxMessageType', '', 2, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', '5a4cbd3d-6ee9-4346-a72e-3cfbae52e066'),
('748efe91-3ccb-4f76-a1ed-282591f57441', 'time', 'time', '', 3, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', 'b0346c78-fb75-4482-9eca-e1f298afab2d'),
('050a2799-2d1d-47a5-a2da-c94a8221affe', 'tenant', 'tenant', '', 4, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', '94e64684-08d7-4b01-b13c-e4adc7a19453'),
('979ad461-8e7e-4415-ae62-270a3149d067', 'topologyName', 'topologyName', '', 5, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', '24815eb2-17e0-4247-8d38-0de69b99b359'),
('6fda1e9b-dabf-44a9-a5b0-77b6b4fa72db', 'componentName', 'componentName', '', 6, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', '9dd9787a-ba67-46fa-8de7-d0e13f962b84'),
('739e37d3-cf8b-4e6d-b77a-9c6ca293bc1c', 'className', 'className', '', 7, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', '6004bba4-ffbf-41d1-8463-3f9234e9d94c'),
('0f12038f-bfec-495e-91b8-c6914ebeeb11', 'errorMessage', 'errorMessage', '', 8, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', '5282473c-6e1a-4618-854b-91a7d5f577c3'),
('bccfe241-45f7-43bb-9165-f9acf31a523c', 'message', 'message', '', 9, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', '38b639e6-9832-497c-82f6-de8a60c007a3'),
('f37c7c46-4ec6-4002-9d1f-15524c91beaf', 'data', 'data', '', 10, 'N', 'Y', '', '', 'e91dd50d-c590-4566-8213-d2e2fde8dc51', '88c19c01-607f-4c22-8795-99110dfa66f0');

INSERT INTO subsystem_info (subsystem_id, subsystem_json, subsystem_name, config, worker_count, ackers_count, defaultfallback, scheduling, status, customJarStatus, failedReason, tenant_id, username) VALUES ('ErrorSearchPipeline', '{
    "id": "ErrorSearchPipeline",
    "name": "ErrorSearchPipeline",
    "status": "inactive",
    "config": null,
    "workerCount": "1",
    "ackersCount": "1",
    "scheduling": null,
    "spouts": [
        {
            "id": "rabbitmq-channel",
            "name": "Rabbitmq_778848",
            "label": "RabbitMQ",
            "className": "com.streamanalytix.storm.core.spout.VajraAMQPSpout",
            "config": [
                "connectionType:default",
                "exchangeName:saxStromErrorMessageExchange",
                "exchangeType:direct",
                "exchangeDurable:true",
                "exchangeAutoDelete:false",
                "routingKey:saxStromErrorMessageQueue",
                "queueName:saxStromErrorMessageQueue",
                "queueDurable:true",
                "queueExclusive:false",
                "queueAutoDelete:false",
                "queueConfig:",
                "mvelfnclist:[]"
            ],
            "messageTypeId": "tracemessage",
            "parallelism": "1",
            "maxSpoutPending": "10",
            "scheme": "com.streamanalytix.storm.core.scheme.TraceMessageScheme",
            "emitStreamIds": [
                {
                    "emitStreamId": "defaultStream",
                    "outputFields": [
                        "tracemessage",
                        "streamIds"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "Indexer_607478"
                },
                {
                    "emitStreamId": "saxErrorStreamRabbitmq778848",
                    "outputFields": [
                        "tracemessage",
                        "streamIds"
                    ],
                    "applyFilter": false,
                    "filterCriteria": [],
                    "componentId": "Indexer_607478"
                }
            ],
            "coordinates": [
                100,
                60
            ],
            "channelType": "single",
            "messageTypes": [
                "saxStormErrorSearchMessage"
            ]
        }
    ],
    "bolts": [
        {
            "id": "index-processor",
            "name": "Indexer_607478",
            "label": "Indexer",
            "className": "com.streamanalytix.storm.core.bolt.DataIndexBolt",
            "config": [
                "isBatchEnable: false",
                "batchSize:1"
            ],
            "messageTypeId": "tracemessage",
            "emitStreamIds": [],
            "groupings": [
                {
                    "type": "shuffle",
                    "componentId": "Rabbitmq_778848",
                    "groupFields": [],
                    "streamId": "defaultStream"
                }
            ],
            "parallelism": 1,
            "taskCount": 1,
            "type": "processor",
            "coordinates": [
                350,
                60
            ],
            "channelType": "single",
            "messageTypes": [
                "saxStormErrorSearchMessage"
            ]
        }
    ],
    "esperHAEnabled": false
}', 'ErrorSearchPipeline', NULL, NULL, NULL, NULL, NULL, 'inactive', 'NA', 'NA', '0', 'superuser');

INSERT INTO message_group_field (group_field_id, field_alias, index_analyzer, search_analyzer, index_tf, store, encrypt, column_family, store_in_indexer, persistence_configured, group_id, tenant_id, data_type, is_common) VALUES
('a3dfb3d2cb-34ea4b93bfa5883f2--d-ad-2','total','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('453-ef93b-5b-3e-242444aa25d5ee9-db84','used','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a29c34e5fe4-49-a-eb4a4dd5deed14395d3','free','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('ea3b55-ea4535344ef944d1d444-e5a39-de','available','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('f133e-494322559ebdf9aaa9-ebb5d855bda','diskReads','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('4f2d9959a5dbe1994ab383-1-df32b3e-3b4','diskWrites','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('dda3da2baa5824aea34dbac45a34fff4-4e9','diskReadBytes','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('ea3e38-3dab124ba5-a334dbfbd55-b544d8','diskWriteBytes','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('3c-d3-44d3f1f4-a-a4fe92-23-52198ed53','deviceName','standard','standard','3','no','false','','yes','','0','0','java.lang.String','N'),
('d-3aeeb24-4ae4932dbdb-efbdca41143b5a','dirName','standard','standard','3','no','false','','yes','','0','0','java.lang.String','N'),
('5e3e532d3422-dd9-fb911e-de9ab4e4eb59','speed','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('4d1-3d4eb4a29f949a-49ae5a32f34dab344','transmittedDropped','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('re3cad4a9a443a5-bd-5842a--2339a9bfea','transmittedCarrier','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d94f4ef1-541ff4a23-d5dc5-5b51f25-bbe','transmittedCollision','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('f-8-342594225-345e54455ea8cbed9a4d88','transmittedOverruns','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('b9-a5154a-33e239443e-34ce9be42948b-4','transmittedBytes','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('8-b5ae95d233ba-349-2a512a453b33-b3c2','transmittedPackets','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a45b839-9e2338ba-b58134f4541ef3a134d','transmittedErrors','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('ad3d33eae9ebaa55e9abd4aad--9fff4bd-d','receivedDropped','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('24c5a-4b3b-92-deeea9-4deb4-b3-c-23db','receivedBytes','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('ddbbb4c3ebf2a32--33f22435-da-e5da143','receivedPackets','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('b24ffbcba---5b-ea-31d34e-a5-a4ceb34b','receivedFrame','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('b44e388c9db-fb4-3-5cadb818f--ebfa-33','receivedErrors','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('2a3e3f-fdd3-f51-4df3d34b3ad-8d23cd8f','receivedOverruns','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('ad--33ea4b2a3b-45-d495bb2959f152f-d3','interfaceName','standard','standard','3','no','false','','yes','','0','0','java.lang.String','N'),
('a4e9a4-5499bb3e3539e4a3e4c8a-e5d5cfe','MemoryPoolPSEdenSpaceUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d1-f-23-f31cf4c-ff-dc4a2a-a93b-5aaa3','MemoryPoolPSOldGenUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a3b9a29b5c341-5ae4d3e25e543d8e984daa','MemoryPoolPSPermGenUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('9ed398f545b-3bf2-445-8da-1544ca5e4e4','MemoryPoolPSSurvivorSpaceUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('4a2-db1e25-b3b-fbd5-3f8a53f3314dd-12','MemoryPoolCodeCacheUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('3b44f59f43ba42e1d5ada55-445-fb-54ee2','GarbageCollectorPSMarkSweepCollectionCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d9935dcd4b-4a4-bbb5ec99deb--4438adee','GarbageCollectorPSScavengeCollectionCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a5-edb43352ea5a-b5-9ff-a3--455ee2ffd','GarbageCollectorPSMarkSweepCollectionTime','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('1335d4b1135fecd9bdfb944933e33-3f93f5','GarbageCollectorPSScavengeCollectionTime','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('ee92e54babb-b3d-ba1e-2a1--a49afbfddb','ThreadingDaemonThreadCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d1b5-dd92a82bb-43-9d428e-5a2-e1aeefe','ThreadingPeakThreadCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('9dcb4193325ee142-e354fd-54b3d-14b2e3','ThreadingThreadCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('25e-b4abfdfbd5de5b-bbfdb5ae3-3ff2dee','alias','standard','standard','3','no','false','','yes','','0','0','java.lang.String','N');

INSERT INTO message_field( message_field_id, field_name, field_label, regex, field_order,alertable_yn, is_input_field, del_prefix, del_postfix, message_id, group_field_id) VALUES
('fdc544af-5da5-4-558f-b9ad3e45cb3d4a4', 'total', 'Total', NULL,1, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', 'a3dfb3d2cb-34ea4b93bfa5883f2--d-ad-2'),
('4a3d4251ba-559d4-5---1f32d3a19d-1989', 'used', 'Used', NULL,2, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', '453-ef93b-5b-3e-242444aa25d5ee9-db84'),
('cedcfad5-3babe-e4dc--a55e8431-34db-5', 'free', 'Free', NULL,3, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', 'a29c34e5fe4-49-a-eb4a4dd5deed14395d3'),
('2f3-aaad4c39c25d44-dad59be43-5dc9b-4', 'available', 'Available', NULL,4, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', 'ea3b55-ea4535344ef944d1d444-e5a39-de'),
('4fe34a344253-e2eab13db-f-3-eb5e54e4d', 'diskReads', 'Reads', NULL,5, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', 'f133e-494322559ebdf9aaa9-ebb5d855bda'),
('fd-fb33ecc5528eab-1-d-2554bcb-e1394d', 'diskWrites', 'Writes', NULL,6, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', '4f2d9959a5dbe1994ab383-1-df32b3e-3b4'),
('a5ace81-3dccfb-a4e445b4--bb--d-1ddad', 'diskReadBytes', 'Read Bytes', NULL,7, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', 'dda3da2baa5824aea34dbac45a34fff4-4e9'),
('f4b9f2ab5425fee2a3a3b5f-54b3c53dfeb5', 'diskWriteBytes', 'Write Bytes', NULL,8, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', 'ea3e38-3dab124ba5-a334dbfbd55-b544d8'),
('92594-3e25a4d1eb9a424-dc1538cdd4e593', 'host', 'hostname', NULL, 9, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', 'd173f9fd-1a06-4742-afcd-f6ce2e93c53b'),
('b2fab4eedae-4a5ab-a9b34eb24ed35da2-a', 'deviceName', 'Device Name', NULL, 10, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', '3c-d3-44d3f1f4-a-a4fe92-23-52198ed53'),
('d99a339d5fbcf355d1d4a9b5db1c-94e34-d', 'dirName', 'Directory Name', NULL, 11, 'Y', 'Y', NULL, NULL, 'b181cb56-ef61-4114-8092-e0cde1058d09', 'd-3aeeb24-4ae4932dbdb-efbdca41143b5a'),
('5312aeaaa2-d3a53eaaa24dbe-222ba234f3', 'speed', 'Speed', NULL,1, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', '5e3e532d3422-dd9-fb911e-de9ab4e4eb59'),
('ddd8b5a9ce98a12e-d432352b334e-293-13', 'transmittedDropped', 'Transmitted Dropped', NULL,2, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', '4d1-3d4eb4a29f949a-49ae5a32f34dab344'),
('2d331d4df4f-83-5fee9abfb5--94c5dba19', 'transmittedCarrier', 'Transmitted Carrier', NULL,3, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 're3cad4a9a443a5-bd-5842a--2339a9bfea'),
('5d3ab2-de-43ba48552ad1542f3a3424-e3f', 'transmittedCollision', 'Transmitted Collision', NULL,4, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 'd94f4ef1-541ff4a23-d5dc5-5b51f25-bbe'),
('acb-24a-4ee-3d198cadd2b-be9ae43dab15', 'transmittedOverruns', 'Transmitted Overruns', NULL,5, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 'f-8-342594225-345e54455ea8cbed9a4d88'),
('3d5-eea24-339decad4eed5b4--d9a5b-2b2', 'transmittedBytes', 'Transmitted Bytes', NULL,6, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 'b9-a5154a-33e239443e-34ce9be42948b-4'),
('3a5afd39f5--2bf34242e243ebb-efdb1bf5', 'transmittedPackets', 'Transmitted Packets', NULL,7, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', '8-b5ae95d233ba-349-2a512a453b33-b3c2'),
('4433-3ff9b33-2d229-f95-54dc39e5824de', 'transmittedErrors', 'Transmitted Errors', NULL,8, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 'a45b839-9e2338ba-b58134f4541ef3a134d'),
('31bbd24b942fa3b55ba---5-f49b25-3f3a3', 'receivedDropped', 'Received Dropped', NULL,9, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 'ad3d33eae9ebaa55e9abd4aad--9fff4bd-d'),
('32a33-f4afeea4feb45be9e5a33e111431aa', 'receivedBytes', 'Received Bytes', NULL,10, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', '24c5a-4b3b-92-deeea9-4deb4-b3-c-23db'),
('f4efc15-9f-5e414e2dbd42dc4b2334e959c', 'receivedPackets', 'Received Packets', NULL,11, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 'ddbbb4c3ebf2a32--33f22435-da-e5da143'),
('d3-9dea293feafee5e-14eb5345-dfa5dae5', 'receivedFrame', 'Received Frame', NULL,12, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 'b24ffbcba---5b-ea-31d34e-a5-a4ceb34b'),
('3-b-39d934f152bdbcb4d3ffac55-2ae3585', 'receivedErrors', 'Received Errors', NULL,13, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 'b44e388c9db-fb4-3-5cadb818f--ebfa-33'),
('ae-292c2-853a1ea953-3b-4ab9bd344f531', 'receivedOverruns', 'Received Overruns', NULL,14, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', '2a3e3f-fdd3-f51-4df3d34b3ad-8d23cd8f'),
('fde422fc43a85ebc93b-3d-5-248435d444b', 'host', 'hostname', NULL, 15, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 'd173f9fd-1a06-4742-afcd-f6ce2e93c53b'),
('da2429e8ee5eda8245e2a-b-5a1df42-cacb', 'interfaceName', 'Interface Name', NULL, 16, 'Y', 'Y', NULL, NULL, 'a981cb54-fe61-ea14-8312-ca3d3d087e54', 'ad--33ea4b2a3b-45-d495bb2959f152f-d3'),
('d1b94db-821d3-e5e9e3dee555e533dfbb5d', 'MemoryPoolPSEdenSpaceUsageused', 'PS Eden Space Usage', NULL,1, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a4e9a4-5499bb3e3539e4a3e4c8a-e5d5cfe'),
('5dbd9decb5b5-ecaae44fe9c3c5e923fd3aa', 'MemoryPoolPSOldGenUsageused', 'PS OldGen Usage', NULL,2, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'd1-f-23-f31cf4c-ff-dc4a2a-a93b-5aaa3'),
('818a343-35b4bada44-aaaa3-8c45ddb8f9e', 'MemoryPoolPSPermGenUsageused', 'PS PermGen Usage', NULL,3, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a3b9a29b5c341-5ae4d3e25e543d8e984daa'),
('43-3b5d595-aaee1ada5a-f4843a4f4a4eab', 'MemoryPoolPSSurvivorSpaceUsageused', 'PS Survivor Space Usage', NULL,4, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '9ed398f545b-3bf2-445-8da-1544ca5e4e4'),
('ddbfa352a4853a952a-82-9a2b5-a354abed', 'MemoryPoolCodeCacheUsageused', 'CodeCache Usage', NULL,5, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '4a2-db1e25-b3b-fbd5-3f8a53f3314dd-12'),
('3a43b12babddc-d5fad-3e31c4b1a2baf-d2', 'GarbageCollectorPSMarkSweepCollectionCount', 'PS MarkSweep Collection Count', NULL,6, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '3b44f59f43ba42e1d5ada55-445-fb-54ee2'),
('afd39f--524e3e2b3bd35-5e5aa3b-e5a59a', 'GarbageCollectorPSScavengeCollectionCount', 'PS Scavenge Collection Count', NULL,7, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'd9935dcd4b-4a4-bbb5ec99deb--4438adee'),
('cfdf3dca92b2b-e-b9ad44f95da9b-d9df55', 'GarbageCollectorPSMarkSweepCollectionTime', 'PSMarkSweepCollectionTime', NULL,8, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a5-edb43352ea5a-b5-9ff-a3--455ee2ffd'),
('453efdb--9-eb3ed2abfd3-43dfbebae2b-d', 'GarbageCollectorPSScavengeCollectionTime', 'PS Scavenge Collection Time', NULL,9, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '1335d4b1135fecd9bdfb944933e33-3f93f5'),
('344be3338de-b934ef3e32543fd24eb3f5d4', 'ThreadingDaemonThreadCount', 'Daemon Thread Count', NULL,10, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'ee92e54babb-b3d-ba1e-2a1--a49afbfddb'),
('33f34e535434-f1-24c44293cc43bd18313b', 'ThreadingPeakThreadCount', 'Peak Thread Count', NULL,11, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'd1b5-dd92a82bb-43-9d428e-5a2-e1aeefe'),
('f5f3e2-ab4aad9bf8-333e493d-438e-9333', 'ThreadingThreadCount', 'Thread Count', NULL,12, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '9dcb4193325ee142-e354fd-54b3d-14b2e3'),
('d-d5-e3a4e2b84d2c4b-a2425a4aaf4-aaba', 'host', 'hostname', NULL, 13, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'd173f9fd-1a06-4742-afcd-f6ce2e93c53b'),
('e5425a935a4bac-f-895325d94334f-5b51f', 'alias', 'Process Name', NULL, 14, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '25e-b4abfdfbd5de5b-bbfdb5ae3-3ff2dee');

INSERT INTO supported_languages (code, name) VALUES ('en_US', 'English(US)'),('de_DE', 'German(DE)');

DELETE FROM component WHERE component_id='PersistenceProcessor' OR component_id='HDFSProcessor' OR component_id='IndexerProcessor' OR component_id='PMMLProcessor';

UPDATE component SET component_json = '{
    "id": "aggregation-function-processor",
    "name": "AggregationFunctionProcessor",
    "label": "AggregationFunction",
    "className": "com.streamanalytix.storm.core.bolt.EsperBolt",
    "config": [
        "cep.query:select * from GenericEvent.win:time(2 sec)",
        "cep.query.messageName:tracemessage",
        "cepAction:"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "processor",
    "coordinates": []
}', component_id='AggregationFunctionProcessor' WHERE component_id='StatisticalCEPProcessor';

UPDATE component SET component_json = '{
    "id": "custom-cep-processor",
    "name": "CustomCEPProcessor",
    "label": "CustomCEP",
    "className": "com.streamanalytix.storm.core.bolt.EsperBolt",
    "config": [
        "cep.query:select * from GenericEvent.win:time(2 sec)",
        "cep.query.messageName:tracemessage",
        "cepAction:"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1,
    "taskCount": 1,
    "type": "processor",
    "coordinates": []
}' WHERE component_id = 'CustomCEPProcessor';

UPDATE channel SET channel_json = '{
    "id": "rabbitmq-channel",
    "name": "RabbitMQChannel",
    "label": "RabbitMQ",
    "className": "com.streamanalytix.storm.core.spout.VajraAMQPSpout",
    "config": [
        "exchangeName:",
        "exchangeType:direct^^^topic^^^fanout",
        "exchangeDurable:true^^^false",
        "exchangeAutoDelete:false^^^true",
        "routingKey:",
        "queueName:",
        "queueDurable:true^^^false",
        "queueExclusive:false^^^true",
        "queueAutoDelete:false^^^true",
        "componentType:rabbitmq",
        "connectionName:",
        "connectionId:",
        "hosts:",
        "username:",
        "password:"
    ],
    "messageTypeId": "tracemessage",
    "parallelism": 1,
    "maxSpoutPending": 10,
    "scheme": "com.streamanalytix.storm.core.scheme.MultiTraceMessageScheme",
    "schemaIdentifier": "com.streamanalytix.storm.core.scheme.util.DefaultSchemaIdentifier",
    "emitStreamIds": [],
    "coordinates": []
}' WHERE channel_id='RabbitMQChannel';

UPDATE channel SET channel_json = '{
    "id": "kafka-channel",
    "name": "KafkaChannel",
    "label": "Kafka",
    "className": "com.streamanalytix.storm.core.spout.VajraKafkaSpout",
    "config": [
        "topicName:sampleTopic",
        "zkID:sampleTopic",
        "brokerZkRoot:/brokers",
        "partitions:1",
        "replicationFactor:1",
        "forceFromStart:false",
        "fetch.size:1024",
        "buffer.size:1024",
        "componentType:kafka-channel",
        "connectionName:",
        "connectionId:",
        "zkHosts:",
	    "kafkaBrokers:"
    ],
    "messageTypeId": "tracemessage",
    "parallelism": 1,
    "maxSpoutPending": 10,
    "scheme": "com.streamanalytix.storm.core.scheme.MultiTraceMessageMultiScheme",
    "schemaIdentifier": "com.streamanalytix.storm.core.scheme.util.DefaultSchemaIdentifier",
    "emitStreamIds": [],
    "coordinates": []
}' WHERE channel_id='KafkaChannel';

UPDATE component SET component_json = '{
    "id": "rabbitmq-emitter",
    "name": "RabbitMQEmitter",
    "label": "RabbitMQ",
    "className": "com.streamanalytix.storm.core.bolt.RabbitMQProducerBolt",
    "config": [
        "exchangeName:",
        "exchangeType:direct^^^topic^^^fanout",
        "exchangeDurable:true^^^false",
        "exchangeAutoDelete:false^^^true",
        "routingKey:",
        "queueName:",
        "queueDurable:true^^^false",
        "queueExclusive:false^^^true",
        "queueAutoDelete:false^^^true",
        "outputFormat:json",
        "delimiter:,",
        "outputFields:tracemessage",
        "componentType:rabbitmq",
        "connectionName:",
        "connectionId:",
        "hosts:",
        "username:",
        "password:"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "emitter",
    "coordinates": []
}' WHERE component_id='RabbitMQEmitter';

UPDATE component SET component_json = '{
    "id": "kafka-emitter", 
    "name": "KafkaEmitter", 
    "label": "Kafka", 
    "className": "com.streamanalytix.storm.core.bolt.KafkaProducerBolt", 
    "config": [
        "topicName:sampleTopic",
        "partitions:1",
        "replicationFactor:1",
        "producerType:sync",
        "outputFormat:json",
        "delimiter:,",
        "outputFields:tracemessage",
        "connectionName:",
        "connectionId:",
        "componentType:kafka-emitter",
        "zkHosts:",
	    "kafkaBrokers:"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "emitter",
    "coordinates": []
}' WHERE component_id='KafkaEmitter';

UPDATE channel SET channel_json = '{
    "id": "custom-channel",
    "name": "CustomChannel",
    "label": "CustomChannel",
    "className": "com.streamanalytix.storm.core.spout.CustomChannel",
    "config": [
        "channelPlugin:"
    ],
    "messageTypeId": "tracemessage",
    "parallelism": 1,
    "maxSpoutPending": 10,
    "scheme": "com.streamanalytix.storm.core.scheme.MultiTraceMessageScheme",
    "schemaIdentifier": "com.streamanalytix.storm.core.scheme.util.DefaultSchemaIdentifier",
    "emitStreamIds": [],
    "coordinates": []
}' WHERE channel_id='CustomChannel';

UPDATE component SET component_json = '{
    "id": "custom-processor",
    "name": "CustomProcessor",
    "label": "CustomProcessor",
    "className": "com.streamanalytix.storm.core.bolt.CustomBolt",
    "config": [
        "executorPlugin:"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "processor",
    "coordinates": []
}' WHERE component_id='CustomProcessor';

UPDATE component SET component_json = '{
    "id": "stream-emitter", 
    "name": "StreamEmitter", 
    "label": "Streaming", 
    "className": "com.streamanalytix.storm.core.bolt.stream.RTStreamBolt", 
    "config": [
        "StreamId:rtStreamExchange" 
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "emitter",
    "coordinates": []
}' WHERE component_id='StreamEmitter';

INSERT INTO component (component_id, component_json, type) VALUES ('HbasePersister','{
    "id": "hbase-emitter",
    "name": "Hbase",
    "label": "HBase",
    "className": "com.streamanalytix.storm.core.bolt.TraceMessagePersistenceBolt",
    "config": [
        "dataStore:hbase",
        "connectionName:",
        "connectionId:",
        "isBatchEnable:false",
        "batchSize:1",
        "tableNameExpression:",
        "compression:",
        "regionSplittingDefinition:Default^^^Based on Region Boundaries",
        "regionBoundaries:",
        "zkHosts:",
        "zkPort:",
        "clientRetriesNumber:1",
        "zkRecoveryRetry:1",
        "zkParentNode:/hbase"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "emitter",
    "coordinates": []
}','emitter'),('CassandraPersister','{
    "id": "cassandra-emitter",
    "name": "Cassandra",
    "label": "Cassandra",
    "className": "com.streamanalytix.storm.core.bolt.TraceMessagePersistenceBolt",
    "config": [
        "dataStore:cassandra",
        "connectionName:",
        "connectionId:",
        "isBatchEnable:false",
        "batchSize:1",
        "tableNameExpression:",
        "compression:",
        "hosts:",
        "username:",
        "password:",
        "replicationStrategy:org.apache.cassandra.locator.SimpleStrategy",
        "replicationFactor:1",
        "thriftClientRetries:5",
        "thriftClientRetriesIntervalInMs:5000",
        "connectionRetries:300"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "emitter",
    "coordinates": []
}','emitter'),('HDFSProcessor','{
    "id": "hdfs-emitter",
    "name": "HDFS",
    "label": "HDFS",
    "className": "com.streamanalytix.storm.core.bolt.HDFSBolt",
    "config": [
        "dataStore:hdfs",
        "connectionName:",
        "connectionId:",
        "haEnabled:false",
        "nameservices: ",
        "namenode1Name:",
        "namenode1RPCAddress:",
        "namenode2Name:",
        "namenode2RPCAddress:",
        "fsURI:",
        "username:",
        "isBatchEnable:false",
        "batchSize:1",
        "hdfsPaths:[{\\"hdfsPath\\":\\"\\", \\"fields\\":[],\\"controlFile\\":false,\\"hdfsFilePrefix\\":\\"\\",\\"syncSize\\":1,\\"blockSize\\":67108864,\\"rotationPolicy\\":\\"SizeBased^^^TimeBased^^^SizeOrTimeBased\\",\\"fileRotationTime\\":3600,\\"fileRotationSize\\":536870912,\\"replication\\":1,\\"outputType\\":\\"Delimited\\",\\"delimiter\\":\\"|^^^,^^^:^^^;^^^\\\\t\\",\\"compressionType\\":\\"none^^^deflate^^^gzip^^^bzip2\\" ,\\"rotationActionTemplates\\":{\\"Move HDFS File\\":{\\"location\\":\\"\\"}, \\"Copy HDFS File\\":{\\"locations\\":\\"\\"},\\"Create Hive Table\\":{\\"metastoreUrl\\":\\"\\",\\"databaseName\\":\\"\\",\\"tableName\\":\\"\\",\\"location\\":\\"\\"}},\\"rotationActions\\":{}}]"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "emitter",
    "coordinates": []
}','emitter'),('SolrIndexer','{
    "id": "solr-emitter",
    "name": "Solr",
    "label": "Solr",
    "className": "com.streamanalytix.storm.core.bolt.DataIndexBolt",
    "config": [
        "isBatchEnable: True^^^False",
        "batchSize:1",
        "acrossFieldSearchEnabled:True^^^False",
        "indexNumberOfShards:2",
        "indexReplicationFactor:1",
        "indexExpression:",
        "routingRequired:False^^^True",
        "routingPolicy:",
        "indexSource:True^^^False",
        "indexStore:solr",
        "connectionName:",
        "connectionId:",
        "componentType:solr",
        "zkHosts:"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "emitter",
    "coordinates": []
}','emitter'),('ElasticSearchIndexer','{
    "id": "elasticsearch-emitter",
    "name": "ElasticSearch",
    "label": "ElasticSearch",
    "className": "com.streamanalytix.storm.core.bolt.DataIndexBolt",
    "config": [
        "isBatchEnable: True^^^False",
        "batchSize:1",
        "acrossFieldSearchEnabled:True^^^False",
        "indexNumberOfShards:2",
        "indexReplicationFactor:1",
        "indexExpression:",
        "routingRequired:True^^^False",
        "routingPolicy:",
        "indexSource:True^^^False",
        "indexStore:elasticsearch",
        "connectionName:",
        "connectionId:",
        "componentType:elasticsearch",
        "hosts:",
        "clusterName:"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "emitter",
    "coordinates": []
}','emitter'),('AnalyticsProcessor','{
    "id": "analytics-processor",
    "name": "AnalyticsProcessor",
    "label": "Analytics",
    "className": "com.streamanalytix.storm.core.bolt.PMMLProcessorBolt",
    "config": [],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "processor",
    "coordinates": [],
    "algorithms": [
        {
            "name": "LogisticRegression",
            "id": "logistic-regression"
        },
        {
            "name": "Regression",
            "id": "regression"
        },
        {
            "name": "ClusterModel",
            "id": "cluster-model"
        },
        {
            "name": "SupportVectorMachine",
            "id": "svm"
        },
        {
            "name": "AssociationRules",
            "id": "association-rules"
        },
        {
            "name": "NaiveBayes",
            "id": "naive-bayes"
        },
        {
            "name": "Ensemble",
            "id": "ensemble"
        },
        {
            "name": "NeuralNetwork",
            "id": "neural-network"
        },
        {
            "name": "Tree Model",
            "id": "tree-model"
        }
    ]
}','processor'),('ActiveMQEmitter','{
    "id": "activemq-emitter",
    "name": "ActiveMQEmitter",
    "label": "ActiveMQ",
    "className": "com.streamanalytix.storm.core.bolt.ActiveMQProducerBolt",
    "config": [
        "topicConfig:",
        "exchangeName:",
        "exchangeType:direct^^^topic^^^fanout",
        "exchangeDurable:true^^^false",
        "exchangeAutoDelete:false^^^true",
        "routingKey:",
        "queueName:",
        "queueDurable:true^^^false",
        "queueExclusive:false^^^true",
        "queueAutoDelete:false^^^true",
        "outputFormat:json",
        "delimiter:,",
        "outputFields:tracemessage",
        "componentType:activemq",
        "connectionName:",
        "connectionId:",
        "hosts:",
        "username:",
        "password:"
    ],
    "messageTypeId": "tracemessage",
    "emitStreamIds": [],
    "groupings": [],
    "parallelism": 1,
    "taskCount": 1,
    "type": "emitter",
    "coordinates": []
}','emitter'),('EnrichProcessor','{
    "id": "enrich-processor", 
    "name": "EnrichProcessor", 
    "label": "Enricher", 
    "className": "com.streamanalytix.storm.core.bolt.EnrichBolt", 
    "config": [
        "enrichFlds:{}"
    ], 
    "messageTypeId": "tracemessage", 
    "emitStreamIds": [], 
    "groupings": [], 
    "parallelism": 1, 
    "taskCount": 1, 
    "type": "processor",
    "coordinates": []
}','processor');

INSERT INTO udf_info(udf_Id,udf_name,udf_class,udf_args,udf_param,date_created,date_modified,isudf,jar_name,udf_type,udf_return_type,cache_enable,udf_desc) VALUES ('strContains','strContains','com.streamanalytix.udf.impl.ContainsImpl','String string, String charSeq','[]',now(),now(),'N','','String','boolean','','udf.desc.strContains'),('strLength','strLength','com.streamanalytix.udf.impl.LengthImpl','String string','[]',now(),now(),'N','','String','int','','udf.desc.strLength'),('isStrEmpty','isStrEmpty','com.streamanalytix.udf.impl.EmptyCheckImpl','String string','[]',now(),now(),'N','','String','boolean','','udf.desc.isStrEmpty'), ('strEquals','strEquals','com.streamanalytix.udf.impl.EqualsImpl','String string, String toCompareString','[]',now(),now(),'N','','String','boolean','','udf.desc.strEquals'),('strEqualsIgnoreCase','strEqualsIgnoreCase','com.streamanalytix.udf.impl.EqualsIgnoreCaseImpl','String string, String toCompareString','[]',now(),now(),'N','','String','boolean','','udf.desc.strEqualsIgnoreCase'),('strMatches','strMatches','com.streamanalytix.udf.impl.MatchesImpl','String string, String regex','[]',now(),now(),'N','','String','boolean','','udf.desc.strMatches'),('strIndexOf','strIndexOf','com.streamanalytix.udf.impl.IndexOfImpl','String string, String charSeq','[]',now(),now(),'N','','String','int','','udf.desc.strIndexOf'),('strConcat','strConcat','com.streamanalytix.udf.impl.ConcatImpl','String string, String charSeq','[]',now(),now(),'N','','String','String','','udf.desc.strConcat'),('strReplace','strReplace','com.streamanalytix.udf.impl.ReplaceImpl','String string, String oldCharSeq, String newCharSeq','[]',now(),now(),'N','','String','String','','udf.desc.strReplace'),('strSplit','strSplit','com.streamanalytix.udf.impl.SplitImpl','String string, String regex','[]',now(),now(),'N','','String','String','','udf.desc.strSplit'),('strToLower','strToLower','com.streamanalytix.udf.impl.ToLowerImpl','String string','[]',now(),now(),'N','','String','String','','udf.desc.strToLower'),('strToUpper','strToUpper','com.streamanalytix.udf.impl.ToUpperImpl','String string','[]',now(),now(),'N','','String','String','','udf.desc.strToUpper'),('strNotNull','strNotNull','com.streamanalytix.udf.impl.NotNullImpl','String string','[]',now(),now(),'N','','String','boolean','','udf.desc.strNotNull'),('strIsNull','strIsNull','com.streamanalytix.udf.impl.NullImpl','String string','[]',now(),now(),'N','','String','boolean','','udf.desc.strIsNull'),('strFormat','strFormat','com.streamanalytix.udf.impl.FormatImpl','String formatString, String string','[]',now(),now(),'N','','String','String','','udf.desc.strFormat'),('currentDT','currentDT','com.streamanalytix.udf.impl.CurrentDateImpl','','[]',now(),now(),'N','','Date','String','','udf.desc.currentDT'),('addDT','addDT','com.streamanalytix.udf.impl.AddDateImpl','String dateFormat, String date, Integer daysToBeAdded','[]',now(),now(),'N','','Date','String','','udf.desc.addDT'),('subDT','subDT','com.streamanalytix.udf.impl.SubDateImpl','String dateFormat, String date, Integer daysToBeSubtracted','[]',now(),now(),'N','','Date','String','','udf.desc.subDT'),('diffInDTs','diffInDTs','com.streamanalytix.udf.impl.DifferenceInDatesImpl','String dateFormat, String firstDate, String secondDate','[]',now(),now(),'N','','Date','long','','udf.desc.diffInDTs'),('formatDT','formatDT','com.streamanalytix.udf.impl.FormatDateImpl','String date, String currentDateFormat, String expectedDateFormat','[]',now(),now(),'N','','Date','String','','udf.desc.formatDT'),('beforeDT','beforeDT','com.streamanalytix.udf.impl.BeforeDateImpl','String dateFormat, String firstDate, String secondDate','[]',now(),now(),'N','','Date','boolean','','udf.desc.beforeDT'),('afterDT','afterDT','com.streamanalytix.udf.impl.AfterDateImpl','String dateFormat, String firstDate, String secondDate','[]',now(),now(),'N','','Date','boolean','','udf.desc.afterDT'),('withinDT','withinDT','com.streamanalytix.udf.impl.WithinDatesImpl','String dateFormat, String startDate, String endDate, String dateToBeChecked','[]',now(),now(),'N','','Date','boolean','','udf.desc.withinDatesImpl'),('isValidDT','isValidDT','com.streamanalytix.udf.impl.ValidCalendarDateImpl','String dateFormat, String dateToBeChecked','[]',now(),now(),'N','','Date','boolean','','udf.desc.isValidDT'),('objEncrypt','objEncrypt','com.streamanalytix.udf.impl.EncryptObjectImpl','String object','[]',now(),now(),'N','','Object','String','','udf.desc.objEncrypt'),('objDecrypt','objDecrypt','com.streamanalytix.udf.impl.DecryptObjectImpl','String object','[]',now(),now(),'N','','Object','String','','udf.desc.objDecrypt'),('lookupRDBMS','lookupRDBMS','com.streamanalytix.udf.impl.LookupRDBMS','String selectQuery','[{"propertyName": {"type": "label","value": "driverClass"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "url"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "username"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "password"},"propertyValue": {"type": "password","value": "","required": true}}]',now(),now(),'N','','Lookup','Array','Y','udf.desc.lookupRDBMS'),('lookupWS','lookupWS','com.streamanalytix.udf.impl.LookupWS','','[{"propertyName": {"type": "label","value": "endpoint"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "methodtype"},"propertyValue": {"type": "select","options": [{"label": "GET","value": "GET"},{"label": "POST","value": "POST"}],"value": "GET","required": true}},{"propertyName": {"type": "label","value": "Content-type"},"propertyValue": {"type": "text","value": "application/json","required": false}},{"propertyName": {"type": "label","value": "wsparams"},"propertyValue": {"type": "text","value": "","required": false}}]',now(),now(),'N','','Lookup','String','Y','udf.desc.lookupWS'),('lookupCassandra','lookupCassandra','com.streamanalytix.udf.impl.LookupCassandra','String query','[{"propertyName": {"type": "label","value": "hosts"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "username"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "password"},"propertyValue": {"type": "password","value": "","required": true}}]',now(),now(),'N','','Lookup','Array','','udf.desc.cassnadra'),('lookupHBase','lookupHBase','com.streamanalytix.udf.impl.LookupHBase','String tableName,String rowID, String columnFamily','[{"propertyName": {"type": "label","value": "hdfsUser"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "zkHosts"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "zkPort"},"propertyValue": {"type": "text","value": "","required": false}}]',now(),now(),'N','','Lookup','Array','','udf.desc.hbase'),('lookupES','lookupES','com.streamanalytix.udf.impl.LookupElasticSearch','String queryString','[{"propertyName": {"type": "label","value": "esconnect"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "clustername"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "indexname"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "indextype"},"propertyValue": {"type": "text","value": "","required": true}}]',now(),now(),'N','','Lookup','String','','udf.desc.es');

INSERT INTO field_data_type (data_type_id,data_type_name,data_type_value) VALUES ('Text','Text','java.lang.String'),('Number','Number','java.lang.Long'),('Decimal','Decimal','java.lang.Double'),('Boolean','Boolean','java.lang.Boolean'),('Date','Date','java.util.Date');

INSERT INTO analytics_detail (component_id, pmml_file, subsystem_id, tenant_id) VALUES ('AnalyticsProcessor_2','<?xml version="1.0" encoding="UTF-8"?>
<PMML xmlns="http://www.dmg.org/PMML-4_1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="4.1" xsi:schemaLocation="http://www.dmg.org/PMML-4_1 http://www.dmg.org/v4-1/pmml-4-1.xsd">
   <Header copyright="Copyright (c) 2014 ssaraf" description="Linear Regression Model">
      <Extension name="user" value="ssaraf" extender="Rattle/PMML" />
      <Application name="Rattle/PMML" version="1.4" />
      <Timestamp>2014-05-01 10:27:02</Timestamp>
   </Header>
   <DataDictionary numberOfFields="2">
      <DataField name="a" optype="continuous" dataType="double" />
      <DataField name="b" optype="continuous" dataType="double" />
   </DataDictionary>
   <RegressionModel modelName="Linear_Regression_Model" functionName="regression" algorithmName="least squares">
      <MiningSchema>
         <MiningField name="a" usageType="predicted" />
         <MiningField name="b" usageType="active" />
      </MiningSchema>
      <Output>
         <OutputField name="Predicted_Hwt" feature="predictedValue" />
      </Output>
      <RegressionTable intercept="-0.356662432884953">
         <NumericPredictor name="b" exponent="1" coefficient="4.03406269845852" />
      </RegressionTable>
   </RegressionModel>
</PMML>', 'SelfTestPipeline', '0');

INSERT INTO channel (channel_id, channel_json) VALUES ('ActiveMQChannel','{
    "id": "activemq-channel",
    "name": "ActiveMQChannel",
    "label": "ActiveMQ",
    "className": "com.streamanalytix.storm.core.spout.CustomChannel",
    "config": [
        "channelPlugin:com.streamanalytix.storm.core.spout.ActiveMQSpout",
        "topicConfig:",
        "connectionType:userDefined",
        "exchangeName:",
        "exchangeType:direct^^^topic^^^fanout",
        "exchangeDurable:true^^^false",
        "exchangeAutoDelete:false^^^true",
        "routingKey:",
        "queueName:",
        "queueDurable:true^^^false",
        "queueExclusive:false^^^true",
        "queueAutoDelete:false^^^true",
        "componentType:activemq",
        "connectionName:",
        "connectionId:",
        "hosts:",
        "username:",
        "password:"
    ],
    "messageTypeId": "tracemessage",
    "parallelism": 1,
    "maxSpoutPending": 10,
    "scheme": "com.streamanalytix.storm.core.scheme.MultiTraceMessageScheme",
    "schemaIdentifier": "com.streamanalytix.storm.core.scheme.util.DefaultSchemaIdentifier",
    "emitStreamIds": [],
    "coordinates": []
}'),('ReplayChannel','{
    "id": "replay-channel",
    "name": "ReplayChannel",
    "label": "Replay",
    "className": "",
    "config": [
        "maxRetries:3",
        "x-message-ttl:10",
        "x-dead-letter-exchange:",
        "x-dead-letter-routing-key:",
        "requeueOnFail:false"
    ],
    "messageTypeId": "tracemessage",
    "parallelism": 1,
    "maxSpoutPending": 10,
    "scheme": "com.streamanalytix.storm.core.scheme.ReplayTraceMessageScheme",
    "emitStreamIds": [],
    "coordinates": [],
    "replay-config": {
        "source": [
            {
                "name": "RabbitMQ",
                "className": "com.streamanalytix.storm.core.spout.VajraAMQPSpout",
                "config": [
                    "connectionName:",
                    "connectionId:",
                    "componentType:rabbitmq",
                    "hosts:",
                    "username:",
                    "password:",
                    "exchangeName:",
                    "routingKey:",
                    "queueName:"
                ]
            }
        ],
        "sink": [
            {
                "name": "RabbitMQ",
                "config": [
                    "discardedExchangeName:",
                    "discardedQueueName:"
                ]
            }
        ]
    }
}');

UPDATE udf_info set udf_param='[{"propertyName": {"type": "label","value": "hosts"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "username"},"propertyValue": {"type": "text","value": "","required": false}},{"propertyName": {"type": "label","value": "password"},"propertyValue": {"type": "password","value": "","required": false}},{"propertyName": {"type": "label","value": "connectionRetries"},"propertyValue": {"type": "text","value": "300","required": true}}]' WHERE udf_id='lookupCassandra';

UPDATE udf_info set udf_param='[{"propertyName": {"type": "label","value": "hdfsUser"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "zkHosts"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "zkPort"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "zkParentNode"},"propertyValue": {"type": "text","value": "/hbase","required": true}},{"propertyName": {"type": "label","value": "clientRetriesNumber"},"propertyValue": {"type": "text","value": "1","required": true}},{"propertyName": {"type": "label","value": "zkRecoveryRetry"},"propertyValue": {"type": "text","value": "1","required": true}}]'WHERE udf_id='lookupHBase';

UPDATE udf_info SET udf_param='[{"propertyName": {"type": "label","value": "hosts"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "clusterName"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "indexname"},"propertyValue": {"type": "text","value": "","required": true}},{"propertyName": {"type": "label","value": "indextype"},"propertyValue": {"type": "text","value": "","required": true}}]' WHERE udf_id='lookupES';

INSERT INTO message_group_field (group_field_id, field_alias, index_analyzer, search_analyzer, index_tf, store, encrypt, column_family, store_in_indexer, persistence_configured, group_id, tenant_id, data_type, is_common) VALUES
('a4e9a4-5499bb3e3539e4a3e4c8a-e5d5cfc','MemoryPoolEdenSpaceUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d1-f-23-f31cf4c-ff-dc4a2a-a93b-5aaa5','MemoryPoolPermGenUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a3b9a29b5c341-5ae4d3e25e543d8e984daf','MemoryPoolSurvivorSpaceUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('9ed398f545b-3bf2-445-8da-1544ca5e4e5','MemoryPoolTenuredGenUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('3b44f59f43ba42e1d5ada55-445-fb-54ee3','GarbageCollectorCopyCollectionCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d9935dcd4b-4a4-bbb5ec99deb--4438adet','GarbageCollectorCopyCollectionTime','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a5-edb43352ea5a-b5-9ff-a3--455ee2ffa','GarbageCollectorMarkSweepCompactCollectionCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('1335d4b1135fecd9bdfb944933e33-3f93f2','GarbageCollectorMarkSweepCompactCollectionTime','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a4e9a4-5499bb3e3539e4a3e4c8a-e5d5cfg','MemoryPoolCMSOldGenUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d1-f-23-f31cf4c-ff-dc4a2a-a93b-5aaa7','MemoryPoolCMSPermGenUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a3b9a29b5c341-5ae4d3e25e543d8e984dad','MemoryPoolParEdenSpaceUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('9ed398f545b-3bf2-445-8da-1544ca5e4e3','MemoryPoolParSurvivorSpaceUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('3b44f59f43ba42e1d5ada55-445-fb-54ee0','GarbageCollectorConcurrentMarkSweepCollectionCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d9935dcd4b-4a4-bbb5ec99deb--4438adeh','GarbageCollectorConcurrentMarkSweepCollectionTime','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a5-edb43352ea5a-b5-9ff-a3--455ee2ffb','GarbageCollectorParNewCollectionCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('1335d4b1135fecd9bdfb944933e33-3f93f3','GarbageCollectorParNewCollectionTime','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a4e9a4-5499bb3e3539e4a3e4c8a-e5d5cfk','MemoryPoolG1EdenSpaceUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d1-f-23-f31cf4c-ff-dc4a2a-a93b-5aaa9','MemoryPoolG1OldGenUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a3b9a29b5c341-5ae4d3e25e543d8e984dac','MemoryPoolG1PermGenUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('9ed398f545b-3bf2-445-8da-1544ca5e4e9','MemoryPoolG1SurvivorSpaceUsageused','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('3b44f59f43ba42e1d5ada55-445-fb-54ee8','GarbageCollectorG1OldGenerationCollectionCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('d9935dcd4b-4a4-bbb5ec99deb--4438aden','GarbageCollectorG1OldGenerationCollectionTime','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('a5-edb43352ea5a-b5-9ff-a3--455ee2ffx','GarbageCollectorG1YoungGenerationCollectionCount','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N'),
('1335d4b1135fecd9bdfb944933e33-3f93f7','GarbageCollectorG1YoungGenerationCollectionTime','standard','standard','3','no','false','','yes','','0','0','java.lang.Double','N');

INSERT INTO message_field ( message_field_id, field_name, field_label, regex, field_order,alertable_yn, is_input_field, del_prefix, del_postfix, message_id, group_field_id) VALUES
('d1b94db-821d3-e5e9e3dee555e533dfbb5h', 'MemoryPoolEdenSpaceUsageused', 'Eden Space Usage', NULL,10, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a4e9a4-5499bb3e3539e4a3e4c8a-e5d5cfc'),
('5dbd9decb5b5-ecaae44fe9c3c5e923fd3ab', 'MemoryPoolPermGenUsageused', 'Perm Gen Usage', NULL,11, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'd1-f-23-f31cf4c-ff-dc4a2a-a93b-5aaa5'),
('818a343-35b4bada44-aaaa3-8c45ddb8f9f', 'MemoryPoolSurvivorSpaceUsageused', 'Survivor Space Usage', NULL,12, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a3b9a29b5c341-5ae4d3e25e543d8e984daf'),
('43-3b5d595-aaee1ada5a-f4843a4f4a4eac', 'MemoryPoolTenuredGenUsageused', 'Tenured Gen Usage', NULL,13, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '9ed398f545b-3bf2-445-8da-1544ca5e4e5'),
('3a43b12babddc-d5fad-3e31c4b1a2baf-d1', 'GarbageCollectorCopyCollectionCount', 'Copy Collection Count', NULL,14, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '3b44f59f43ba42e1d5ada55-445-fb-54ee3'),
('afd39f--524e3e2b3bd35-5e5aa3b-e5a59d', 'GarbageCollectorMarkSweepCompactCollectionCount', 'MarkSweepCompact Collection Count', NULL,15, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a5-edb43352ea5a-b5-9ff-a3--455ee2ffa'),
('cfdf3dca92b2b-e-b9ad44f95da9b-d9df53', 'GarbageCollectorCopyCollectionTime', 'Copy Collection Time', NULL,16, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'd9935dcd4b-4a4-bbb5ec99deb--4438adet'),
('453efdb--9-eb3ed2abfd3-43dfbebae2b-j', 'GarbageCollectorMarkSweepCompactCollectionTime', 'MarkSweepCompact Collection Time', NULL,17, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '1335d4b1135fecd9bdfb944933e33-3f93f2'),
('d1b94db-821d3-e5e9e3dee555e533dfbb5r', 'MemoryPoolCMSOldGenUsageused', 'CMS Old Gen Usage', NULL,18, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a4e9a4-5499bb3e3539e4a3e4c8a-e5d5cfg'),
('5dbd9decb5b5-ecaae44fe9c3c5e923fd3av', 'MemoryPoolCMSPermGenUsageused', 'CMS Perm Gen Usage', NULL,19, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'd1-f-23-f31cf4c-ff-dc4a2a-a93b-5aaa7'),
('818a343-35b4bada44-aaaa3-8c45ddb8f9g', 'MemoryPoolParEdenSpaceUsageused', 'Par Eden Space Usage', NULL,20, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a3b9a29b5c341-5ae4d3e25e543d8e984dad'),
('43-3b5d595-aaee1ada5a-f4843a4f4a4eaf', 'MemoryPoolParSurvivorSpaceUsageused', 'Par Survivor Space Usage', NULL,21, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '9ed398f545b-3bf2-445-8da-1544ca5e4e3'),
('3a43b12babddc-d5fad-3e31c4b1a2baf-d3', 'GarbageCollectorConcurrentMarkSweepCollectionCount', 'ConcurrentMarkSweep Collection Count', NULL,22, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '3b44f59f43ba42e1d5ada55-445-fb-54ee0'),
('afd39f--524e3e2b3bd35-5e5aa3b-e5a59v', 'GarbageCollectorParNewCollectionCount', 'ParNew Collection Count', NULL,23, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a5-edb43352ea5a-b5-9ff-a3--455ee2ffb'),
('cfdf3dca92b2b-e-b9ad44f95da9b-d9df57', 'GarbageCollectorConcurrentMarkSweepCollectionTime', 'ConcurrentMarkSweep Collection Time', NULL,24, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'd9935dcd4b-4a4-bbb5ec99deb--4438adeh'),
('453efdb--9-eb3ed2abfd3-43dfbebae2b-e', 'GarbageCollectorParNewCollectionTime', 'ParNew Collection Time', NULL,25, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '1335d4b1135fecd9bdfb944933e33-3f93f3'),
('d1b94db-821d3-e5e9e3dee555e533dfbb5v', 'MemoryPoolG1EdenSpaceUsageused', 'G1 Eden Space Usage', NULL,26, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a4e9a4-5499bb3e3539e4a3e4c8a-e5d5cfk'),
('5dbd9decb5b5-ecaae44fe9c3c5e923fd3ac', 'MemoryPoolG1OldGenUsageused', 'G1 Old Gen Usage', NULL,27, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'd1-f-23-f31cf4c-ff-dc4a2a-a93b-5aaa9'),
('818a343-35b4bada44-aaaa3-8c45ddb8f9h', 'MemoryPoolG1PermGenUsageused', 'G1 Perm Gen Usage', NULL,28, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a3b9a29b5c341-5ae4d3e25e543d8e984dac'),
('43-3b5d595-aaee1ada5a-f4843a4f4a4eas', 'MemoryPoolG1SurvivorSpaceUsageused', 'G1 Survivor Space Usage', NULL,29, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '9ed398f545b-3bf2-445-8da-1544ca5e4e9'),
('3a43b12babddc-d5fad-3e31c4b1a2baf-52', 'GarbageCollectorG1OldGenerationCollectionCount', 'G1 Old Generation Collection Count', NULL,30, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '3b44f59f43ba42e1d5ada55-445-fb-54ee8'),
('afd39f--524e3e2b3bd35-5e5aa3b-e5a59f', 'GarbageCollectorG1YoungGenerationCollectionCount', 'G1 Young Generation Collection Count', NULL,31, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'a5-edb43352ea5a-b5-9ff-a3--455ee2ffx'),
('cfdf3dca92b2b-e-b9ad44f95da9b-d9df58', 'GarbageCollectorG1OldGenerationCollectionTime', 'G1 Old Generation Collection Time', NULL,32, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', 'd9935dcd4b-4a4-bbb5ec99deb--4438aden'),
('453efdb--9-eb3ed2abfd3-43dfbebae2b-f', 'GarbageCollectorG1YoungGenerationCollectionTime', 'G1 Young Generation Collection Time', NULL,33, 'Y', 'Y', NULL, NULL, '8cedf074-d70d-47eb-9d47-94d6731fd495', '1335d4b1135fecd9bdfb944933e33-3f93f7');
