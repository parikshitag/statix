#!/bin/bash
# database setup script

#parsing arguments to get params
VALUE=0;
for var in "$@"
do
    IFS='=' read -a component <<< "${var}"
     if [ "${component[0]}" == "-host" ] ; then
        IFS=':' read -a host <<< "${component[1]}"
           DB_HOST="${host[0]}";
           DB_PORT="${host[1]}";
     elif [ "${component[0]}" == "-username" ]; then
         DB_USERNAME="${component[1]}";
     elif [ "${component[0]}" == "-password" ]; then
         DB_PASSWORD="${component[1]}";
     elif [ "${component[0]}" == "-sqldump" ]; then
         DB_FOLDER="${component[1]}";
     elif [ "${component[0]}" == "-schemas" ]; then
         SCHEMAS="${component[1]}";
     fi
done

export PGPASSWORD=$DB_PASSWORD

IFS=',' read -a schemas <<< "${SCHEMAS}"
for schema in "${schemas[@]}"
do
    IFS='_' read -a db <<< "${schema}";
    echo deleting schema "$schema";
    psql -U $DB_USERNAME -d postgres -h $DB_HOST -p $DB_PORT -c "DROP DATABASE ${db[0]};";
done

