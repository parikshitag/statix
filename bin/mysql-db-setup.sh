#!/bin/bash
# database setup script

#parsing arguments to get params
VALUE=0;
for var in "$@"
do
    IFS='=' read -a component <<< "${var}"
     if [ "${component[0]}" == "-host" ] ; then
	IFS=':' read -a host <<< "${component[1]}"
	   MYSQL_HOST="${host[0]}";
	   MYSQL_PORT="${host[1]}";
     elif [ "${component[0]}" == "-username" ]; then
	 MYSQL_USERNAME="${component[1]}";
     elif [ "${component[0]}" == "-password" ]; then
	 MYSQL_PASSWORD="${component[1]}";
     elif [ "${component[0]}" == "-sqldump" ]; then
	 DUMP_FOLDER="${component[1]}";
     elif [ "${component[0]}" == "-schemas" ]; then
	 SCHEMAS="${component[1]}";
     fi
done

IFS=',' read -a schemas <<< "${SCHEMAS}"
for schema in "${schemas[@]}"
do
    IFS='_' read -a db <<< "${schema}"
    echo "importing $schema";
	mysql -h "$MYSQL_HOST" -P "$MYSQL_PORT" --user="$MYSQL_USERNAME" --password="$MYSQL_PASSWORD" << EOFMYSQL
	CREATE DATABASE  IF NOT EXISTS \`${db[0]}\`; 
	USE \`${db[0]}\`;
	source ${DUMP_FOLDER}/mysql/${schema}.sql;		
EOFMYSQL

done
