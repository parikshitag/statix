#!/bin/bash
# database setup script

# Getting the DB Name
DB_NAME=$1

# Executing the db specific script 
bash $DB_NAME-db-setup.sh $@
