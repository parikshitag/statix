

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import com.statix.commons.config.ConfigReader;

public class TestRunner {
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(ConfigReader.class);
	      for (Failure failure : result.getFailures()) {
	         System.out.println(failure.toString());
	      }
	      System.out.println(result.wasSuccessful());
	}
}
