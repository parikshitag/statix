<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title><spring:message code="stx.title" /></title>

<%
	request.setAttribute("tabName", "schemabuilder");
	String updation_creationResult = (String) request.getAttribute("updation_creationResult");
%>

<script>
var updation_creationResult = '<%=updation_creationResult%>';
</script>

<%@include file="include/meta.jsp"%>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<%@include file="include/header.jsp"%>
			<%@include file="include/sidebar.jsp"%>
		</nav>


		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row page-head">
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i></li>
						<li class="active"><span><spring:message
									code="stx.schema.title" /></span></li>
					</ol>
				</div>


				<div class="row">
					<div class="col-md-12">
						<div class="widget" id="messageWidgetContainer">
							<div class="widget-title">
								<h3>
									<spring:message code="stx.schema.subtitle" />
								</h3>
							</div>
							<div class="widget-content">
								<div class="msg-success">
									<div class="alert alert-success alert-dismissable">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>${alert_SuccessMessage}</div>
								</div>
								<div class="msg-failure">
									<div class="alert alert-error alert-dismissable">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>${alert_FailureMessage}</div>
								</div>
								<div class="widget-loader" id="configMessageLoader"></div>
								<div id="schemaWrap">
									<a href="javascript:void(0)"
										<%-- "${pageContext.request.contextPath}/user/schema?schemaName=" --%>
										class="createModal add-ico pull-right"><i
										class="fa fa-plus "></i><span class="margin-lxs"> <spring:message
												code="stx.schema.newSchema" />
									</span></a>
									<table id="messageGroupTbl" class="table table-bordered"></table>
									<div id="messageErrWrap" class="clearfix"></div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->
	<!-- Modal -->
	<div class="modal fade" id="schemaTypeModal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">
						<spring:message code="stx.label.schemaSource" />
					</h4>
				</div>
				<div class="modal-body">
				<form name="modalForm" class="form-horizontal" id="modalForm"
										action="javascript:void(0)" method="POST"
										accept-encoding="UTF-8"> 
					<div class="control-group">
						<div class="row">
							<div class="radio col-sm-8">
								<label> <input class="idx-rowkey-radio" type="radio"
									name="idxFQNtype" value="import"> <i
									class="fa fa-files-o"></i> <spring:message
										code="stx.schema.import" />
								</label>
							</div>
							<div class="radio col-sm-8">
								<label> <input class="idx-rowkey-radio" type="radio"
									name="idxFQNtype" value="manual"> <i
									class="fa fa-wrench"></i> <spring:message
										code="stx.schema.manual" />
								</label>
							</div>
						</div>
					</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						<spring:message code="stx.label.cancel" />
					</button>
					<button id="createAlertBtn" type="button" class="btn btn-primary">
						<spring:message code="stx.label.continue" />
					</button>
				</div>
			</div>
			<!-- /.modal-content  dklskdsl-->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<script>
		var SchemaBuilder = new SchemaBuilder();
	</script>
</body>
</html>
