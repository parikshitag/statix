<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title><spring:message code="stx.title" /></title>

<%
	request.setAttribute("tabName", "schemaimport");
	String schemaName = request.getParameter("schemaName");
%>

<script>
		var schemaName = '<%=schemaName%>';
	if (schemaName == 'null')
		schemaName = '';
</script>

<%@include file="include/meta.jsp"%>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<%@include file="include/header.jsp"%>
			<%@include file="include/sidebar.jsp"%>
		</nav>


		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row page-head">
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i></li>
						<li><a
							href="${pageContext.request.contextPath}/user/schemabuilder"><spring:message
									code="stx.schema.title" /></a></li>
						<%
							if (schemaName != null && !schemaName.equalsIgnoreCase("")) {
						%>
						<li class="active"><span><%=schemaName%></span></li>
						<%
							} else {
						%>
						<li class="active"><span><spring:message
									code="stx.schema.newSchema" /></span></li>
						<%
							}
						%>
					</ol>
				</div>

				<div id="ajaxLoader">
					<img
						src="${pageContext.request.contextPath}/resources/images/ui-anim_basic_16x16.gif">
					<spring:message code="stx.loading" />
				</div>

				<div class="row">
					<form name="importSchema" class="form-horizontal"
												id="importSchema"
												action="javascript:void(0)" method="POST"
												>
						<div class="col-md-12">
							<div class="widget" id="messageWidgetContainer">
								<div class="widget-title">
									<ul class="nav nav-pills" id="schemaConfigureTabs">
										<li class="active"><a class="step" href="#step1"><spring:message
													code="stx.schema.step1" /></a></li>
										<li><a class="step" href="#step2"><spring:message
													code="stx.schema.step2" /></a></li>
										<li><a class="step" href="#step3"><spring:message
													code="stx.schema.step3" /></a></li>
									</ul>
								</div>
								<div class="widget-content">
									<div class='widget-content-extend'>
										<div class="tab-content widget-tab-content">
											<div class="tab-pane stepDetails active" id="step1">
											
												<div class="row">
													<div id="schemaNameWrap" class="form-group col-md-6">
														<label class="col-sm-4 control-label"><spring:message
																code="stx.label.schemaName" /> <spring:message
																code="stx.colonSign" />&nbsp;<span class="req"><spring:message
																	code="stx.asteriskSign" /></span> </label>
														<div class="col-sm-8">
															<spring:message code="stx.label.schemaName"
																var="schemaName" />
															<spring:message
																code="stx.parsley.message.onlyAlphabetAndNumber"
																var="onlyAlphabetAndNumber" />
															<input type="text" class="form-control"
																id="schemaNameFld" name="schemaName"
																placeholder="${schemaName}" data-parsley-required="true"
																data-parsley-pattern="^[A-Za-z\u00e4\u00f6\u00fc\u00c4\u00d6\u00dc\u00df\u00e1\u00e9\u00ed\u00f3\u00fa\u00fd\u00c0\u00c8\u00cc\u00d2\u00d9\u00e0\u00e8\u00ec\u00f2\u00f9\u00c1\u00c9\u00cd\u00d3\u00da\u00dd\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c3\u00d1\u00d5\u00a1\u00bf\u00e7\u00c7\u0152\u0153\u00df\u00d8\u00f8\u00c5\u00e5\u00c6\u00e6\u00de\u00fe\u00d0\u00f0]+[0-9A-Za-z\u00e4\u00f6\u00fc\u00c4\u00d6\u00dc\u00df\u00e1\u00e9\u00ed\u00f3\u00fa\u00fd\u00c0\u00c8\u00cc\u00d2\u00d9\u00e0\u00e8\u00ec\u00f2\u00f9\u00c1\u00c9\u00cd\u00d3\u00da\u00dd\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c3\u00d1\u00d5\u00a1\u00bf\u00e7\u00c7\u0152\u0153\u00df\u00d8\u00f8\u00c5\u00e5\u00c6\u00e6\u00de\u00fe\u00d0\u00f0]*$"
																data-parsley-maxlength="60"
																data-parsley-pattern-message="${onlyAlphabetAndNumber}"
																disabled autofocus />

														</div>
													</div>
												</div>
												<div class="row">
													<div id="schImport" class="form-group col-md-6">
														<label class="col-sm-4 control-label"><spring:message
																code="stx.label.file" /> <spring:message
																code="stx.colonSign" /> </label>
														<div class="col-sm-7">
															<input id="dataDir" type="file" class="filestyle"
																name="fileName"
																data-placeholder=/user/user_name/data_dir data-size="sm"
																data-icon="false" data-buttonText="...">
														</div>
													</div>
												</div>
												<div class="row">
													<div id="schImport" class="form-group col-md-8">
														<label class="col-sm-3 control-label">Import data
															from file </label>
														<div class="col-sm-2">
															<input type="checkbox" name="multiLine" checked="checked"
																id="isMultiLineFld" class="checkbox col-sm-9">
														</div>
													</div>
												</div>
										</div>
											<div class="tab-pane stepDetails" id="step2">
												<div class="row">
													<div id="delimiterWrap" class="form-group col-md-12">
														<label class="control-label pull-left col-sm-2"><spring:message
																code="stx.delimiter" /> <spring:message
																code="stx.colonSign" />&nbsp;<span class="req"></span>
														</label>
														<div class="pull-left col-sm-3">
															<select name="delimitValue"
																class="margin-lm margin-rs grid-delimiter form-control"
																id="schemaParserVal">
																<option value=","><spring:message
																		code="stx.commaSeparated" /></option>
																<option value="\t"><spring:message
																		code="stx.tabSeparated" /></option>
																<option value=":"><spring:message
																		code="stx.colonSeparated" /></option>
																<option value=";"><spring:message
																		code="stx.semicolonSeparated" /></option>
																<option value="|"><spring:message
																		code="stx.pipeSeparated" /></option>
																<option value="other"><spring:message
																		code="stx.otherSeparatedType" /></option>
															</select>
														</div>
														<div id="schemaParserDelOtherWrap"
															class="pull-left col-sm-2">
															<spring:message code="stx.placeholder.delimiter"
																var="delimiter" />
															<input type="text" class="form-control"
																name="otherDelimitValue" id="schemaParserDelOtherFld"
																placeholder="${delimiter}" data-parsley-maxlength="2" />
														</div>
														<div class="pull-left margin-rs">
															<button id="previewBtn" type="submit"
																name="previewSchema" class="btn btn-primary">
																<spring:message code="stx.label.preview" />
															</button>
														</div>
													</div>

													<div class="row">
														<label class="control-label pull-left col-sm-2"><spring:message
																code="stx.label.tblPreview" /> <spring:message
																code="stx.colonSign" />&nbsp;<span class="req"></span>
														</label>
														<table id="schemaFieldTbl" class="table table-bordered"></table>
														<div id="schemaErrWrap" class="clearfix"></div>
													</div>
												</div>
											</div>
											<div class="tab-pane stepDetails" id="step3">
												<div class="sel-msg-wrap">
													<div class="box-wrap">
														<div class="row">
															<div id="isHeaderData"
																class="pull-left form-group col-md-5">
																<label class="pull-left col-sm-11 control-label">Use
																	first row as column names </label>
																<div class="pull-left col-sm-1">
																	<input type="checkbox" checked="checked"
																		id="isHeaderRow" class="checkbox col-sm-9">
																</div>
															</div>
														</div>
														<div class="row">
															<div id="defineMetadata"></div>
															<!-- <table id="defineColumnsTbl"  class="table table-bordered"></table> -->
														</div>
														<div class="row">
															<div class="input-group">
																<textarea id="sqlgenerate" name="createQuery"
																	style="max-width: 100%; min-width: 100%"
																	class="col-md-12" rows=2 placeholder="Generated SQL"></textarea>
																<span class="input-group-addon btn btn-primary" id="refreshSQL">
																	<i class="fa fa-refresh "></i>
																</span>
															</div>
														</div>


														<div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
										<div class="form-footer">
											<div class="clearfix"></div>

											<button id="updateBtn" type="button"
												class="btn btn-primary pull-right">
												<spring:message code="stx.label.update" />
											</button>
											<button id="createBtn" type="button"
												class="btn btn-primary pull-right margin-rs">
												<spring:message code="stx.label.create" />
											</button>
											<button id="nextBtn" type="button"
												class="btn btn-primary pull-right margin-rs">
												<spring:message code="stx.label.next" />
											</button>
											<button id="backBtn" type="button"
												class="btn btn-primary pull-right margin-rs">
												<spring:message code="stx.label.back" />
											</button>

											<button id="cancelBtn" type="button"
												class="btn btn-default pull-right margin-rs">
												<spring:message code="stx.label.cancel" />
											</button>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
	<!-- Modal -->
	<div class="modal fade" id="uploadFileModal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">
						<spring:message code="stx.label.schemaSource" />
					</h4>
				</div>
				<div class="modal-body">
					<div id="filechooser"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						<spring:message code="stx.label.cancel" />
					</button>
					<button id="createAlertBtn" type="button" class="btn btn-primary">
						<spring:message code="stx.label.continue" />
					</button>
				</div>
			</div>
			<!-- /.modal-content  dklskdsl-->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<script>
		var SchemaImport = new SchemaImport();
	</script>
</body>
</html>
