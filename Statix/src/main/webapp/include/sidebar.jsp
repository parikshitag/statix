<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="sidebar-wrapper">
	<ul>
<%-- 		<li class='logo-wrap'><img
			src="${pageContext.request.contextPath}/resources/images/logo.png" />
			<span><img
				src="${pageContext.request.contextPath}/resources/images/sax.png" /></span>
		</li> --%>
		<li class='action active'>
				<a href="#"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>
		</li>
		<li class='action'>
				<a href="#"><i class="fa fa-server"></i><span>Overview</span></a>
		</li>
		<li class='action'>
				<a href="#"><i class="fa fa-flag"></i><span>Events</span></a>
		</li>
		<li class='action'>
				<a href="#"><i class="fa fa-cart-plus"></i><span>About</span></a>
		</li>
		<li class='action'>
				<a href="#"><i class="fa fa-youtube-play"></i><span>Services</span></a>
		</li>
		<li class='action'>
				<a href="#"><i class="fa fa-wrench"></i><span>Contact</span></a>
		</li>

	</ul>
	<iframe width="0" height="0" id="frmLogout" src="" class="hidden-elem"></iframe>
</div>