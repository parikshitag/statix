
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<!-- Sidebar -->
<div id="sidebar-wrapper" class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
	<ul class="nav" id="side-menu">

		<li class="active"><a href="#"><span
				class="fa-stack fa-lg pull-left"><i
					class="fa fa-dashboard fa-stack-1x "></i></span> Dashboard</a>
			<ul class="nav-pills nav-stacked" style="list-style-type: none;">
				<li><a href="#">link1</a></li>
				<li><a href="#">link2</a></li>
			</ul></li>
		<li><a href="#"><span class="fa-stack fa-lg pull-left"><i
					class="fa fa-flag fa-stack-1x "></i></span> Shortcut</a>
			<ul class="nav-pills nav-stacked" style="list-style-type: none;">
				<li><a href="#"><span class="fa-stack fa-lg pull-left"><i
							class="fa fa-flag fa-stack-1x "></i></span>link1</a></li>
				<li><a href="#"><span class="fa-stack fa-lg pull-left"><i
							class="fa fa-flag fa-stack-1x "></i></span>link2</a></li>

			</ul></li>
		<li><a href="#"><span class="fa-stack fa-lg pull-left"><i
					class="fa fa-cloud-download fa-stack-1x "></i></span>Overview</a></li>
		<li><a href="#"> <span class="fa-stack fa-lg pull-left"><i
					class="fa fa-cart-plus fa-stack-1x "></i></span>Events
		</a></li>
		<li><a href="#"><span class="fa-stack fa-lg pull-left"><i
					class="fa fa-youtube-play fa-stack-1x "></i></span>About</a></li>
		<li><a href="#"><span class="fa-stack fa-lg pull-left"><i
					class="fa fa-wrench fa-stack-1x "></i></span>Services</a></li>
		<li><a href="#"><span class="fa-stack fa-lg pull-left"><i
					class="fa fa-server fa-stack-1x "></i></span>Contact</a></li>
	</ul>
	</div>
</div>
<!-- /#sidebar-wrapper -->