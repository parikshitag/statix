<%@ page language="java" contentType="text/html; charset=ISO-8859-1" import="com.statix.mvc.ui.utils.ScriptletHelper" pageEncoding="ISO-8859-1"%>
    
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
					
<%
	String tabName = (String) request.getAttribute("tabName");

	String baseUrl = ScriptletHelper.evaluateBaseURL(request);
%>

 <script>
	var baseUrl = "<%=baseUrl%>";
</script> 

<%@include file="i18n.jsp"%>

<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/js/lib/metisMenu/dist/metisMenu.min.css" class="include" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/js/lib/font-awesome-4.5.0/css/font-awesome.min.css" class="include" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/select2.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/css/select2-bootstrap.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/css/theme.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resources/css/simple-sidebar.css" rel="stylesheet" type="text/css" />

<script src="${pageContext.request.contextPath}/resources/js/lib/jquery-1.12.0.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/lib/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/lib/parsley.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/lib/select2.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/lib/metisMenu/dist/metisMenu.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/base.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/sidebar_menu.js"></script>

<% if("schemabuilder".equalsIgnoreCase(tabName)) { %>
	<script src="${pageContext.request.contextPath}/resources/js/lib/datatable/js/jquery.dataTables.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/schemabuilder.js"></script>
<% } %>

<% if("schema".equalsIgnoreCase(tabName)) { %>
	<script src="${pageContext.request.contextPath}/resources/js/lib/jquery-ui.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/schema.js"></script>
<% } %>

<% if("schemaimport".equalsIgnoreCase(tabName)) { %>
	<script src="${pageContext.request.contextPath}/resources/js/lib/bootstrap-filestyle.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/lib/datatable/js/jquery.dataTables.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.filechooser.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/schema-import.js"></script>
<% } %>
