<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script>
	var i18N = [];

	i18N['stx.label.edit'] = "<spring:message code="stx.label.edit" />";
	i18N['stx.label.delete'] = "<spring:message code="stx.label.delete" />";
	i18N['stx.schema.schemaName'] = "<spring:message code="stx.schema.schemaName" />";
	i18N['stx.schema.schemaSource'] = "<spring:message code="stx.schema.schemaSource" />";
	i18N['stx.schema.actions'] = "<spring:message code="stx.schema.actions" />";
	i18N['stx.placeholder.pleaseSelect'] = "<spring:message code="stx.placeholder.pleaseSelect" />";
	
</script>
