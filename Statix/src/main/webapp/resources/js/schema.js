var Schema = function() {
    var obj = this,
    isSchema = true,
    schemaCreateUrl = baseUrl + "/res/schema/create",
    schemaImportUrl = baseUrl + "/user/schema/import"
    
    obj.init = function() {
     
    	 $("input[name='schemaName']").attr("disabled", false), $("#updateSchemaBtn").hide(), $("#createSchemaBtn").show();	
    	 $(document).on("change", "select#schemaParserType", function() {
             var parserVal = $(this).val();
     		("DELIMITED" == parserVal ? 
     				(		$("#schemaForm input[name\x3d'delimiter']").val($("select#schemaParserVal").val()), 
     						$("select#schemaParserVal").val(",").trigger("change"), 
     						$("#schemaParserValWrap").show())
     				: 
     					($("#schemaForm input[name\x3d'delimiter']").val(""), $("#schemaParserValWrap, #schemaParserDelOtherWrap").hide()))
         });
    	 $(document).on("change", "select#schemaParserVal", function() {
     		"other" == $("select#schemaParserVal").val() ? 
     				($("#schemaParserDelOtherWrap").is(":visible") || $("#schemaParserDelOtherFld").val(""), $("#schemaParserDelOtherWrap").show()) : $("#schemaParserDelOtherWrap").hide();
     	     $("#schemaForm input[name\x3d'delimiter']").val($("select#schemaParserVal").val())
         });
    	 $(document).on("click", ".idx-rowkey-radio", function() {

             "import" == $(this).val() ?($("#schemaParserTypeWrap").show(),$("select#schemaParserType").val("DELIMITED").trigger("change")) : ($("#schemaParserTypeWrap input[name\x3d'schemaParserDelOtherFld']").val(""), $("#schemaParserTypeWrap").hide())
         });
        $(document).on("click", "#updateSchemaBtn", function() {
            validate = obj.validateForm("schemaForm");
            !0 == validate ? ($("#groupNameFld").attr("disabled", !1).val($("#groupNameFld").val()), n = !0, $("#msgGroupForm").attr("method", "POST"), $("#msgGroupForm").attr("action", v).submit()) : $("a[href\x3d'#indexingWrap']").trigger("click")
        });
        $(document).on("click", "#createSchemaBtn", function() {
        	$("#createSchemaBtn").prop("disabled", 1);
        	"other" == $("select#schemaParserVal").val() ? ($("#schemaParserDelOtherFld").attr("data-parsley-required", "true"), $("#schemaForm input[name\x3d'delimiter']").val($("#schemaParserDelOtherFld").val())) :
            ($("#schemaParserDelOtherFld").removeAttr("data-parsley-required"), $("#schemaForm input[name\x3d'delimiter']").val($("select#schemaParserVal").val()));

            $("#schemaForm").attr("method", "GET"), $("#schemaForm").attr("action", schemaImportUrl).submit();
        });   
        $(document).on("click", "#cancelSchemaBtn", function() {
            $("#schemaForm").attr("method", "GET"), $("#schemaForm").attr("action", schemaImportUrl).submit();
        });
    	$("select").select2({
            placeholder: i18N["stx.placeholder.pleaseSelect"]
        });
        
        $("input.idx-rowkey-radio[value\x3d'manual']").trigger("click");
    	$("select#schemaParserType").trigger("change");
    };
    obj.init()
};