var SchemaImport = function() {
    var obj = this,
        isSchema = true,
        g, gd, tableArr = ['', ''],numColumns,
        fileSubmitUrl = baseUrl + "/res/schema/serde/preview",
        schemaUrl = baseUrl + "/res/schema/serde",
        schemaCreateUrl = baseUrl + "/res/schema/create",
        uploadUrl = baseUrl + "/res/schema/upload"

    obj.init = function() {
        $(".step").click(function(event) {
            event.preventDefault();
            $(".stepDetails").hide();
            var _step = $(this).attr("href");
            $(_step).css("visibility", "visible").show();
            $("#backBtn").hide();
            if (_step != "#step1") {
                $("#backBtn").css("visibility", "visible").show();
            }
            if (_step != "#step3") {
                $("#nextBtn").show();
                $("#createBtn").hide();
            } else {
                $("#nextBtn").hide();
                $("#createBtn").css("visibility", "visible").show();
            }
            if (_step === "#step2") {
            	obj.uploadFile();
                obj.chooseDelimiter();
            } else if (_step === "#step3") {
                obj.defineColumns();
                obj.refreshSQL();
            }
            $(".step").parent().removeClass("active");
            $(this).parent().addClass("active");


        });
        $(document).on("change", "select#schemaParserVal", function() {
            "other" == $("select#schemaParserVal").val() ?
                ($("#schemaParserDelOtherWrap").is(":visible") || $("#schemaParserDelOtherFld").val(""), $("#schemaParserDelOtherWrap").show()) : $("#schemaParserDelOtherWrap").hide();
            $("#schemaForm input[name\x3d'delimiter']").val($("select#schemaParserVal").val())
        });
        $("#nextBtn").click(function() {
            $("ul.nav-pills li.active").next().find("a").click();
        });
        $("#backBtn").click(function() {
            $("ul.nav-pills li.active").prev().find("a").click();
        });
        $("#previewBtn").click(function() {
            obj.chooseDelimiter();
        });
        $("#createBtn").click(function() {
        	/*var query = {};         
            query["schemaName"] = $("#schemaNameFld").val();
            query["fileName"] = $("input[type=file]").val();
            query["createQuery"] = $("#sqlgenerate").val(); */ 
            $("#importSchema").attr("method", "POST"), $("#importSchema").attr("action", schemaCreateUrl).submit();
            /*alert(obj.postAjaxData(schemaCreate, JSON.stringify(query)));*/
        });
        $("#isHeaderRow").click(function() {
            obj.defineColumns();
        });
        $("#refreshSQL").click(function() {
            obj.refreshSQL();
        });
        $("select#schemaParserVal").val(",").trigger("change");
        $("input[name='schemaName']").attr("disabled", false), $("#updateBtn,#createBtn,#backBtn,#schemaParserDelOtherWrap").hide(), $("#nextBtn").show();
    };
    obj.getAjaxData = function(b, a) {
        var d;
        $.ajax({
            type: "GET",
            dataType: "json",
            contentType: "application/json; charset\x3dutf-8",
            data: a,
            async: !1,
            url: b,
            beforeSend: function(a) {},
            success: function(a) {
                d = a
            }
        });
        // console.log(d.schemaName);
        return d
    };
    obj.postAjaxData = function(b, a) {
        // console.log(a);
        var d;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: a,
            async: !1,
            url: b,
            beforeSend: function(c) {},
            success: function(c) {
                d = c
            },
            error: function(c, a, b) {
                console.log("FAIL: " + b);
            }
        });
        return d
    };
    obj.uploadFile = function(){
    	var formdata = new FormData();
    	  formdata.append("file", $('#dataDir')[0].files[0]);
    	  $.ajax({
              type: "POST",            
              data: formdata,
              enctype: 'multipart/form-data',
              processData: false,
              contentType: false,
              async: !1,
              url: uploadUrl,
              beforeSend: function(c) {},
              success: function(c) {
                  d = c
              },
              error: function(c, a, b) {
                  console.log("FAIL: " + b);
              }
          });
    }
    obj.refreshSQL = function(){
    	var schemaName = $("#schemaNameFld").val();
    	var delimiter = "other" != $("select#schemaParserVal").val() ? $("select#schemaParserVal").val() : $("#schemaForm input[name\x3d'delimiter']").val();
    	var column = ''
    	for (j = 0; j < numColumns; j++) {
    		column+= $("#colname" + j).val() + ' ' + $("#coltype" + j).val()
    		if(j != (numColumns-1)) column += ', '
    	}  		
    	var sql = 'CREATE TABLE IF NOT EXISTS ' + schemaName + '(' + column + ') ROW FORMAT DELIMITED FIELDS TERMINATED BY \'' + delimiter + '\' LINES TERMINATED BY \'\\n\' STORED AS TEXTFILE' 
    	sql += ($("#isHeaderRow").prop("checked") === true) ?
    			 ' TBLPROPERTIES("skip.header.line.count"="1")' : '';
    	$("#sqlgenerate").val(sql);
    }
    obj.chooseDelimiter = function() {
        var preview = {};
        preview["schemaName"] = $("#schemaNameFld").val();
        preview["fileName"] = $("input[type=file]").val();
        preview["delimiter"] = "other" != $("select#schemaParserVal").val() ? $("select#schemaParserVal").val() : $("#schemaForm input[name\x3d'delimiter']").val();
        var dt = obj.postAjaxData(schemaUrl, JSON.stringify(preview));
        obj.preview(dt);

    };
    
    //Print Table Preview in Datatables
    obj.preview = function(schemaData) {
        var customHeader = [];
        tableArr.length = 0;
        $.each(schemaData, function(i, rval) {
            var arr = []
            if (i <= 2) {
                $.each(schemaData[i], function(j, cval) {
                    arr.push(cval);
                });
                //console.log(arr);
                tableArr[i] = arr;
            }
        });
        numColumns = schemaData[0].length;
        for (j = 0; j < schemaData[0].length; j++) {
            tmp = {
                'sTitle': "row " + (j + 1)
            };
            customHeader.push(tmp);
        };
        if (typeof g != 'undefined') {
            g.fnDestroy();
            $("#schemaFieldTbl").empty();
        }
        g = $("#schemaFieldTbl").dataTable({
            aaData: schemaData,
            bJQueryUI: !1,
            bAutoWidth: 1,
            bFilter: !1,
            bLengthChange: 1,
            bSort: !1,
            sDom: 'r<"H"lf><"datatable-scroll"t><"F"ip>',
            sPaginationType: "full_numbers",
            fnDrawCallback: function(a) {
                $(".tt").tooltip()
            },
            aoColumns: customHeader
        });
    }
    
    // Print Table Columns in Datatables
    obj.defineColumns = function() {

        var str = '<table id="defineColumnsTbl"  class="table table-bordered">' +
            '<thead>' +
            '<th id="column_names" style="width:210px">Column Name</th>' +
            '<th style="width:210px">Column Type</th>' +
            '<th><em>Sample Row 1</em></th>' +
            '<th><em>Sample Row 2</em></th>' +
            '</thead>' +
            '<tbody>'
        if (tableArr.length > 0) {
            for (i = 0; i < tableArr[0].length; i++) {
                str += '<tr>'
                for (j = 0; j < tableArr.length; j++) {
                    if (j === 0) {
                        str += '<td>' + '<input type="text" class="form-control" id="colname' + i + '" value ="'
                        str += ($("#isHeaderRow").prop("checked") === false) ? 'row ' + (i + 1) : tableArr[j][i]
                        str += '" />' + '</td>'
                    } else {
                        if (j === 1) {
                            str += '<td>' + '<select class="grid-delimiter form-control" id="coltype' + i + '">'
                            str += '<option value="string" selected="selected">string</option>'
                            str += '<option value="tinyint">tinyint</option>'
                            str += '<option value="smallint">smallint</option>'
                            str += '<option value="int">int</option>'
                            str += '<option value="bigint">bigint</option>'
                            str += '<option value="boolean">boolean</option>'
                            str += '<option value="float">float</option>'
                            str += '<option value="double">double</option>'
                            str += '<option value="array">array</option>'
                            str += '<option value="map">map</option>'
                            str += '<option value="timestamp">timestamp</option>'
                            str += '<option value="date">date</option>'
                            str += '<option value="char">char</option>'
                            str += '<option value="varchar">varchar</option>'
                            str += '</select>'
                            str += '</td>'
                        }
                        str += '<td>' + (tableArr[j][i]) + '</td>'
                    }
                }
                str += '</tr>'
            }
        }

        str += '</tbody></table>';
        $("#defineMetadata").empty();
        $("#defineMetadata").append(str);
    }
    obj.init()
};