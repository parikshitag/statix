var SchemaBuilder = function(n) {
    var obj = this,
        g, f, 
        schemaUrl = baseUrl + "/res/schema/list",
        schemaImportUrl = baseUrl + "/user/schema/import"
        k = "/res/schema/delete",
        l = "/adminui/sources",
        m = "/adminui/sources/list";
    obj.init = function() {
      /*  var cookie = obj.getCookie("STATIX_TIMEOUT_URL");
        "" != cookie && (obj.setCookie("STATIX_TIMEOUT_URL", null, 0), window.location = cookie);
        null == updation_creationResult
        		|| "FAILURE" != updation_creationResult.toUpperCase() && "SUCCESS" != updation_creationResult.toUpperCase()
        		|| (
        				window.history.pushState("", "", m), 
        				$(".msg-success, .msg-failure").hide(),
        				"FAILURE" == updation_creationResult.toUpperCase() ? $(".msg-failure").fadeIn(1E3) : "SUCCESS" == updation_creationResult.toUpperCase() && $(".msg-success").fadeIn(1E3), f = setTimeout(function() {
                $(".msg-failure , .msg-success").fadeOut(2500)
            }, 1E4));*/
        var schemaData = obj.getAjaxData(schemaUrl);
        obj.initSchemaList(schemaData);
        $("#configMessageLoader").hide();
        $(document).on("click", ".createModal", function(l) {
        	//l.preventDefault();
        	console.log("hi");
        	$("input.idx-rowkey-radio[value\x3d'manual']").trigger("click");
        	$("#schemaTypeModal .modal-dialog").width(600);
        	$("#schemaTypeModal").modal("show");        	
        });$
        $(document).on("click", "#createAlertBtn", function() {
        	console.log("inside createalertbutton");
        	$("#createAlertBtn").prop("disabled", 0);
        	
               $("#modalForm").attr("method", "GET"), $("#modalForm").attr("action", schemaImportUrl).submit();
        });
    };
    obj.setCookie = function(baseUrl, a, d) {
        var date = new Date;
        date.setTime(date.getTime() + 864E5 * d);
        d = "expires\x3d" + date.toGMTString();
        document.cookie = baseUrl + "\x3d" + a + "; " + d + ";path\x3d" + contextPath + "/"
    };
    obj.getCookie = function(b) {
        b += "\x3d";
        for (var a = document.cookie.split(";"), d = 0; d < a.length; d++) {
            var c = a[d].trim();
            if (0 == c.indexOf(b)) return c.substring(b.length,
                c.length)
        }
        return ""
    };
    obj.getAjaxData = function(b, a) {
        var d;
        $.ajax({
            type: "GET",
            dataType: "json",
            contentType: "application/json; charset\x3dutf-8",
            data: a,
            async: !1,
            url: b,
            beforeSend: function(a) {},
            success: function(a) {
                d = a
            }
        });
        console.log(d.schemaName);
        return d
    };
    obj.initSchemaList = function(schemaData) {
        "undefined" !== typeof schemaData.thrownError ? $("#messageErrWrap").html('\x3cdiv class\x3d"alert alert-danger margin-t"\x3e' + errorObj.TABLE_RENDERING + "\x3c/div\x3e") : 0 == schemaData.length ? $("#messageErrWrap").html('\x3cdiv class\x3d"alert alert-info margin-t"\x3e' + errorObj.NO_DATA + "\x3c/div\x3e") : g = $("#messageGroupTbl").dataTable({
            aaData: schemaData,
            bJQueryUI: !1,
            bAutoWidth: !1,
            bFilter: !0,
            iDisplayLength: 10,
            sDom: "ftip",
            sPaginationType: "full_numbers",
            fnDrawCallback: function(a) {
                $(".tt").tooltip()
            },
            aoColumns: [{
                mData: "schemaName",
                sTitle: i18N["stx.schema.schemaName"]
            }, {               
                mData: "schemaSource",
                sTitle: i18N['stx.schema.schemaSource']
            }, {
                bSearchable: !1,
                mDataProp: null,
                bSortable: !1,
                sDefaultContent: "",
                sTitle: i18N["stx.schema.actions"],
                sWidth: "80px",
                mData: function(a) {
                    return '\x3cspan class\x3d"action-wrap"\x3e\x3cinput type\x3d"hidden" name\x3d"groupName" value\x3d"' +
                        a.groupName + '"\x3e\x3cinput type\x3d"hidden" name\x3d"groupId" value\x3d"' + 
                        a.groupId + '"\x3e\x3cinput type\x3d"hidden" name\x3d"fieldCounter" value\x3d"' + 
                        a.fieldCounter + '"\x3e\x3ca href\x3d"javascript:void(0)" title\x3d"' + 
                        i18N["stx.label.edit"] + '" class\x3d"ico-edit tt"\x3e\x3ci class\x3d"fa fa-edit"\x3e\x3c/i\x3e\x3c/a\x3e\x3ca href\x3d"' + 
                        a.groupName + '" title\x3d"' + i18N["stx.label.delete"] + '" class\x3d"ico-del tt"\x3e\x3ci class\x3d"fa fa-trash-o"\x3e\x3c/i\x3e\x3c/a\x3e\x3c/span\x3e'
                }
            }]
        })
    };
    obj.init()
};