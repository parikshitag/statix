var SideBar = function() {
	var obj = this
	obj.init = function() {
		$("#menu-toggle").click(function(e) {
			console.log("menu-toggle clicked");
			e.preventDefault();
			$("#swrapper").toggleClass("toggled");
		});
		$("#menu-toggle-2").click(function(e) {
			console.log("menu-toggle-2 clicked");
			e.preventDefault();
			$("#swrapper").toggleClass("toggled-2");
			$('#menu ul').hide();
		});

	};
	$('#menu ul').hide();
	$('#menu ul').children('.current').parent().show();
	// $('#menu ul:first').show();
	$('#menu li a').click(function() {
		var checkElement = $(this).next();
		if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
			return false;
		}
		if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
			$('#menu ul:visible').slideUp('normal');
			checkElement.slideDown('normal');
			return false;
		}
	});
	obj.init()
};