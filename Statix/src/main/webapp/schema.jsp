<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title><spring:message code="stx.title" /></title>

<%
	request.setAttribute("tabName", "schema");
	String schemaName = request.getParameter("schemaName");
%>

<script>
		var schemaName = '<%=schemaName%>
	';
	if (schemaName == 'null')
		schemaName = '';
</script>

<%@include file="include/meta.jsp"%>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation"
			style="margin-bottom: 0">
			<%@include file="include/header.jsp"%>
			<%@include file="include/sidebar.jsp"%>
		</nav>


		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row page-head">
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i></li>
						<li><a
							href="${pageContext.request.contextPath}/user/schemabuilder"><spring:message
									code="stx.schema.title" /></a></li>
						<%
							if (schemaName != null && !schemaName.equalsIgnoreCase("")) {
						%>
						<li class="active"><span><%=schemaName%></span></li>
						<%
							} else {
						%>
						<li class="active"><span><spring:message
									code="stx.schema.newSchema" /></span></li>
						<%
							}
						%>
					</ol>
				</div>

				<div id="ajaxLoader">
					<img
						src="${pageContext.request.contextPath}/resources/images/ui-anim_basic_16x16.gif">
					<spring:message code="stx.loading" />
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="widget" id="messageWidgetContainer">
							<div class="widget-title">
								<ul class="nav nav-pills" id="schemaConfigureTabs">
									<li class="active"><a href="#schemaConfigureWrap"><spring:message
												code="stx.schema.schemaConfiguration" /></a></li>
								</ul>
							</div>
							<div class="widget-content">
								<div class='widget-content-extend'>
									<form name="schemaForm" class="form-horizontal" id="schemaForm"
										action="javascript:void(0)" method="POST"
										data-parsely-validate accept-encoding="UTF-8">

										<div class="tab-content widget-tab-content">
											<div class="tab-pane active" id="schemaConfigureWrap">
												<div class="hide-elem duplicate-name-error"
													id="duplicateNameError"></div>
												<div class="row">
													<div id="schemaNameWrap" class="form-group col-md-6">
														<label class="col-sm-4 control-label"><spring:message
																code="stx.label.schemaName" /> <spring:message
																code="stx.colonSign" />&nbsp;<span class="req"><spring:message
																	code="stx.asteriskSign" /></span> </label>
														<div class="col-sm-8">
															<spring:message code="stx.label.schemaName"
																var="schemaName" />
															<spring:message
																code="stx.parsley.message.onlyAlphabetAndNumber"
																var="onlyAlphabetAndNumber" />
															<input type="text" class="form-control"
																id="schemaNameFld" name="schemaName"
																placeholder="${schemaName}" data-parsley-required="true"
																data-parsley-pattern="^[A-Za-z\u00e4\u00f6\u00fc\u00c4\u00d6\u00dc\u00df\u00e1\u00e9\u00ed\u00f3\u00fa\u00fd\u00c0\u00c8\u00cc\u00d2\u00d9\u00e0\u00e8\u00ec\u00f2\u00f9\u00c1\u00c9\u00cd\u00d3\u00da\u00dd\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c3\u00d1\u00d5\u00a1\u00bf\u00e7\u00c7\u0152\u0153\u00df\u00d8\u00f8\u00c5\u00e5\u00c6\u00e6\u00de\u00fe\u00d0\u00f0]+[0-9A-Za-z\u00e4\u00f6\u00fc\u00c4\u00d6\u00dc\u00df\u00e1\u00e9\u00ed\u00f3\u00fa\u00fd\u00c0\u00c8\u00cc\u00d2\u00d9\u00e0\u00e8\u00ec\u00f2\u00f9\u00c1\u00c9\u00cd\u00d3\u00da\u00dd\u00c2\u00ca\u00ce\u00d4\u00db\u00e2\u00ea\u00ee\u00f4\u00fb\u00c3\u00d1\u00d5\u00a1\u00bf\u00e7\u00c7\u0152\u0153\u00df\u00d8\u00f8\u00c5\u00e5\u00c6\u00e6\u00de\u00fe\u00d0\u00f0]*$"
																data-parsley-maxlength="60"
																data-parsley-pattern-message="${onlyAlphabetAndNumber}"
																disabled autofocus />

														</div>
													</div>
												</div>
												<div class="row">
													<div class="form-group col-md-12">
														<div class="row">
														<div class="radio col-sm-6">
															<label> <input class="idx-rowkey-radio"
																type="radio" name="idxFQNtype" value="import">
																<i class="fa fa-files-o"></i> <spring:message code="stx.schema.import" />
															</label>
														</div>
														<div id="schemaParserTypeWrap" class="form-group col-md-10 margin-ts">
															<label class="col-sm-2 control-label"><spring:message
																	code="stx.label.schemaParserType" /> <spring:message
																	code="stx.colonSign" /></label>
															<div class="col-sm-3">
																<select class="grid-type form-control"
																	id="schemaParserType" name="schemaSource">
																	<option value="DELIMITED"><spring:message
																			code="stx.delimited" /></option>
																	<option value="json"><spring:message
																			code="stx.json" /></option>
																</select>
															</div>
															<div id="schemaParserValWrap">
																<div class="col-sm-3">
																	<select class="margin-lm grid-delimiter form-control"
																		id="schemaParserVal">
																		<option value=","><spring:message
																				code="stx.commaSeparated" /></option>
																		<option value="\\t"><spring:message
																				code="stx.tabSeparated" /></option>
																		<option value=":"><spring:message
																				code="stx.colonSeparated" /></option>
																		<option value=";"><spring:message
																				code="stx.semicolonSeparated" /></option>
																		<option value="|"><spring:message
																				code="stx.pipeSeparated" /></option>
																		<option value="other"><spring:message
																				code="stx.otherSeparatedType" /></option>
																	</select>
																</div>
																<div class="col-sm-2">
																	<div id="schemaParserDelOtherWrap">
																		<spring:message code="stx.placeholder.delimiter"
																			var="delimiter" />
																		<input type="text" class="form-control"
																			id="schemaParserDelOtherFld"
																			placeholder="${delimiter}" data-parsley-maxlength="2" />
																	</div>
																</div>
															</div>
														</div>
														</div>
														<div class="radio">
															<label> <input class="idx-rowkey-radio"
																type="radio" name="idxFQNtype" value="manual">
																<i class="fa fa-wrench"></i> <spring:message code="stx.schema.manual" />
															</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
									<div class="clearfix"></div>
									<div class="form-footer">
										<div class="clearfix"></div>

										<button id="updateSchemaBtn" type="button"
											class="btn btn-primary pull-right">
											<spring:message code="stx.label.update" />
										</button>
										<button id="createSchemaBtn" type="button"
											class="btn btn-primary pull-right margin-rs">
											<spring:message code="stx.label.create" />
										</button>
										<button id="cancelBtn" type="button"
											class="btn btn-default pull-right margin-rs">
											<spring:message code="stx.label.cancel" />
										</button>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<script>
		var Schema = new Schema();
	</script>
</body>
</html>
