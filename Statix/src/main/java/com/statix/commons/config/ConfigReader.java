package com.statix.commons.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.statix.framework.hive.TableOperations;

public class ConfigReader {
	private static ConfigReader instance;

	private static final Logger logger = LoggerFactory.getLogger(ConfigReader.class);

	private Properties getConfigurations() {
		final String property = System.getProperty("CONFIG_PATH");
		if (StringUtils.isEmpty(property)) {
			logger.warn("ERROR: Config path not entered. Use -DCONFIG_PATH to pass as VM argument.");
		}
		logger.info("Reading property file: " + "stx-env.properties");
		File file = new File(property, "stx-env.properties");
		logger.info("File: " + file.getAbsolutePath());
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(file));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return properties;
	}
	
	private static ConfigReader getInstance() {
		logger.info("In TableOperations.getInstance method");
        if (instance == null ) {
            synchronized (ConfigReader.class) {
                if (instance == null) {
                	logger.info("TableOperations instance is null");
                    instance = new ConfigReader();
                }
            }
        }

        return instance;
    }
	
	public static String getProperty(String property){
		return ConfigReader.getInstance().getConfigurations().getProperty(property);
	}
	
}
