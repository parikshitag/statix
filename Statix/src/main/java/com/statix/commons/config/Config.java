package com.statix.commons.config;

import java.io.File;

public class Config {
	public static final String HOME_DIR = ConfigReader.getProperty("server.home_dir") + "/";
	public static final String LOCAL_DIR = ConfigReader.getProperty("local.home_dir") + File.separator;
	public static final String DATA_DIR = HOME_DIR  + "data" + "/";
	public static final String LDATA_DIR = LOCAL_DIR + "data" + File.separator;
	public static final String IP = ConfigReader.getProperty("server.ip");
	
	public static final String HDFS_URL = ConfigReader.getProperty("hdfs.url");
	public static final String HDFS_USERNAME = ConfigReader.getProperty("hdfs.username");
	public static final String HDFS_DATA_DIR = ConfigReader.getProperty("hdfs.data_dir") + File.separator;
	
	public static final String HIVE_URL = ConfigReader.getProperty("hive.url");
	public static final String HIVE_USERNAME = ConfigReader.getProperty("hive.username");
	public static final String HIVE_PASSWORD = ConfigReader.getProperty("hdfs.password");
}
