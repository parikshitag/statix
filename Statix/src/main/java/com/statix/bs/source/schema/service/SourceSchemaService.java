package com.statix.bs.source.schema.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.statix.bs.source.schema.model.SourceSchema;
import com.statix.bs.source.serde.model.CSVSerde;

@Service
public interface SourceSchemaService {
	List<SourceSchema> listSchemas();
	String insertSchema(CSVSerde csvserde);
}
