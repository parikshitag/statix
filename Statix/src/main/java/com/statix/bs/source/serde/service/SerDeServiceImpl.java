package com.statix.bs.source.serde.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.statix.bs.source.schema.dao.SourceSchemaDaoImpl;
import com.statix.bs.source.serde.dao.CSVSerdeDaoImpl;
import com.statix.bs.source.serde.dao.SerDeDao;

@Service("SerDeServiceImpl")
public class SerDeServiceImpl implements SerDeService {

	private static final Logger logger = LoggerFactory.getLogger(SerDeServiceImpl.class);
	
	@Autowired
	@Qualifier("CSVSerdeDaoImpl")
	private CSVSerdeDaoImpl serde;
	
	@Override
	public List<String[]> getPreview(String file, String delimiter) {
		try {
			return serde.deserialize(new FileReader(file), delimiter);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
