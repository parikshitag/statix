package com.statix.bs.source.schema.model;

public class SourceSchema{
	private String schemaId;
	private String tenantId;
	private String schemaName;
	private String schemaSource;
	   
	public String getSchemaId() {
		return schemaId;
	}

	public void setSchemaId(String schemaId) {
		this.schemaId = schemaId;
	}
	
	public String getTenantId() {
        return this.tenantId;
    }
    
    public void setTenantId(final String tenantId) {
        this.tenantId = tenantId;
    }
    
	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	public String getSchemaSource() {
		return schemaSource;
	}

	public void setSchemaSource(String schemaSource) {
		this.schemaSource = schemaSource;
	}


    
}
