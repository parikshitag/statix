package com.statix.bs.source.serde.dao;

import java.io.Reader;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;


@Service
public interface SerDeDao {
	public void init(HashMap<String, Object> conf);
	public Object deserialize(Reader reader, String delimiter);
	public void serialize(Object obj);
}
