package com.statix.bs.source.serde.model;

public class CSVSerde {

	private String schemaName;
	private String fileName;
	private String delimiter;
	private String otherDelimiter;
	private String createQuery;
	private String loadQuery;
	
	public String getSchemaName() {
		return schemaName;
	}
	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDelimiter() {
		return delimiter;
	}
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}
	public String getOtherDelimiter() {
		return otherDelimiter;
	}
	public void setOtherDelimiter(String otherDelimiter) {
		this.otherDelimiter = otherDelimiter;
	}
	
	public String getCreateQuery() {
		return createQuery;
	}
	public void setCreateQuery(String createQuery) {
		this.createQuery = createQuery;
	}
	public String getLoadQuery() {
		return loadQuery;
	}
	public void setLoadQuery(String loadQuery) {
		this.loadQuery = loadQuery;
	}
	public String toString(){
		return
				"Message [schemaName=" +
				schemaName +
				", schemaLoc=" +
				fileName +
				", delimiter=" +
				delimiter +
				", otherDelimiter=" +
				otherDelimiter +
				", createQuery=" +
				createQuery +
				", loadQuery=" +
				loadQuery +
				"]";
	}
	
}
