package com.statix.bs.source.serde.dao;

import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.opencsv.CSVReader;
import com.statix.bs.source.schema.dao.SourceSchemaDaoImpl;

@Repository("CSVSerdeDaoImpl")
public class CSVSerdeDaoImpl implements SerDeDao {
	
	private static final Logger logger = LoggerFactory.getLogger(CSVSerdeDaoImpl.class);

	HashMap<String,Object> conf;
	@Override
	public void init(HashMap<String, Object> conf) {
		// TODO Auto-generated method stub
		conf.put("separaterChar", (char)',');
		conf.put("quoteChar", (char)'\"');
		conf.put("escapeChar", (char)'\"');
		this.conf = conf;
	}

	@Override
	public List<String[]> deserialize(Reader reader, String delimiter) {
		List<String[]> row = new ArrayList<>();
		char delimit = delimiter.equals("\\t")? '\t': delimiter.charAt(0);
	    CSVReader csv = null;
	    try {
	    			csv = new CSVReader(reader, delimit);	//, (char)conf.get("quoteChar"), (char)conf.get("escapeChar"));      
	      String[] readLine;
	      int cnt = 0;
	      while((readLine = csv.readNext()) != null && cnt < 50){	      
		      /*for (int i=0; i< readLine.length; i++) {
		        if (readLine != null && i < readLine.length) {
		          row.set(i, readLine[i]);
		        } else {
		          row.set(i, null);
		        }
		      }	 */     
	    	  cnt++;
	    	  row.add(readLine);
	      }
	      return row;
	    } catch (final Exception e) {
	    	e.printStackTrace();
	    } finally {
	      if (csv != null) {
	        try {
	          csv.close();
	        } catch (final Exception e) {
	          // ignore
	        }
	      }
	    }
		return null;
	}

	@Override
	public void serialize(Object obj) {
		// TODO Auto-generated method stub
		
	}
}
