package com.statix.bs.source.serde.service;

import org.springframework.stereotype.Service;

@Service
public interface SerDeService {
	Object getPreview(String dataDir, String str);
}
