package com.statix.bs.source.schema.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import com.statix.bs.source.schema.dao.SourceSchemaDaoImpl;
import com.statix.bs.source.schema.model.SourceSchema;
import com.statix.bs.source.serde.model.CSVSerde;

@Service("SourceSchemaServiceImpl")
public class SourceSchemaServiceImpl implements SourceSchemaService {

	private static final Logger logger = LoggerFactory.getLogger(SourceSchemaDaoImpl.class);

	@Autowired
	@Qualifier("SourceSchemaDaoImpl")
	private SourceSchemaDaoImpl srcSchemaDaoImpl;
	
	@Override
	public List<SourceSchema> listSchemas() {
		logger.info("Inside Service implementation listSchemas method");
		return srcSchemaDaoImpl.getAllSourceSchemas();
	}

	@Override
	public String insertSchema(CSVSerde csvserde) {
		logger.info("Inside Service implementation insertSchema method");
		return srcSchemaDaoImpl.insertSourceSchema(csvserde);
	}
	
	
}
