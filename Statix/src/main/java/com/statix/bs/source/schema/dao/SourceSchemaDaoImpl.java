package com.statix.bs.source.schema.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.statix.bs.sql.SqlQueryMapper;
import com.statix.bs.source.schema.model.SourceSchema;
import com.statix.bs.source.serde.model.CSVSerde;

@Repository("SourceSchemaDaoImpl")
public class SourceSchemaDaoImpl {

	private static final Logger logger = LoggerFactory.getLogger(SourceSchemaDaoImpl.class);
	
	/*@Autowired
	//@Qualifier("SqlQueryMapper")
*/	private SqlQueryMapper query;
	@Autowired
    @Qualifier("jdbcTemplate")
    private JdbcTemplate jdbcTemplate;
	public SourceSchemaDaoImpl() {
		// TODO Auto-generated constructor stub
		query = new SqlQueryMapper();
	}
	public List<SourceSchema> getAllSourceSchemas() {
		logger.info("Inside DAO Implementation getAllSourceSchemas Method");
		final ArrayList<SourceSchema> list = new ArrayList<SourceSchema>();
        final String str = query.getQueries().get("SOURCESDALIMPL_GETALLSCHEMAS");
        final SqlRowSet queryForRowSet = jdbcTemplate.queryForRowSet(str);
        while (queryForRowSet.next()) {
            final SourceSchema srcSchema = new SourceSchema();
            srcSchema.setSchemaId(queryForRowSet.getString("schema_id"));
            srcSchema.setSchemaName(queryForRowSet.getString("schema_name"));
            srcSchema.setSchemaSource(queryForRowSet.getString("schema_source"));
            srcSchema.setTenantId(queryForRowSet.getString("tenant_id"));
            list.add(srcSchema);
        }
        return list;
	}
	
	public String insertSourceSchema(CSVSerde csvserde){
		logger.info("Inside DAO Implementation createSourceSchema Method");
		final String sql = query.getQueries().get("SOURCESDALIMPL_INSERTSCHEMA");
		//final SqlRowSet queryForRowSet = jdbcTemplate.queryForRowSet(sql);
		int schemaId = new AtomicInteger().incrementAndGet();
		int tenantId = new AtomicInteger().incrementAndGet();
		String schemaSource = "Dilimited";
		int update=0;
		try{
		update = jdbcTemplate.update(sql,  new Object[] {schemaId, tenantId, csvserde.getSchemaName(), schemaSource});
		}catch(Exception e){
			e.printStackTrace();
		}
		if(update > 0)
			return "Success";
		else
			return "Failure";
	}
}
