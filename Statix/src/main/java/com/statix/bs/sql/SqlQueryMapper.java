package com.statix.bs.sql;

import java.util.HashMap;
import java.util.Map;

public class SqlQueryMapper {
	//public static final IConfiguration config;
	public static final Map<String, String> query;
	
	public Map<String, String> getQueries() {
        return query;
    }
	
	static{
		(query = new HashMap<String, String>())
				.put("SOURCESDALIMPL_GETALLSCHEMAS", "select schema_id, tenant_id, schema_name, schema_source from src_schemas order by date_created");
		query.put("SOURCESDALIMPL_INSERTSCHEMA","insert into src_schemas (schema_id, tenant_id, schema_name,schema_source) values(?,?,?,?)");			
	}
}
