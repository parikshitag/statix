package com.statix.mvc.ui.utils;

import javax.servlet.http.HttpServletRequest;

public class ScriptletHelper {

	public static String evaluateBaseURL(final HttpServletRequest httpServletRequest) {
        return httpServletRequest.getScheme() + "://" + httpServletRequest.getServerName() + ":" + httpServletRequest.getServerPort() + httpServletRequest.getContextPath();
    }
}
