package com.statix.mvc.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.statix.bs.source.schema.model.SourceSchema;
import com.statix.bs.source.schema.service.SourceSchemaService;
import com.statix.bs.source.serde.model.CSVSerde;
import com.statix.bs.source.serde.model.FileMeta;
import com.statix.commons.config.Config;
import com.statix.commons.config.ConfigReader;
import com.statix.framework.IO.IOOperations;
import com.statix.framework.hdfs.FileSystemOperations;
import com.statix.framework.hive.TableOperations;

@Controller("ResourceController")
@RequestMapping({ "/res" })
public class resources extends BaseController {

	private static final long serialVersionUID = -347217503187600767L;
	private static final Logger logger = LoggerFactory.getLogger(resources.class);

	@Autowired
	@Qualifier("SourceSchemaServiceImpl")
	private SourceSchemaService srcSchemaService;

	@RequestMapping("/schema/list")
	@ResponseBody
	public List<SourceSchema> getAllSourceSchemas() {
		logger.info("Inside resources controller getAllSourceSchemas method");
		try {
			return (List<SourceSchema>) srcSchemaService.listSchemas();
		} catch (Exception ex) {
			logger.error("Error occurred while getting all Schemas: ", (Throwable) ex);
		}
		return (Collections.emptyList());
	}

	@RequestMapping(value = { "/schema/create" }, method = { RequestMethod.POST })
	public ModelAndView createSchema(final CSVSerde csvserde) {
		logger.info("Inside resources controller createSchema method");
		final ModelAndView modelAndView = new ModelAndView("schemabuilder");
		String output = "Table Creation Output: " + TableOperations.getInstance().executeQuery(csvserde.getCreateQuery());
		String query = "LOAD DATA LOCAL INPATH '" + Config.DATA_DIR  + csvserde.getFileName() + "' INTO TABLE "
				+ csvserde.getSchemaName();
		csvserde.setLoadQuery(query);
		logger.info("Data: " + csvserde.toString());
		output += "\n" + "Load Table Output: " +  TableOperations.getInstance().executeQuery(query);
		srcSchemaService.insertSchema(csvserde);
		modelAndView.addObject("updation_creationResult", (Object)"SUCCESS");
        modelAndView.addObject("updation_creationSuccessMessage", (Object)output);
        return modelAndView;
	}

	@RequestMapping(value = "/schema/upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload(@RequestParam("file") MultipartFile file) {
		FileMeta fileMeta = new FileMeta();
		if (!file.isEmpty()) {
			try {
				// 2.3 create new fileMeta
				fileMeta.setName(file.getName());
				fileMeta.setFileName(file.getOriginalFilename());
				fileMeta.setFileSize(file.getSize() / 1024 + " Kb");
				fileMeta.setFileType(file.getContentType());
				fileMeta.setBytes(file.getBytes());

				// Create the file on server
				new IOOperations(fileMeta);

				return "You successfully uploaded file=" + fileMeta.getFileName();
			} catch (Exception e) {
				return "You failed to upload " + fileMeta.getFileName() + " => " + e.getMessage();
			}
		} else {
			return "You failed to upload " + fileMeta.getFileName() + " because the file was empty.";
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/schema/serde" }, method = { RequestMethod.POST })
	@ResponseBody
	public List<String[]> getSerde(@RequestBody final CSVSerde csvserde) {
		logger.info("Inside resources controller getSerde method");
		logger.info("Data directory file:" + csvserde.getFileName());
		try {
			return (List<String[]>) serdeService.getPreview(Config.LDATA_DIR + csvserde.getFileName(),
					csvserde.getDelimiter());
		} catch (Exception ex) {
			logger.error("Error occurred while getting all Schemas: ", (Throwable) ex);
		}
		return (Collections.emptyList());
	}

}
