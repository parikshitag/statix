package com.statix.mvc.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.statix.bs.source.serde.service.SerDeService;

public class BaseController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3718076489118152474L;
	@Autowired
	@Qualifier("SerDeServiceImpl")
	SerDeService serdeService;
}
