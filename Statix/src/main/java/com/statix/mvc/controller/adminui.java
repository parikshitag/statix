package com.statix.mvc.controller;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.statix.bs.source.serde.model.CSVSerde;

@Controller("UIController")
@RequestMapping({ "/user" })
public class adminui extends BaseController{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2851154579546966548L;
	
	private static final Logger logger = LoggerFactory.getLogger(resources.class);

	@RequestMapping(value = { "/schema" }, method = { RequestMethod.GET })
	public ModelAndView newSchema(final HttpServletRequest httpServletRequest) {
		/*
		 * boolean contains = false; boolean contains2 = false; String
		 * configuration = "none"; String configuration2 = "none"; try {
		 * configuration =
		 * A.\u00c3.getConfiguration("system-config.persistence.store");
		 * configuration2 =
		 * A.\u00c3.getConfiguration("system-config.index.type"); final
		 * List<LicensingModuleEnum> b = this.B(); if (null != b) { contains =
		 * b.contains(LicensingModuleEnum.PERSISTENCE); contains2 =
		 * b.contains(LicensingModuleEnum.INDEXING); if
		 * (b.contains(LicensingModuleEnum.INDEXING_PERSISTENCE)) { contains =
		 * true; contains2 = true; } } } catch (Exception ex) {
		 * A.\u00c1.error((Object)
		 * "Error occurred while requesting message group configuration page: ",
		 * (Throwable)ex); } if (contains) {
		 * httpServletRequest.setAttribute("enablePersistence",
		 * (Object)configuration); } else {
		 * httpServletRequest.setAttribute("enablePersistence", (Object)"none");
		 * } if (contains2) { httpServletRequest.setAttribute("enableIndexing",
		 * (Object)configuration2); } else {
		 * httpServletRequest.setAttribute("enableIndexing", (Object)"none"); }
		 */
		return new ModelAndView("schema", "domain", (Object) "logmonitoring");
	}

	@RequestMapping(value = { "/schemabuilder" }, method = { RequestMethod.GET })
	public ModelAndView schemaBuilder(final HttpServletRequest httpServletRequest) {
		return new ModelAndView("schemabuilder");
	}

	@RequestMapping(value = { "/schema/import" }, method = { RequestMethod.GET })
	public ModelAndView schemaImport(final HttpServletRequest httpServletRequest) {
		return new ModelAndView("schema-import");
	}
	
/*	
	@ResponseBody
	@RequestMapping(value = { "/schema/import" }, params= "previewBtn", method = { RequestMethod.GET })
    public List<String[]> schemaPreview(final CSVSerde csvserde) {  
		logger.info("Inside adminui controller getSerde method");
		logger.info("Data directory file:" + csvserde.getSchemaName());
		try{
                return (List<String[]>)serdeService.getPreview("d:\\csv\\" + csvserde.getSchemaLoc());
		} catch(Exception ex){
			logger.error("Error occurred while getting all Schemas: ", (Throwable)ex);
		}
        return   (Collections.emptyList());
    }*/
}
