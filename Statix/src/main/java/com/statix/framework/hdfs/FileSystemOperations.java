package com.statix.framework.hdfs;

import java.security.PrivilegedExceptionAction;
import java.util.Arrays;

import org.apache.commons.io.FilenameUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.statix.bs.source.serde.model.FileMeta;
import com.statix.commons.config.ConfigReader;

public class FileSystemOperations {

	private static final Logger logger = LoggerFactory.getLogger(FileSystemOperations.class);

	public FileSystemOperations(FileMeta fileMeta) {
		Path filePath = new Path(ConfigReader.getProperty("hdfs.data_dir") + "/" + FilenameUtils.removeExtension(fileMeta.getFileName()));
		logger.info("fileName: " + filePath.toString());
		
		try {
			UserGroupInformation ugi = UserGroupInformation.createRemoteUser(ConfigReader.getProperty("hdfs.url"));
			ugi.doAs(new PrivilegedExceptionAction<Void>() {

				public Void run() throws Exception {
					Configuration conf = new Configuration();
					conf.set("fs.defaultFS", ConfigReader.getProperty("hdfs.url"));
					conf.set("hadoop.job.ugi", ConfigReader.getProperty("hdfs.username"));

					FileSystem fs = FileSystem.get(conf);
					fs.mkdirs(filePath);
					FSDataOutputStream stream = fs.create(new Path(filePath.toString() + '/' + fileMeta.getFileName()));
					stream.write(fileMeta.getBytes());
					stream.flush();
					logger.info("file written to hdfs");
					stream.close();

					return null;
				}
			});
		} catch (Exception e) {
			logger.info("error: " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}