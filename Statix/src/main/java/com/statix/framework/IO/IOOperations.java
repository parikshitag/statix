package com.statix.framework.IO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.statix.bs.source.serde.model.FileMeta;
import com.statix.commons.config.Config;
import com.statix.commons.config.ConfigReader;

public class IOOperations {

	private static final Logger logger = LoggerFactory.getLogger(IOOperations.class);

	public IOOperations(FileMeta fileMeta) {
		String file = Config.LDATA_DIR + File.separator + fileMeta.getFileName();
		logger.info("fileName: " + file);

		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);
			fos.write(fileMeta.getBytes());		
			fos.flush();
			logger.info("file written to hdfs");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		

	}
}