package com.statix.framework.hive;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.statix.commons.config.ConfigReader;

import java.sql.DriverManager;

public class TableOperations {
	private static TableOperations instance;
	private static String driverName = "org.apache.hive.jdbc.HiveDriver";
	Statement stmt;

	private static final Logger logger = LoggerFactory.getLogger(TableOperations.class);

	public static TableOperations getInstance() {
		logger.info("In TableOperations.getInstance method");
		if (instance == null) {
			synchronized (TableOperations.class) {
				if (instance == null) {
					logger.info("TableOperations instance is null");
					(instance = new TableOperations()).initialize();
				}
			}
		}

		return instance;
	}

	public void initialize() {

		// Register driver and create driver instance
		try {
			Class.forName(driverName);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		logger.info("Driver loaded");
		Connection con;
		try {
			con = DriverManager.getConnection(ConfigReader.getProperty("hive.url"),
					ConfigReader.getProperty("hive.username"), ConfigReader.getProperty("hive.password"));
			// create statement
			stmt = con.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String executeQuery(String query) {
		try {
			stmt.execute(query);
			logger.info("Query executed succesfully");
			return "Query Executed Successfully";
		} catch (SQLException e) {
			return "Fail: " + e.getMessage();
		}
	}

}
