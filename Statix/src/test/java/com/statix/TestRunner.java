package com.statix;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import com.statix.framework.hive.TableOperationsTest;

public class TestRunner {
	 public static void main(String[] args) {
	      Result result = JUnitCore.runClasses(TableOperationsTest.class);
	      for (Failure failure : result.getFailures()) {
	         System.out.println(failure.toString());
	      }
	      System.out.println(result.wasSuccessful());
	   }
}
